from sqlalchemy import Column, Integer,String,Enum,ARRAY,DateTime,UniqueConstraint,ForeignKeyConstraint,BOOLEAN,CheckConstraint,Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from datetime import datetime
from config import Base,engine,Session,ALLOWED_DATATYPES,login_manager
from flask_login import UserMixin


#creating the userloader block to fetch the currenlty logged in user
session = Session()
@login_manager.user_loader
def load_user(userId):
    return session.query(MetaUser).filter(MetaUser.userId==userId).first()

class MetaUser(Base,UserMixin):
    __tablename__='meta_users'
    userId = Column(Integer,primary_key=True)
    userName = Column(String(20),nullable=False)
    password = Column(String(60),nullable=False)
    userEmail = Column(String(40),nullable=False)
    _userRight = Column('userRight',Enum('ADMIN','PUBLISHER','AUTHOR',name='URight'),default='PUBLISHER',nullable=False)
    _userStatus = Column('userStatus',Enum('ACTIVE','INACTIVE','SUSPEND',name='UStatus'),default='ACTIVE',nullable=False)
    schemaAccess=Column(ARRAY(String),nullable=True)
    createdOn=Column(DateTime,default=datetime.now)
    UniqueConstraint(userName, name='same_user_entry')
    
    def get_id(self):
        return self.userId

    @hybrid_property
    def userRight(self):
        return self._userRight
    
    @userRight.setter
    def userRight(self,val):
        self._userRight=val.upper()

    @hybrid_property
    def userStatus(self):
        return self._userStatus
    
    @userStatus.setter
    def userRight(self,val):
        self._userStatus=val.upper()
        
    @property
    def serialized(self):
        return { 'userId':self.userId,'userName':self.userName,'password':self.password,'userEmail':self.userEmail,'userStatus':self._userStatus,'userRight':self._userRight,'schemaAccess':self.schemaAccess,'createdOn':self.createdOn }
    
    def __repr__(self):
        return f'{{userId={self.userId},userName={self.userName},password={self.password},userEmail={self.userEmail},userStatus={self._userStatus},userRight={self._userRight},schemaAccess={self.schemaAccess},createdOn={self.createdOn} }} '

class MetaLayoutDataSource(Base):
    __tablename__='metalayout_datasources'
    id = Column(Integer,primary_key=True)
    datasourceName= Column(String(80),unique=True, nullable=False)
    datasourceDescription=Column(String(200),nullable=True)
    createdOn = Column(DateTime,default=datetime.now)
    createdBy= Column(Integer,nullable=False)
    updatedOn = Column(DateTime,nullable=True)
    updatedBy= Column(Integer,nullable=True)
    ForeignKeyConstraint([createdBy],[MetaUser.userId],name='fk_create_datasource_user')
    ForeignKeyConstraint([updatedBy],[MetaUser.userId],name='fk_update_datasource_user')
    create_users=relationship("MetaUser",backref="ds_creator",foreign_keys=[createdBy], lazy=True)    
    update_users=relationship("MetaUser",backref="ds_updator",foreign_keys=[updatedBy], lazy=True)


    def __init__(self,datasourceName,datasourceDescription,createdBy):
        self.datasourceName=datasourceName
        self.datasourceDescription=datasourceDescription
        self.createdBy=createdBy
      
    @property
    def serialized(self):
        return {
            'datasourceId':self.id,
            'datasourceName':self.datasourceName,
            'datasourceDescription':self.datasourceDescription,
            'updatedOn':self.updatedOn,
            'updatedBy':self.updatedBy,
            'createdOn':self.createdOn,
            'createdBy':self.createdBy,
            'datasource_creator':self.create_users.serialized,
            'datasource_updator':self.update_users.serialized if self.update_users else ''
        }
    
    @property
    def base_serialized(self):
        return {
            'datasourceId':self.id,
            'datasourceName':self.datasourceName,
            'datasourceDescription':self.datasourceDescription
        }

    def __repr__(self):
        return f" MetaLayoutDataSource < datasourceId={self.id},datasourceName={self.datasourceName}, datasourceDescription={self.datasourceDescription},createdOn={self.createdOn}, createdBy={self.createdBy}, updatedOn={self.updatedOn},updatedBy={self.updatedBy}>"


class MetaLayout(Base):
    __tablename__='meta_layouts'
    id = Column(Integer,primary_key=True)
    layoutHead= Column(String(50),unique=True, nullable=False)
    layoutDescription=Column(String(200),nullable=True)
    headerAtRow=Column(Integer,nullable=False,default=1)
    footerRowCount=Column(Integer,nullable=False,default=0)
    createdOn = Column(DateTime,default=datetime.now)
    createdBy= Column(Integer,nullable=False)
    updatedOn = Column(DateTime,nullable=True)
    updatedBy= Column(Integer,nullable=True)
    ForeignKeyConstraint([createdBy],[MetaUser.userId],name='fk_create_layout_user')
    ForeignKeyConstraint([updatedBy],[MetaUser.userId],name='fk_update_layout_user')
    create_users=relationship("MetaUser",backref="creator",foreign_keys=[createdBy], lazy=True)    
    update_users=relationship("MetaUser",backref="updator",foreign_keys=[updatedBy], lazy=True)
    CheckConstraint(headerAtRow>0,name='chk_negative_headerval')
    CheckConstraint(footerRowCount>=0,name='chk_negative_footerVal')


    def __init__(self,layoutHead,layoutDescription,createdBy,headerAtRow=None,footerRowCount=None):
        self.layoutHead=layoutHead
        self.layoutDescription=layoutDescription
        self.createdBy=createdBy
        self.headerAtRow=headerAtRow
        self.footerRowCount=footerRowCount
      
    @property
    def serialized(self):
        return {
            'layoutId':self.id,
            'layoutHead':self.layoutHead,
            'layoutDescription':self.layoutDescription,
            'headerAtRow':self.headerAtRow,
            'footerRowsCount':self.footerRowCount,
            'updatedOn':self.updatedOn,
            'updatedBy':self.updatedBy,
            'createdOn':self.createdOn,
            'createdBy':self.createdBy,
            'creator':self.create_users.serialized,
            'updator':self.update_users.serialized if self.update_users else '',
            'datasources':[ds_data.base_serialized for ds_data in (session.query(MetaLayoutDataSource)
            .filter(MetaLayoutDataSource.id.in_(
                [data.schemaName for data in (
                    session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.layoutHead==self.id).all()
                    )])).all())]
        }

    def __repr__(self):
        return f" MetaLayout < layoutid={self.id},laoutHead={self.layoutHead}, layoutDesc={self.layoutDesc},createdOn={self.createdOn}, createdBy={self.createdBy}, updatedOn={self.updatedOn},updatedBy={self.updatedBy}>"

    
class MetaLayoutDetail(Base):
    __tablename__ = 'meta_layouts_dtl'
    id=Column(Integer,primary_key=True)    
    attrIdentifier=Column(String(5000),nullable=False)
    attrName=Column(String(50),nullable=False)
    attrDescription=Column(String(200),nullable=True)
    _attrType=Column('attrType',String(20),nullable=False)
    attrMinLength=Column(Integer)
    attrMaxLength=Column(Integer)
    attrLOV=Column(ARRAY(String),nullable=True)
    _attrNull=Column('attrNull',BOOLEAN,default=False)
    atrrRegex=Column(String(150),nullable=True)
    fieldOrder=Column(Integer,nullable=False)
    layoutHead=Column(Integer,nullable=False)
    createdOn = Column(DateTime,default=datetime.now)
    createdBy= Column(Integer,nullable=False)
    updatedOn = Column(DateTime,nullable=True)
    updatedBy= Column(Integer,nullable=True)
    layoutInfo=relationship('MetaLayout',backref="layoutInfo",foreign_keys=[layoutHead],lazy=True)
    created_by=relationship('MetaUser',backref='detailscreator',foreign_keys=[createdBy],lazy=True)
    updated_by=relationship('MetaUser',backref='detailsupdator',foreign_keys=[updatedBy],lazy=True)
    ForeignKeyConstraint([createdBy],[MetaUser.userId],name='fk_create_layoutdtl_user')
    ForeignKeyConstraint([updatedBy],[MetaUser.userId],name='fk_update_layoutdtl_user')
    ForeignKeyConstraint([layoutHead],[MetaLayout.id],name='fk_layoutid_layoutdtl_layout') 
    CheckConstraint(attrMinLength<=attrMaxLength,name='chk_min_max')
    CheckConstraint(_attrType.in_([dt for dt in ALLOWED_DATATYPES]), name='chk_type_attribute')
    UniqueConstraint(attrIdentifier,layoutHead,name='uq_same_field_in_layout')
    UniqueConstraint(fieldOrder,layoutHead,name='uq_same_col_order_in_layout')
    CheckConstraint(fieldOrder>0,name='chk_negative_ordering')
  
    def __init__(self,attrName,layoutHead,attrIdentifier,attrType,createdBy,fieldOrder,attrDescription=None,attrMinLength=None,attrMaxLength=None,attrLOV=None,attrNull=None,attrRegex=None):
        self.attrName=attrName
        self.attrDescription=attrDescription
        self.attrIdentifier=attrIdentifier
        self._attrType=attrType
        self.attrMinLength=attrMinLength
        self.attrMaxLength=attrMaxLength
        self.attrLOV=attrLOV
        self._attrNull=attrNull
        self.atrrRegex=attrRegex
        self.layoutHead=layoutHead
        self.fieldOrder=fieldOrder
        self.createdBy=createdBy
    
    @property
    def getInstance(self):
        return MetaLayoutDetail('','','','',None,1)

    def build(self,dictValue):
        for k,v in dictValue.items():
             setattr(self,k,v)
        return self

    def get_mandateField():
        return ['attrIdentifier','attrDescription','attrName','attrType','attrMinLength','attrMaxLength','attrLOV','attrNull','atrrRegex','fieldOrder','layoutHead','createdBy']   

    @hybrid_property
    def attrNull(self):
       return self._attrNull
    
    @attrNull.setter
    def attrNull(self,val):
        self._attrNull=bool(val)
    
    @hybrid_property
    def attrType(self):
        return self._attrType
    
    @attrType.setter
    def attrType(self,val):
        self._attrType=val.upper()

    @property
    def serialized(self):
        return {
            'layoutDtlId':self.id,
            'attrIdentifier':self.attrIdentifier,
            'attrDescription':self.attrDescription,
            'attrName':self.attrName,
            'attrType':self._attrType,
            'attrMinLength':self.attrMinLength,
            'attrMaxLength':self.attrMaxLength,
            'attrLOV':self.attrLOV,
            'attrNull':self._attrNull,
            'atrrRegex':self.atrrRegex,
            'layoutHead':self.layoutHead,
            'fieldOrder':self.fieldOrder,
            'createdOn':self.createdOn,
            'createdBy':self.createdBy,
            'updatedOn':self.updatedOn,
            'updatedBy':self.updatedBy,
            'layoutInfo':self.layoutInfo.serialized,
            'detailscreator':self.created_by.serialized,
            'detailsupdator':self.updated_by.serialized if self.updated_by else '' \
        }

    @property
    def detailList(self):
        return [
            self.attrIdentifier,
            self.attrName,
            self.attrDescription,            
            self._attrType,
            self.attrMinLength,
            self.attrMaxLength,
            self.attrLOV,
            self._attrNull,
            self.atrrRegex
           # self.fieldOrder
        ]

    def __repr__(self):
        return f"MetaLayoutDetails <'layoutDtlId'={self.id},'attrIdentifier'={self.attrIdentifier},'attrDescription'={self.attrDescription},'attrName'={self.attrName},'attrType'={self._attrType},'attrMinLength'={self.attrMinLength},'attrMaxLength'={self.attrMaxLength},'attrLOV'={self.attrLOV},'attrNull'={self._attrNull},'attrRegex'={self.atrrRegex},'fieldOrder'={self.fieldOrder},'layoutId'={self.layoutHead},'createdOn'={self.createdOn},'createdBy'={self.createdBy},'updatedOn'={self.updatedOn},'updatedBy'={self.updatedBy}>"

class MetaFileUploadLogs(Base):
    __tablename__='meta_fileupload_logs'
    id=Column(Integer,primary_key=True)
    name=Column(String(100),nullable=False)
    description=Column(Text,nullable=True)
    uploadedFileName=Column(String(200),nullable=False)
    savedFileName=Column(String(300),nullable=False)
    fileSize=Column(String(10),nullable=False)
    isLayoutFile=Column(BOOLEAN,nullable=False,default=False)
    isDatasetFile=Column(BOOLEAN,nullable=False,default=False)
    belongsToLayout=Column(Integer)
    logTime = Column(DateTime,default=datetime.now)    
    remarks=Column(Text,nullable=True)
    uploadedBy= Column(Integer,nullable=False)
    layoutinfo=relationship("MetaLayout",backref="layoutinfo",foreign_keys=[belongsToLayout],lazy=True)
    uploader=relationship('MetaUser',backref='uploader',foreign_keys=[uploadedBy],lazy=True)
    ##constraints
    CheckConstraint(isLayoutFile != isDatasetFile, name='chk_layout_dataset_conflict')
    ForeignKeyConstraint([belongsToLayout],[MetaLayout.id],name='fk_uploaded_file_layout')
    ForeignKeyConstraint([uploadedBy],[MetaUser.userId],name='fk_file_uploader')

    def __init__(self,name,description,uploadedFileName,savedFileName,fileSize,belongsToLayout,uploadedBy,isLayoutFile=False,isDatasetFile=False,remarks=None):
        self.name=name
        self.description
        self.uploadedFileName=uploadedFileName
        self.savedFileName=savedFileName
        self.fileSize=fileSize
        self.belongsToLayout=belongsToLayout
        self.uploadedBy=uploadedBy
        self.isLayoutFile=isLayoutFile
        self.isDatasetFile=isDatasetFile
        self.remarks=remarks

    
    @property
    def serialized(self):
        return {
        'uploadlog_id':self.id,
        'name':self.name,
        'description':self.description,
        'upload_filename':self.uploadedFileName,
        'upload_filename':self.savedFileName,
        'filesize':self.fileSize,
        'layout_id':self.belongsToLayout,
        'uploaded_by':self.uploadedBy,
        'layout_file':self.isLayoutFile,
        'dataset_file':self.isDatasetFile,
        'start_time':self.logTime,
        'remarks':self.remarks,
        'uploader_info':self.uploader.serialized,
        'metadata_info':self.layoutinfo.serialized if self.layoutinfo else ''
        }
    

class MetaClientDatasets(Base):
    __tablename__='meta_client_dataset'
    id=Column(Integer,primary_key=True)
    datasetName=Column(String(75),nullable=False)
    uploadDetail=Column(Integer,nullable=False)
    uploadLog = relationship("MetaFileUploadLogs",backref='uploadlog',lazy=True,foreign_keys=[uploadDetail])
    UniqueConstraint(datasetName,name='unq_dataset_name')
    ForeignKeyConstraint([uploadDetail],[MetaFileUploadLogs.id], name='fk_file_upload_log')
    
    def __init__(self,datasetName,uploadDetail):
        self.datasetName=datasetName
        self.uploadDetail=uploadDetail
    
    @property
    def serialized(self):
        return {
            'dataset_id':self.id,
            'dataset_name':self.datasetName,
            'dataset_detail':self.uploadDetail,
            'upload_log_details':self.uploadLog.serialized
        }
        
class MetaDataMasterEntry(Base):
    __tablename__='metadata_lookup'
    lookupId = Column(Integer,primary_key=True)
    lookupKey = Column(String(25), nullable=False)
    lookupValue=Column(Text,nullable=False, unique=True)
    remarks=Column(Text,nullable=True)

    def __init__(self,lookupKey,lookupValue,remarks=None):
        self.lookupKey=lookupKey
        self.lookupValue=lookupValue
        self.remarks=remarks
    
    @property
    def serialized(self):
        return {
            'lookup_id':self.lookupId,
            'lookup_key':self.lookupKey,
            'lookup_value':self.lookupValue,
            'remarks':self.remarks
        }
        
class MetaLayoutSchemaMapping(Base):
    __tablename__='meta_layoutschema_mapper'
    mapId = Column(Integer,primary_key=True)
    schemaName=Column(Integer,nullable=False)
    layoutHead=Column(Integer,nullable=False)
    createdOn=Column(DateTime,default=datetime.now)
    createdBy=Column(Integer,nullable=False)
    updatedOn=Column(DateTime,nullable=True)
    updatedBy=Column(Integer,nullable=True)
    UniqueConstraint(schemaName,layoutHead, name='unq_layoutschema_map')
    ForeignKeyConstraint([schemaName],[MetaLayoutDataSource.id],name='fk_mapper_schema')
    ForeignKeyConstraint([layoutHead],[MetaLayout.id],name='fk_mapper_layoutid')
    ForeignKeyConstraint([createdBy],[MetaUser.userId],name='fk_mapper_creator')
    ForeignKeyConstraint([updatedBy],[MetaUser.userId],name='fk_mapper_updator')
    updator=relationship('MetaUser',foreign_keys=[updatedBy],backref='mapupdator',lazy=True)
    creator=relationship('MetaUser',foreign_keys=[createdBy],backref='mapcreator',lazy=True)
    layoutName=relationship('MetaLayout',foreign_keys=[layoutHead],backref='layoutName',lazy=True)
    datasourceInfo=relationship('MetaLayoutDataSource',foreign_keys=[schemaName],backref='datasourceInfo',lazy=True)


    def __init__(self,schemaName,layoutHead,createdBy):
        self.schemaName = schemaName
        self.layoutHead = layoutHead
        self.createdBy = createdBy

    @property
    def serialized(self):
        return {
            'mapper_id':self.mapId,
            'layout_id':self.layoutHead,
            'schema_id':self.schemaName,
            'createdOn':self.createdOn,
            'createdBy':self.createdBy,
            'updatedOn':self.updatedOn,
            'updatedBy':self.updatedBy,
            'schemaInfo':self.datasourceInfo.serialized if self.datasourceInfo else '',
            'creatorInfo':self.creator.serialized if self.createdBy else '',
            'updatorInfo':self.updator.serialized if self.updatedBy else '',
            'layoutInfo':self.layoutName.serialized if self.layoutName else ''           
        }
    
    def __repr__(self):
        return f" MetaLayoutSchemaMapping < mapper_id={self.mapper_id},layout_id={self.layoutHead}, schema_id={self.schemaName},createdOn={self.createdOn}, createdBy={self.createdBy}, updatedOn={self.updatedOn},updatedBy={self.updatedBy}>"


# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)