from config import api,ALLOWED_DATATYPES,app
from flask_restplus import fields
from werkzeug.datastructures import FileStorage


# defining the model for Lookup master vallue response
master_value = api.model('master_lookup_val',{'lookup_value':fields.String,'remarks':fields.String})


# defining the model for userobject response and request
user_model=api.model('user_general_info',{'userName':fields.String(attribute='userName',description='register username'), 'userEmail':fields.String(description='abc@example.com'),'_userStatus':fields.String(enum=['ACTIVE','INACTIVE','SUSPEND']),'_userRight':fields.String(enum=['AUTHOR','ADMIN','PUBLISHER']),'schemaAccess':fields.List(fields.String(description='List of all accessible schemas')),'createdOn':fields.DateTime})
user_model_get_params = api.model('user_fetch_by_id',{'user_id':fields.Integer(description='Userid to retrieve',default=None),'pagination':fields.Nested(api.model('pagination_pram',{"limitedBy":fields.Integer,"userid_offset":fields.Integer}))})
user_model_get = api.inherit('user_fetch_all',user_model,{'userId':fields.Integer})
user_model_post = api.model('user_data_post',{ "username": fields.String(required=True,description='register username'),"password": fields.String(required=True),"email":fields.String(required=True,description='abc@example.com'),"status" :fields.String(required=True,enum=['ACTIVE','INACTIVE','SUSPEND']),"schema_access_ist": fields.List(fields.String(description='List of all accessible schemas')),"user_right" : fields.String(required=True,enum=['AUTHOR','ADMIN','PUBLISHER']) })
user_model_put = api.model('user_data_put',{"newData":fields.Nested(user_model,required=True)})
user_password_put = api.model('user_pass_put',{"new_password":fields.Nested(api.model('password_param',{'password':fields.String(required=True) }),required=True)})


#defining the models for the datasource
datasource_get=api.model('datasource_data_get',{'datasourceId':fields.Integer,
            'datasourceName':fields.String,
            'datasourceDescription':fields.String,
            'updatedOn':fields.DateTime,
            'updatedBy':fields.Integer,
            'createdOn':fields.DateTime,
            'createdBy':fields.Integer,
            'datasource_creator':fields.Nested(api.model('usermodel_ds_creator',user_model)),
            'datasource_updator':fields.Nested(api.model('usermodel_ds_updator',user_model))})

datasource_post=api.model('datasource_data_post',{
            'datasource_name':fields.String(required=True),
            'datasource_description':fields.String(required=True)
            })

datasource_model = api.model('datasource_general_info',{
            'datasourceName':fields.String(required=True,attribute='datasourceName'),
            'datasourceDescription':fields.String(attribute='datasourceDescription')
            })

datasource_model_list = api.model('datasource_List',{
            'datasourceId':fields.String(required=True,attribute='datasourceId'),
            'datasourceName':fields.String(required=True,attribute='datasourceName'),
            'datasourceDescription':fields.String(required=True,attribute='datasourceDescription')
            })

datasource_put=api.model("datasource_data_put",{
    "newDatasourceData":fields.Nested(
        api.model('datasouce_desc',datasource_model),required=True)
        })

datasource_user_map=api.model('datasource_user_mapping',{
    'MappingDetail':fields.List(
        fields.Nested(
            api.model('datasourceusermap_payload',{
            'datasource_id':fields.Integer(required=True),
            'userids':fields.List(fields.Integer(required=True))}
            )
        )
    ,required=True)
})
#defining the models for the metainfo
metainfo_get=api.model('metainfo_data_get',{'layoutId':fields.Integer,
            'layoutHead':fields.String,
            'layoutDescription':fields.String,
            'headerAtRow':fields.Integer,
            'footerRowsCount':fields.Integer,
            'updatedOn':fields.DateTime,
            'updatedBy':fields.Integer,
            'createdOn':fields.DateTime,
            'createdBy':fields.Integer,
            'creator':fields.Nested(api.model('usermodel_creator',user_model)),
            'updator':fields.Nested(api.model('usermodel_updator',user_model)),
            'datasources':fields.List(fields.Nested(datasource_model_list))})

metainfo_post=api.model('metainfo_data_post',{
            'layout_head':fields.String(required=True),
            'layout_description':fields.String(required=True),
            'headerOnRow':fields.Integer(default=1),
            'footerRows':fields.Integer(default=0),
            'override_Flag':fields.Boolean(default=False)
            })

metainfo_model = api.model('metainfo_general_info',{
            'layoutHead':fields.String(required=True,attribute='layoutHead'),
            'layoutDescription':fields.String(attribute='layoutDescription'),
            'headerAtRow':fields.Integer(default=1),
            'footerRowCount':fields.Integer(default=0)

            })

metainfo_model_list = api.model('metainfo_List',{
            'layoutId':fields.String(required=True,attribute='layoutId'),
            'layoutHead':fields.String(required=True,attribute='layoutHead'),
            'layoutDescription':fields.String(attribute='layoutDescription'),
            'headerAtRow':fields.Integer,
            'footerRowsCount':fields.Integer,
            })

metainfo_put=api.model("metainfo_data_put",{"newLayoutData":fields.Nested(api.model('layout_desc',metainfo_model),required=True)})

#defining the models for the metainfo-detials
metainfo_detail=api.model('metainfo_detail_base',{
    'layoutDtlId':fields.Integer,
    'atrrRegex':fields.String,
    'attrDescription':fields.String,
    'attrIdentifier':fields.String,
    'attrName':fields.String,
    'attrMinLength':fields.Integer,
    'attrMaxLength':fields.Integer,
    'attrLOV':fields.List(fields.String),
    'attrNull':fields.Boolean,
    'attrType':fields.String(enum=list(ALLOWED_DATATYPES)),
    'layoutHead':fields.Integer,
    'fieldOrder':fields.Integer
})
metainfo_detail_get=api.inherit('metainfo_detail_data_get',metainfo_detail,{
            'createdOn':fields.DateTime,
            'createdBy':fields.Integer,
            'updatedOn':fields.DateTime,
            'updatedBy':fields.Integer,
            'layoutInfo':fields.Nested(api.model('info_on_layout_desc',metainfo_get)),
            'detailscreator':fields.Nested(api.model('info_on_detail_creator',user_model)),
            'detailsupdator':fields.Nested(api.model('info_on_detail_updator',user_model))
            })

metainfo_detail_bulkget=api.model('metainfo_detail_data_bulkget',{
    'message':fields.String,
    'remarks':fields.String,
    'defect':fields.String,
    'causedby':fields.String,
    'attribute_details':fields.List(fields.Nested(metainfo_detail_get))
})

metainfo_detail_post = api.model('metainfo_detail_data_post',{    
    'attribute_regex':fields.String(),
    'attribute_desc':fields.String(),
    'attribute_identifier':fields.String(required=True),
    'attribute_name':fields.String(),
    'attribute_minlen':fields.Integer(),
    'attribute_maxlen':fields.Integer(),
    'attribute_lov':fields.List(fields.String),
    'attribute_null':fields.Boolean(required=True),
    'attribute_type':fields.String(required=True,enum=list(ALLOWED_DATATYPES)),
    'attribute_layout':fields.Integer(required=True),
    'field_order':fields.Integer(required=True)
})

metainfo_detail_bulk=api.model('metainfo_detail_bulk_post',{
    'Detail_data':fields.List(fields.Nested(metainfo_detail_post,required=True),required=True),
    'override_flag':fields.Boolean(default=False),
    'override_layout':fields.Integer
})

metainfo_detail_put = api.model('metainfo_detail_data_put',{
    'newDetailInfo':fields.List(fields.Nested(metainfo_detail),required=True),  
    'newLayoutData':fields.Nested(api.model('layoutdata_appended',{'headerAtRow':fields.Integer,
            'footerRowCount':fields.Integer,'layoutHead':fields.Integer})),
})

#defining the model for the file handling  for meta data import and dataset import 
from flask_restplus import reqparse
meta_data_upload=reqparse.RequestParser()
# meta_data_upload.add_argument('metadata_file',location='files',type=FileStorage,required=True)
meta_data_upload.add_argument('Metadata name',dest='name',location='form',required=True)
meta_data_upload.add_argument('Metadata description',dest='description',location='form',default='')
meta_data_upload.add_argument('Header at row',dest='header_onrow',type=int, location='form',default=1,required=True)
meta_data_upload.add_argument('Footers on row',dest='footer_rows',type=int, location='form',default=0)
# meta_data_upload.add_argument('Header at top',dest='header_ontop',type=bool, location='form',default=False)
meta_data_upload.add_argument('Seperator in csv',dest='csv_seperator',location='form',default=',')
meta_data_upload.add_argument('Encoding format',dest='data_encoding',location='form',default='utf-8')
meta_data_upload.add_argument('File size',dest='file_size',location='form',type=int, default='0')

meta_data_upload_stream=meta_data_upload.copy()
meta_data_upload_stream.add_argument('metadata_file',location='files',type=FileStorage,required=True)
meta_data_upload_bucket=meta_data_upload.copy()
meta_data_upload_bucket.add_argument('metadata_filename',location='form',required=True)

dataset_upload=reqparse.RequestParser()
# dataset_upload.add_argument('dataset_file',location='files',type=FileStorage,required=True)
dataset_upload.add_argument('Dataset name',dest='name',location='form',required=True)
dataset_upload.add_argument('Selected metadata name',dest='description', type=int, location='form',required=True)
# dataset_upload.add_argument('Header at top',dest='header_ontop', type=bool, location='form',default=False)
dataset_upload.add_argument('Override Header footer',dest='override_headfoot',type=bool,default=False)
dataset_upload.add_argument('Header at row',dest='header_onrow',type=int, location='form',default=1)
dataset_upload.add_argument('Footers on row',dest='footer_rows',type=int, location='form',default=0)
dataset_upload.add_argument('Seperator in csv',dest='csv_seperator',location='form',default=',')
dataset_upload.add_argument('Encoding format',dest='data_encoding',location='form',default='utf-8')
dataset_upload.add_argument('File size',dest='file_size',location='form',type=int, default='0')


dataset_upload_stream=dataset_upload.copy()
dataset_upload_stream.add_argument('dataset_file',location='files',type=FileStorage,required=True)
dataset_upload_bucket=dataset_upload.copy()
dataset_upload_bucket.add_argument('dataset_filename',location='form',required=True)

#defining the models for the schemaset and the layout mapping 
mapping_data= api.model('base_mapping_datainfo',{
    'mapper_id':fields.Integer,
    'layout_id':fields.Integer,
    'schema_id':fields.Integer,
    'createdOn':fields.DateTime,
    'createdBy':fields.Integer,
    'updatedOn':fields.DateTime,
    'updatedBy':fields.Integer})

mapping_data_get=api.inherit('mapping_datainfo_get',mapping_data,{
            'creatorInfo':fields.Nested(user_model),
            'updatorInfo':fields.Nested(user_model),
            'layoutInfo':fields.Nested(metainfo_model),
            'schemaInfo':fields.Nested(datasource_model_list)
})

mapping_data_postinfo = api.model('mapping_datainfo_post',{
    'mappings':fields.List(fields.Nested(
        api.model('mappost_payload',{
        'schema_name':fields.Integer(required=True),
        'layout_id':fields.Integer(required=True)
})))})

mapping_data_put_content=api.model('mapput_payload_nested',{
            'schemaName':fields.Integer(required=True),
            'layoutHead':fields.Integer(required=True)
        })

mapping_data_put=api.model('mapping_datainfo_put',{
    'newmappings':fields.List(fields.Nested(api.model('mapput_payload',{
        'map_id':fields.Integer(required=True),
        'mapdata':fields.Nested(mapping_data_put_content)
    })))
})

mapping_data_remove=api.model('mapping_datainfo_remove',{
    'removemappings':fields.List(fields.Nested(api.model('mapremove_payload',{
        'map_id':fields.Integer(required=True)
    })),required=True)
})


#defining the model for metallayout,mapping and detail data posting at once
metaimport_upload_post=api.model('metaimport_upload_post',{
    'layout_info':fields.Nested(metainfo_post),
    'mapping_info':fields.Nested(mapping_data_postinfo),
    'detail_info':fields.Nested(metainfo_detail_bulk)
})

#creating the model for the log table 
file_uploadlog_model=api.model('base_uploadlog_model',{
        'uploadlog_id':fields.Integer,
        'name':fields.String,
        'description':fields.String,
        'upload_filename':fields.String,
        'upload_filename':fields.String,
        'filesize':fields.String,
        'layout_id':fields.Integer,
        'uploaded_by':fields.Integer,
        'layout_file':fields.Boolean,
        'dataset_file':fields.Boolean,
        'start_time':fields.DateTime,
        'remarks':fields.String,
        'uploader_info':fields.Nested(user_model),
        'metadata_info':fields.Nested(metainfo_get)
})

#creating models for dataset information
dataset_model=api.model('base_dataset_model',{
    'dataset_id':fields.Integer,
    'dataset_name':fields.String,
    'dataset_detail':fields.Integer,
    'upload_log_details':fields.Nested(file_uploadlog_model)})

dataset_filter_get = api.model('dataset_filter_all',{
    'datasetName':fields.String(required=True,description='dataset name to search')
    })

#creating model for metadata verification
metadata_attr_model=api.model('meta_attributes_details',{
    'name':fields.String,
    'type':fields.String,
    'min_length':fields.Integer,
    'max_length':fields.Integer,
    'allowed_lov':fields.List(fields.String),
    'field_order':fields.Integer,
    'regex_val':fields.String,
    'null_allowed':fields.Boolean
})

metadata_preverification_model=api.model('metadata_preverfication_model',{
    'metadata_name':fields.String,
    'metadata_description':fields.String,
    'headerAtRow':fields.Integer,
    'footerRowCount':fields.Integer,
    'metadata_details':fields.List(fields.Nested(metadata_attr_model,skip_none=True))
    })