from config.socketDispatcher import handle_dataset_message
from .datacleaner import file_cleaner
from .datadetector import generate_metadata_from_dataframe
from .datavalidator import initiateDataLevelValidation,initiateColumnOrderValidation,initiateRowLimitValidation
from datetime import datetime
from genericservices.ConcurrencyManager import ConcurrentModuleProcessorRegistration
import pandas as pd
import math,json


def process_excel_for_metadata(file_io,file_type,headeronTop,headeronRow=0,footerRows=0,encodingUsed='utf-8',engineparser='xlrd'):
    # total_metadata_sheets=pd.ExcelFile(myfile).sheet_names
    total_metadata_sheets=['sheet1']
    meta_data_pre_verification_list=[] 
    try:
        for sheetindex in range(len(total_metadata_sheets)):
            refine_df_from_sheet=file_cleaner(file_io,file_type,sheet_to_process=sheetindex,header_on_top=headeronTop,header_row=headeronRow,footer_row=footerRows,encodingUsed=encodingUsed,parsingengine=engineparser)
            print(refine_df_from_sheet.head(5))
            meta_data_pre_verification_list=generate_metadata_from_dataframe(refine_df_from_sheet,dataencoding=encodingUsed)
        
        return meta_data_pre_verification_list
    except Exception as exce:
        raise Exception('Error during the excel processing '+exce.__str__())


def process_csv_for_metadata(file_io,file_type,headeronTop,seperator=',',headeronRow=0,footerRows=0,encodingUsed='utf-8'):
    try:
        meta_data_pre_verification_list=[]
        refine_df_from_sheet=file_cleaner(file_io,file_type,seperator=seperator,header_on_top=headeronTop,header_row=headeronRow,footer_row=footerRows,encodingUsed=encodingUsed)
        print(refine_df_from_sheet.head(5))
        meta_data_pre_verification_list=generate_metadata_from_dataframe(refine_df_from_sheet,dataencoding=encodingUsed)
        return meta_data_pre_verification_list
    except Exception as exce:
        raise Exception('Error during the csv file processing '+exce.__str__())

def perform_dataqc_for_csv(file_io,file_type,headeronTop,metadata_details,qc_logs,seperator=',',headeronRow=0,footerRows=0,encodingUsed='utf-8',LimitedFileSize=True):
    LimitedFileSize=False
    qc_logs.append({'type':'SUCCESS','message':'Data Fetching Initiated for the file','logtime':str(datetime.now())})
    handle_dataset_message({'type':'SUCCESS','message':'Data Fetching Initiated for the file'})
    refine_df_from_sheet=file_cleaner(file_io,file_type,seperator=seperator,header_on_top=headeronTop,header_row=headeronRow,footer_row=footerRows,encodingUsed=encodingUsed)
    print(refine_df_from_sheet.head(5))
    qc_logs.append({'type':'SUCCESS','message':'Data Fetching Completed for the file','logtime':str(datetime.now())})
    handle_dataset_message({'type':'SUCCESS','message':'Data Fetching Completed for the file'})
    #now we have to initaiate the validation for the dataframe
    column_name_List_required = [metadata.attrIdentifier for metadata in metadata_details]
    column_order_validation=initiateColumnOrderValidation(refine_df_from_sheet,column_name_List_required)
    if column_order_validation:
        if isinstance(column_order_validation,list):
            defectMsg = 'Column order violated by '+str(column_order_validation)
        else:
            defectMsg=column_order_validation
                
        return {'message':defectMsg,
        'defect':'',
        'tips':'Expected column order is: '+str(column_name_List_required),
        'Log_list':qc_logs
        }
    qc_logs.append({'type':'SUCCESS','message':'Column Ordering Validation Succeded','logtime':str(datetime.now())})
    handle_dataset_message({'type':'SUCCESS','message':'Column Ordering Validation Succeded'})
    if LimitedFileSize:
        row_limit_validation = initiateRowLimitValidation(refine_df_from_sheet,qc_logs)
        if row_limit_validation['message'] is not None:
            return {'message':row_limit_validation['message'],
            'defect':'',
            'tips':'Please Upload the file containing rows in defined limit',
            'Log_list':row_limit_validation['Log_list']
            }
        qc_logs=row_limit_validation['Log_list']
    if refine_df_from_sheet.shape[0]>5000: #go with the concurrent processsing methodology
            try:
                dataset_QC_result=doParallelDFQCProcessing(dataframe_obj=refine_df_from_sheet,metadata_details_toValidate=metadata_details,existing_QCLogs=qc_logs,dataframe_headerno=headeronRow)
            except Exception as exc:
                print('Cannot process Via concurrent methods')
                print(exc)
                #go with the normal processing methodology.
                #now we have to initaiate the validation for the dataframe
                dataset_values_to_validate = list(refine_df_from_sheet.apply(lambda x: list(x),axis=1))
                dataset_QC_result=initiateDataLevelValidation(dataset_values_to_validate,metadata_details,qc_logs,headeronRow=headeronRow)

    else:
        #go with the normal processing methodology.
        #now we have to initaiate the validation for the dataframe
        dataset_values_to_validate = list(refine_df_from_sheet.apply(lambda x: list(x),axis=1))
        dataset_QC_result=initiateDataLevelValidation(dataset_values_to_validate,metadata_details,qc_logs,headeronRow=headeronRow)

    dataset_QC_result['tips']='Error processing the file:'+str(file_io.filename)
    return dataset_QC_result
    #return value include the exception or error that has arised during validation if any other wise return false

def perform_dataqc_for_excel(file_io,file_type,headeronTop,metadata_details,qc_logs,headeronRow=0,footerRows=0,encodingUsed='utf-8',engineparser='xlrd',LimitedFileSize=True):
    # total_dataset_sheets=pd.ExcelFile(myfile).sheet_names
    total_dataset_sheets=['sheet1']
    dataset_QC_result=None
    LimitedFileSize=False
    for sheetindex in range(len(total_dataset_sheets)):
        qc_logs.append({'type':'SUCCESS','message':'Data Fetching Initiated for the file'+str(total_dataset_sheets[sheetindex]),'logtime':str(datetime.now())})
        handle_dataset_message({'type':'SUCCESS','message':'Data Fetching Initiated for the file: '+str(total_dataset_sheets[sheetindex])})
        refine_df_from_sheet=file_cleaner(file_io,file_type,sheet_to_process=sheetindex,header_on_top=headeronTop,header_row=headeronRow,footer_row=footerRows,encodingUsed=encodingUsed,parsingengine=engineparser)
        print('dataframe received is ')
        print(refine_df_from_sheet.head(5))
        qc_logs.append({'type':'SUCCESS','message':'Data Fetching Completed for the file'+str(total_dataset_sheets[sheetindex]),'logtime':str(datetime.now())})
        handle_dataset_message({'type':'SUCCESS','message':'Data Fetching Completed for the file: '+str(total_dataset_sheets[sheetindex])})
        column_name_List_required = [metadata.attrIdentifier for metadata in metadata_details]
        column_order_validation=initiateColumnOrderValidation(refine_df_from_sheet,column_name_List_required)
        if column_order_validation:
            if isinstance(column_order_validation,list):
                defectMsg = 'Oder violated by '+str(column_order_validation)
            else:
                defectMsg=column_order_validation

            dataset_QC_result={'message':'Column Order Validation failed on sheet '+str(sheetindex+1),
            'defect':defectMsg,
            'tips':'Expected column order is: '+str(column_name_List_required),
            'Log_list':qc_logs
            }
            break
        qc_logs.append({'type':'SUCCESS','message':'Column Ordering Validation Succeded::'+str(total_dataset_sheets[sheetindex]),'logtime':str(datetime.now())})
        handle_dataset_message({'type':'SUCCESS','message':'Column Ordering Validation Succeded::'+str(total_dataset_sheets[sheetindex])})
        if LimitedFileSize:
            row_limit_validation = initiateRowLimitValidation(refine_df_from_sheet,qc_logs)
            if row_limit_validation['message'] is not None:
                return {'message':row_limit_validation['message'],
                'defect':'Error processing sheet:'+str(sheetindex+1),
                'tips':'Please Upload the file containing rows in defined limit',
                'Log_list':row_limit_validation['Log_list']
                }
                break
            qc_logs=row_limit_validation['Log_list']

        if refine_df_from_sheet.shape[0]>5000: #go with the concurrent processsing methodology
            try:
                validation_result=doParallelDFQCProcessing(dataframe_obj=refine_df_from_sheet,metadata_details_toValidate=metadata_details,existing_QCLogs=qc_logs,dataframe_headerno=headeronRow)
            except Exception as exc:
                print('Cannot process Via concurrent methods')
                print(exc)
                #go with the normal processing methodology.
                #now we have to initaiate the validation for the dataframe
                dataset_values_to_validate = list(refine_df_from_sheet.apply(lambda x: list(x),axis=1))
                validation_result=initiateDataLevelValidation(dataset_values_to_validate,metadata_details,qc_logs,headeronRow=headeronRow)

        else:
            #go with the normal processing methodology.
            #now we have to initaiate the validation for the dataframe
            dataset_values_to_validate = list(refine_df_from_sheet.apply(lambda x: list(x),axis=1))
            validation_result=initiateDataLevelValidation(dataset_values_to_validate,metadata_details,qc_logs,headeronRow=headeronRow)
        
               
        if validation_result['message']:#return value include the exception or error that has arised during validation if any other wise return false
            dataset_QC_result= validation_result
            dataset_QC_result['tips']='Error processing sheet:'+str(sheetindex+1)
            break
        else:
            dataset_QC_result= validation_result
    
    return dataset_QC_result
            
def dataframeSlicer(dataframe,no_of_slice):
    if not isinstance(dataframe,pd.DataFrame):
        return "No a DataFrame object Received."
    if not isinstance(no_of_slice,int):
        return "Invalid value for the slice number."
    try:
        last_row=dataframe.shape[0]
        chunk_size=math.ceil(dataframe.shape[0]/no_of_slice)
        slice_border = [ i for i in range(0,last_row,chunk_size)]
        slice_border.append(last_row)
        df_slices=[]
        for i in range(len(slice_border)-1):
            data=dataframe[slice_border[i]:slice_border[i+1]]

            df_slices.append({'data':data,'ActualrowNum':slice_border[i]})
        return df_slices
    except Exception as exc:
        raise Exception('Exception during the slicing'+str(exc))

def doParallelDFQCProcessing(dataframe_obj,metadata_details_toValidate,dataframe_headerno,existing_QCLogs=[],slices=8):
    try:
        pram_list = [[list(data['data'].apply(lambda x: list(x),axis=1)),metadata_details_toValidate,[],(data['ActualrowNum']+dataframe_headerno)] for data in dataframeSlicer(dataframe=dataframe_obj,no_of_slice=slices)]
        register_dataqc_process = ConcurrentModuleProcessorRegistration(module_block=initiateDataLevelValidation,module_params=pram_list,execution_log_dumpAt='./filestorage/tempfile')
        validation_reports=register_dataqc_process.initiateProcesses()
        total_issues=0
        all_logs=[]
        if isinstance(validation_reports,str):
            with open(validation_reports,'r') as temp_log_file:
                for line in temp_log_file:
                    report=json.loads(line)[0]
                    total_issues=total_issues+report['issueCount']
                    for log in report['Log_list']:
                        all_logs.append(log)   
        else:
            for report in validation_reports:
                total_issues=total_issues+report['issueCount']
                for log in report['Log_list']:
                    all_logs.append(log)        

        if total_issues==0:
            validation_result={'message':None, 'Log_list':existing_QCLogs+all_logs}
        else:
            validation_result={'message':'QC Failed::Total issue(s) in file are: '+str(total_issues), 'Log_list':existing_QCLogs+all_logs}
        
        return validation_result
    except Exception as ex:
        raise Exception('parallel processing error'+str(ex))


