import pandas as pd

def file_cleaner(file_io,file_type,seperator=',',sheet_to_process=0,header_on_top=False,header_row=0,footer_row=0,encodingUsed='utf-8',parsingengine='xlrd'):
    try:
        if header_row !=0:
            raw_df_from_mid=excel_reader(file_io,sheet_to_process=sheet_to_process,header_row=header_row-1,encodingUsed=encodingUsed,parserengine=parsingengine) if (file_type in ['xlsx','xlsb']) else csv_reader(file_io,seperator_used=seperator,header_row=header_row-1,encodingUsed=encodingUsed)
            print(raw_df_from_mid.head(5))
            raw_df_from_mid=raw_df_from_mid.drop(raw_df_from_mid.columns[raw_df_from_mid.columns.str.contains('unnamed',case=False,na=False)],axis=1)
            raw_df_from_mid=raw_df_from_mid.dropna(how='all')
            raw_df_from_mid.columns = raw_df_from_mid.columns.fillna('to_drop')
            try:
                raw_df_from_mid.drop('to_drop', axis = 1,inplace=True)
            except KeyError as exc:
                pass
            # raw_df_from_mid=raw_df_from_mid.dropna(how='all',axis=1)
            if footer_row:
                raw_df_from_mid=raw_df_from_mid.drop(raw_df_from_mid.tail(footer_row).index)
            return raw_df_from_mid
        else:
            raw_df = excel_reader(file_io,sheet_to_process=sheet_to_process,header_row=None,encodingUsed=encodingUsed,parserengine=parsingengine) if (file_type in ['xlsx','xlsb']) else csv_reader(file_io,seperator_used=seperator,header_row=None,encodingUsed=encodingUsed)
            print(raw_df.head(5))
            raw_df.dropna(how="all",inplace=True)
            raw_df.columns = raw_df.columns.fillna('to_drop')
            try:
                raw_df.drop('to_drop', axis=1,inplace=True)
            except KeyError as exc:
                pass
            # raw_df=raw_df.dropna(how='all',axis=1)
            raw_df=raw_df.reset_index().drop(columns='index')
            if raw_df.empty:
                return None #empty sheet received
            
            if footer_row:
                raw_df=raw_df.drop(raw_df.tail(footer_row).index)

            if header_on_top:
                try:
                    raw_df.columns=raw_df.iloc[0]
                except IndexError as ind:
                    pass
                except Exception as ex:
                    print('exception occured '+ex.__str__())
                
            raw_df.drop(index=0,inplace=True)
    except Exception as exc:
        raise Exception('Error during the data cleaning for the File '+str(exc))
    return raw_df

def excel_reader(file_io,sheet_to_process=0,header_row=None,footer_row=0,encodingUsed='utf-8',parserengine='xlrd'):
    try:
        return pd.read_excel(file_io,sheet_name=sheet_to_process, header=header_row,skipfooter=footer_row,encoding=encodingUsed,engine=parserengine)
    except Exception as ex:
        raise Exception('Cannot read your excel file'+str(ex))

def csv_reader(file_io,seperator_used=',',header_row=None,footer_row=0,encodingUsed='utf-8'):
    try:
        return pd.read_csv(file_io,sep=seperator_used,header=header_row,skipfooter=footer_row,encoding=encodingUsed)
    except Exception as ex:
        raise Exception('Cannot read your csv file'+str(ex))


