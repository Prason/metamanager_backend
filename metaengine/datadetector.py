from config import inbuilt_predefined_basic_types
import re,datetime
import pandas as pd
from metaengine.regexengine import RegexHelper

def generate_metadata_from_dataframe(dataframe_to_process,dataencoding='utf-8'):
    # dataframe_to_process=dataframe_to_process.head(50000)
    datatypelist_expected = dataframe_to_process.dtypes
    metadata_list=[]
    try:
        if sum([int(data) for data in list(dataframe_to_process.columns)]):
            hasHeaderonTop=False
    except:
            hasHeaderonTop=True
    try:
        for col_name_received,col_type_received in datatypelist_expected.items():
            try:
                identified_attributes={}
                if str(col_type_received) in inbuilt_predefined_basic_types:
                    if str(col_type_received).find('int')==0:
                        series_obj = dataframe_to_process[col_name_received]
                        if isinstance(series_obj,pd.DataFrame):
                            series_obj= series_obj.iloc[:,0]
                        get_value_counts_index_list=list(series_obj.value_counts().index)
                        if len(get_value_counts_index_list)==2:#possibility of the boolean
                            get_value_counts_index_list.sort()
                            if get_value_counts_index_list==[0,1]:
                                col_type_finalized='BOOLEAN'
                            else:
                                col_type_finalized='NUMERIC'
                        else:
                            col_type_finalized='NUMERIC'
                            
                    elif str(col_type_received).find('float')==0:
                        series_obj = dataframe_to_process[col_name_received]
                        if isinstance(series_obj,pd.DataFrame):
                            series_obj= series_obj.iloc[:,0]
                        get_value_counts_index_list=list(series_obj.value_counts().index)
                        if len(get_value_counts_index_list)==2:#possibility of the boolean
                            get_value_counts_index_list.sort()
                            if get_value_counts_index_list==[0,1]:
                                col_type_finalized='BOOLEAN'
                            else:
                                col_type_finalized='FLOAT'
                                
                        elif len(list(series_obj.dropna()))==0:
                            col_type_finalized='TEXT'
                        else:
                            types_value = series_obj.dropna().apply(lambda x: identifytype(x))
                            try:
                                col_type_finalized=types_value.value_counts().idxmax()
                            except Exception as exc:
                                print(exc.__str__())
                                col_type_finalized='TEXT'                       

                    elif str(col_type_received).find('bool')==0:
                        col_type_finalized='BOOLEAN'
                    elif str(col_type_received).find('datetime')==0:
                        col_type_finalized='DATETIME'
                    else:
                        col_type_finalized='TEXT'
                        
                    series_obj = dataframe_to_process[col_name_received]
                else:
                    series_obj = dataframe_to_process[col_name_received]
                    if isinstance(series_obj,pd.DataFrame):
                        series_obj= series_obj.iloc[:,0]

                    types_value = series_obj.dropna().apply(lambda x: identifytype(x))
                    try:
                        col_type_finalized=types_value.value_counts().idxmax()
                    except Exception as exc:
                        print(exc.__str__())
                        col_type_finalized='TEXT'

                #now we also generate the additional properties of the column based on the datatype identified
                identified_attributes=derieve_additional_attributes(col_type_finalized,series_obj)
                if hasHeaderonTop:
                    col_name_finalized=col_name_received
                else:
                    col_name_finalized='column_'+str(col_name_received)
                identified_attributes['name']=col_name_finalized
                identified_attributes['field_order']=len(metadata_list)+1
                metadata_list.append(identified_attributes)
            except Exception as ex:
                raise Exception('Error during processing file content on column: '+str(col_name_received)+' type of :'+col_type_finalized+':>'+ex.__str__())
    except Exception as exc:
        print('Iteration '+exc.__str__())

    return metadata_list

def derieve_additional_attributes(datatype_value,column_data):
    # 'NUMERIC','FLOAT','TEXT','DATETIME','DATE','ENUM','ARRAY','BOOLEAN','IPADDRESS','EMAIL','URL'
    properties={'type':datatype_value}
    regexhelper= RegexHelper()
    if column_data.isna().sum()>=1:
         properties['null_allowed']=True
    
    if datatype_value in ['NUMERIC','FLOAT']:
        #for numeric and floating type
        try:
            column_data=column_data.apply(lambda x : pd.to_numeric(x,'coerce'))
            max_val=str(column_data.max()).split('.')
            min_val=str(column_data.min()).split('.')
        except Exception as ex:
            print('except'+ex.__str__())
            max_val = str(max([x for x in column_data if isinstance(x,int)])).split('.') if datatype_value=='NUMERIC' else str(max([x for x in column_data if isinstance(x,float)])).split('.')
            min_val = str(min([x for x in column_data if isinstance(x,int)])).split('.') if datatype_value=='NUMERIC' else str(min([x for x in column_data if isinstance(x,float)])).split('.')
        max_val.append('0')
        min_val.append('0')
        if (max_val[0] in ['1','0'] and min_val[0]=='0'):
            properties['regex_val']='[01]\.[0-9]{1,}' if datatype_value=='FLOAT' else '[01]{1,}'

        if max_val[1]=='0':
            max_length=len(max_val[0])
            properties['max_length']=max_length
        else:
            max_length=False          
        if min_val[1]=='0':
            min_length=len(min_val[0])
            properties['min_length']=min_length
        else:   
            min_length=False         
        if max_length==min_length:
            if max_length:
                properties['regex_val']='[0-9]{'+str(max_length)+'}(\.[0])?' if datatype_value in ['NUMERIC'] else '[0-9]{'+str(max_length)+'}\.[0-9]{1,2}'
        
        if 'max_length' not in properties.keys():
            properties['max_length']=100

        #identify LOV if any
        index_list = list(column_data.value_counts().index)
        if len(index_list)<10 and len(index_list)!=0:
            cast_to_num=[]
            for v in index_list:
                if v==int(v):
                    cast_to_num.append(int(v))
                else:
                    cast_to_num.append(v)
                    
            index_list=cast_to_num
            properties.update({'type':'ENUM'})
            properties['allowed_lov']=index_list

    elif datatype_value=='IPADDRESS':
        properties['regex_val']=regexhelper.get_ipPattern()
    elif datatype_value=='BOOLEAN':
        index_list = list(column_data.value_counts().index)
        properties['regex_val']='('+'|'.join([str(data) for data in index_list])+')'
            
    elif datatype_value=='EMAIL':
        properties['regex_val']=regexhelper.get_emailPattern()
    elif datatype_value=='URL':
        properties['regex_val']=regexhelper.get_urlPattern()
    elif datatype_value=='ARRAY':
        properties['regex_val']=regexhelper.get_textArrayPattern()
    elif datatype_value=='TEXT':
        len_series=column_data.apply(lambda x: len(str(x)))
        max_length_text=len_series.max()
        min_length_text=len_series.min()
        properties['min_length']=int(min_length_text)
        properties['max_length']=20 if max_length_text<20 else 50 if max_length_text<50 else 100 if max_length_text<100 else 250 if max_length_text<250 else 500 if max_length_text<500 else 1000 if max_length_text<1000 else 2000 if max_length_text<2000 else 5000 if max_length_text<5000 else 10000
        #identify LOV if any
        index_list = list(column_data.value_counts().index)
        if len(index_list)<10 and len(index_list)!=0:
            properties.update({'type':'ENUM'})
            properties['allowed_lov']=index_list
        else:#if the value is not LOV then identify the pattern in this 
            #possible_regex=column_data.apply(lambda x: regexhelper.determineTextRegexLevel(x))
            # properties['regex_val']=possible_regex.value_counts().idxmax()
            properties['regex_val']='.+'

    return properties

def identifytype(value):
    try:
        if isinstance(value,bool):
                return 'BOOLEAN'
        elif isinstance(value,int):
                return 'NUMERIC'
        elif isinstance(value,float):
            try:
                return 'NUMERIC' if int(value)==value else 'FLOAT'
            except:
                return 'TEXT'
        elif (isinstance(value,datetime.datetime)):
                return 'DATETIME'
        elif (isinstance(value,datetime.date)):
                return 'DATE'
        elif (re.fullmatch(r'(yes|no|true|false)',str(value).lower())):
                return 'BOOLEAN'
        elif re.fullmatch(r'[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}',str(value)):
                return 'IPADDRESS'
        elif re.fullmatch(r'[A-Za-z0-9]+[\._]?[A-Za-z0-9]*@[a-zA-Z0-9]+\.[a-zA-Z]{2,3}',str(value)):
                return 'EMAIL'
        elif re.fullmatch(r'[a-zA-Z]{4,6}://.+/?',str(value)):
                return 'URL'
        elif re.match(r'[a-zA-Z0-9 @\._]+[,;:|]',str(value)):   
            if len(re.sub(r'[a-zA-Z0-9 @._]+[,;:|]','',str(value)+','))==0:
                return 'ARRAY'            
        elif re.fullmatch(r'[0-9]+\.[0-9]+',str(value)):
                return 'FLOAT'
        elif re.fullmatch(r'[0-9]+',str(value)):
                return 'NUMERIC'
        elif len(re.sub(r'(([0-9]{1,2}(\\|-|\/)[0-9]{1,2}\3[0-9]{4})|([0-9]{4}(\\|-|\/)[0-9]{1,2}\5[0-9]{1,2})) [0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?','',str(value)))==0:
                return 'DATETIME'
        elif len(re.sub(r'(([0-9]{1,2}(\\|-|\/)[0-9]{1,2}\3[0-9]{4})|([0-9]{4}(\\|-|\/)[0-9]{1,2}\5[0-9]{1,2}))','',str(value)))==0:
                return 'DATE'
        else:
                return 'TEXT'
        #All these regex refereence has been used in the daata QC part also.
    except Exception as ex:
        print(ex.__str__)
        raise Exception('exception during data identify: '+ex.__str__)