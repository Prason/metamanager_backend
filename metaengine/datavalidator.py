import re,datetime
import pandas as pd
#from config.socketDispatcher import handle_dataset_message

#'NUMERIC','FLOAT','TEXT','DATETIME','DATE','ENUM','ARRAY','BOOLEAN','IPADDRESS','EMAIL','URL'
def initiateDataLevelValidation(datasetList,metainfoList,qc_logs=[],headeronRow=0):
    
    defect_report=[]
    row_number=headeronRow+1
    for item in datasetList:
        #print('processing for '+str(item))
        if not defect_report:
            if (row_number%100)==0:
                qc_logs.append({'type':'SUCCESS','message':'QC completed for '+str(row_number)+' row(s)','logtime':str(datetime.datetime.now())})
                #handle_dataset_message({'type':'SUCCESS','message':'QC completed for '+str(row_number)+' row(s)'})

        for index in range(len(metainfoList)):
            result={}
            metadetail=metainfoList[index]
            isNullAllowed=metadetail._attrNull
            minLengthAllowed=metadetail.attrMinLength
            maxLengthAlllowed=metadetail.attrMaxLength
            patternToValidate=metadetail.atrrRegex
            if (item[index] is None) or (pd.isna(item[index])) or (pd.isnull(item[index])):
                if isNullAllowed:
                    continue
                else:
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Empty Value is not permitted in column \'\''+metadetail.attrIdentifier+'\'\', Must have some <'+metadetail._attrType+'> data.',
                                    'defect':'On Row '+str(row_number)+' | value :\''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            if(metadetail._attrType=='FLOAT'):
                try:
                    required_digits=' upto '+str(maxLengthAlllowed)+' digit(s) before decimal' if maxLengthAlllowed else ''
                    if patternToValidate:
                        pat_string=patternToValidate
                    else:
                        pat_string=getPatternForFloat(minLengthAllowed,maxLengthAlllowed)['patternIs']
                    if(re.sub(pat_string,'',str(item[index]))):
                        if(str(item[index]).lower().count('e')):
                            if not (re.sub(pat_string,'',str(format(item[index],'.5f')))):
                                continue #data is correct so continue
                        pat_string=getPatternForNumberic(minLengthAllowed,maxLengthAlllowed)['patternIs']
                        if(re.sub(pat_string,'',str(item[index]))):  
                            result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Decimal Data on column \'\''+metadetail.attrIdentifier+'\'\' '+required_digits+' of pattern '+str(pat_string),
                                        'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                            #handle_dataset_message(result)
                            defect_report.append(result)

                except Exception as excep:
                    print('from Decimal except')
                    print(excep.__str__)
                    if(re.sub(getPatternForNumberic(minLengthAllowed,maxLengthAlllowed)['patternIs'],'',str(item[index]))):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Decimal Data on column \''+metadetail.attrIdentifier+'\' '+required_digits+' of pattern '+str(pat_string),
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

            elif(metadetail._attrType=='NUMERIC'):
                try:
                    required_digits='upto '+str(maxLengthAlllowed)+' digit(s) ' if maxLengthAlllowed else ''
                    
                    if patternToValidate:
                        pat_string=patternToValidate
                    else:
                        pat_info=getPatternForNumberic(minLengthAllowed,maxLengthAlllowed)
                        pat_string=pat_info['patternIs']
                        required_digits=pat_info['tips']
                    
                    try:
                        converted_val=format(item[index],'.0f')
                        item[index]=converted_val if converted_val==item[index] else item[index]
                    except:
                        pass                    
                    if not (re.fullmatch(pat_string,str(item[index]))):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Numeric Data ('+str(pat_string)+') on column \''+metadetail.attrIdentifier+'\' | '+required_digits+' ',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)
                     
                except Exception as excep:
                    print('from numeric except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Numeric Data ('+str(pat_string)+') on column \''+metadetail.attrIdentifier+'\' | '+required_digits+' ',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='DATETIME'):
                try:
                    pat_string=r'(([0-9]{1,2}(\\|-|\/)[0-9]{1,2}\3[0-9]{4})|([0-9]{4}(\\|-|\/)[0-9]{1,2}\5[0-9]{1,2})) [0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?'
                    if(isinstance (item[index],datetime.datetime)):
                        pass #it is correct data
                    elif(re.sub(pat_string,'',str(item[index]))):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid date-time format  on column \''+metadetail.attrIdentifier+'\' [/,-,\] are allowed on yy-mm-dd H:M:S format,   ',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)
                        

                except Exception as excep:
                    print('from datetime except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid date-time format \''+metadetail.attrIdentifier+'\' [/,-,\] are allowed on yy-mm-dd H:M:S format,   ',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='TEXT'):
                #anything to upload is allowed under the text field
                try:
                    data_length=len(str(item[index]))
                    minLenAlllowed=minLengthAllowed if minLengthAllowed  else 1
                    maxLenAlllowed=maxLengthAlllowed if maxLengthAlllowed else data_length
                    if minLenAlllowed or maxLenAlllowed:
                        if((data_length>maxLenAlllowed) or  (data_length<minLenAlllowed) ):
                            result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Data length is not appropriate for column: \''+metadetail.attrIdentifier+'\' '+str(minLenAlllowed)+' upto '+str(maxLenAlllowed)+' characters only allowed.',
                                            'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                            #handle_dataset_message(result)
                            defect_report.append(result)

                    if patternToValidate != '':
                        if not re.fullmatch(re.compile(patternToValidate, re.MULTILINE | re.DOTALL),str(item[index])):
                            result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Column \''+metadetail.attrIdentifier+'\' unmatched with the defined format: \''+str(patternToValidate)+'\'  ',
                                        'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                            #handle_dataset_message(result)
                            defect_report.append(result)

                except  Exception as excep:
                    print('from textual except')
                    # print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Column \''+metadetail.attrIdentifier+'\' unmatched with the defined format: \''+str(patternToValidate)+'\'  ',
                                        'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='DATE'):
                try:
                    pat_string=r'(([0-9]{1,2}(\\|-|\/)[0-9]{1,2}\3[0-9]{4})|([0-9]{4}(\\|-|\/)[0-9]{1,2}\5[0-9]{1,2}))'
                    if(isinstance (item[index],datetime.datetime)):
                        pass #it is correct data
                    elif(re.sub(pat_string,'',str(item[index]))):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid date format on column \''+metadetail.attrIdentifier+'\' [/,-,\] are allowed on yy-mm-dd format,   ',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from date except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid date format on column \''+metadetail.attrIdentifier+'\' [/,-,\] are allowed on yy-mm-dd format,   ',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='ENUM'):
                try:
                    for value in list(metadetail.attrLOV):
                        try:
                            item[index] = int(item[index]) if item[index]==int(item[index]) else item[index]
                        except:
                            pass
                        if value==str(item[index]):
                            continue

                    if str(item[index]) not in list(metadetail.attrLOV):

                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Value found at column \''+metadetail.attrIdentifier+'\', ENUMs are predefined values from => '+str(metadetail.attrLOV)+'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from ENUM except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Value found at column \''+metadetail.attrIdentifier+'\', ENUMs are predefined values from => '+str(metadetail.attrLOV)+'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='ARRAY'):
                try:
                    pat_string = patternToValidate if patternToValidate else r'[a-zA-Z0-9 @._]+[,;:|]'
                    if len(re.sub(pat_string,'',str(item[index])+','))!=0:
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Not a Array value of pattern '+str(pat_string)+' at column \''+metadetail.attrIdentifier+'\'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from Array except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Not a Array value of pattern '+str(pat_string)+' at column \''+metadetail.attrIdentifier+'\'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='BOOLEAN'):
                try:
                    pat_to_check= patternToValidate if patternToValidate else r'(0|1|yes|no|true|false|0.0|1.0)'
                    if metadetail.attrLOV:
                        if str(item[index]) not in list(metadetail.attrLOV):
                            result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Not a boolean value at column \''+metadetail.attrIdentifier+'\', expected => '+str(metadetail.attrLOV)+'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                            #handle_dataset_message(result)
                            defect_report.append(result)
                    
                    elif not (re.fullmatch(pat_to_check,str(item[index]))):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Not a boolean value at column \''+metadetail.attrIdentifier+'\'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from boolean except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Not a boolean value at column \''+metadetail.attrIdentifier+'\'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='IPADDRESS'):
                try:
                    pat_string=r'[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}'
                    if not re.fullmatch(pat_string,str(item[index])):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid IP at column \''+metadetail.attrIdentifier+'\'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from Ip address except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid IP at column \''+metadetail.attrIdentifier+'\'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='EMAIL'):
                try:
                    pat_string=r'[A-Za-z0-9]+[\._]?[A-Za-z0-9]*@[a-zA-Z0-9]+\.[a-zA-Z]{2,3}'
                    if not re.fullmatch(pat_string,str(item[index])):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Email at column \''+metadetail.attrIdentifier+'\'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from email except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid Email at column \''+metadetail.attrIdentifier+'\'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

            elif(metadetail._attrType=='URL'):
                try:
                    pat_string=r'[a-zA-Z]{4,6}://.+/?'
                    if not re.fullmatch(pat_string,str(item[index])):
                        result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid URL at column \''+metadetail.attrIdentifier+'\'',
                                    'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                        #handle_dataset_message(result)
                        defect_report.append(result)

                except Exception as excep:
                    print('from url except')
                    print(excep.__str__)
                    result.update({'type':'FAIL','logtime':str(datetime.datetime.now()),'message':'Invalid URL at column \''+metadetail.attrIdentifier+'\'',
                                'defect':'On Row '+str(row_number)+' | value : \''+str(item[index])+'\''})
                    #handle_dataset_message(result)
                    defect_report.append(result)

        row_number+=1 
    print(f'completed {row_number}')
    if not defect_report:
        result.update({'issueCount':0,'message':None, 'Log_list':qc_logs}) # return empty dictionary when everthing is fine
    else:
        final_logs=qc_logs + defect_report
        result.update({'issueCount':len(defect_report),'message':'QC Failed::Total issue(s) in file are: '+str(len(defect_report)), 'Log_list':final_logs})
    return result

def initiateColumnOrderValidation(dataframe_toValidate,column_list):
    try:
        return list(dataframe_toValidate.columns[dataframe_toValidate.columns != column_list])

    except Exception as excep:
        error_string = str(excep.__str__()).rsplit(':',1)[1]
        if error_string:
            return 'Columns count mismatched : '+str(error_string)
        else:
            return 'Columns count mismatched : '+str(excep.__str__())
    #this will return the name of the all he columns whose name mismatch has occured

def initiateRowLimitValidation(dataframe_toValidate,qc_logs):
    max_row_bound=50000
    row_found=dataframe_toValidate.shape[0]
    if row_found>max_row_bound:
        qc_logs.append({'type':'FAIL','message':'Row Limit exceeded :'+str(row_found),'logtime':str(datetime.datetime.now())})
        #handle_dataset_message({'type':'FAIL','message':'Row Limit exceeded :'+str(row_found)})
        return {'message':'Maximum row Limit exceded for the data. Found '+str(row_found)+', Expected '+str(max_row_bound),'Log_list':qc_logs}
    qc_logs.append({'type':'SUCCESS','message':'Data is within expected '+str(max_row_bound)+' row(s) Limit :'+str(row_found),'logtime':str(datetime.datetime.now())})
    #handle_dataset_message({'type':'SUCCESS','message':'Data is within expected '+str(max_row_bound)+' row(s) Limit :'+str(row_found)})
    return {'message':None,'Log_list':qc_logs} #none means no rules are broke
    

def getPatternForFloat(minLengthAllowed,maxLengthAlllowed):
    required_digits=''
    if minLengthAllowed and maxLengthAlllowed:
        pat_string=r'[-+]?[0-9]{'+str(minLengthAllowed)+','+str(maxLengthAlllowed)+'}\.[0-9]+'
    elif minLengthAllowed:
        pat_string=r'[-+]?[0-9]{'+str(minLengthAllowed)+',}\.[0-9]+'
    elif maxLengthAlllowed:
        pat_string=r'[-+]?[0-9]{1,'+str(maxLengthAlllowed)+'}\.[0-9]+'
    else:
        pat_string=r'[-+]?[0-9]+\.[0-9]+'
    
    return {'tips':required_digits,'patternIs':pat_string}


def getPatternForNumberic(minLengthAllowed,maxLengthAlllowed):
    required_digits=''
    if minLengthAllowed and maxLengthAlllowed:
        required_digits='with '+str(minLengthAllowed)+' to '+str(maxLengthAlllowed)+' digit(s)'
        pat_string=r'[-+]?[0-9]{'+str(minLengthAllowed)+','+str(maxLengthAlllowed)+'}(\.[0])?'
    elif minLengthAllowed:
        required_digits='with minimum '+str(minLengthAllowed)+' digit(s)'
        pat_string=r'[-+]?[0-9]{'+str(minLengthAllowed)+',}(\.[0])?'
    elif maxLengthAlllowed:
        required_digits='with maximum '+str(maxLengthAlllowed)+' digit(s)'
        pat_string=r'[-+]?[0-9]{1,'+str(maxLengthAlllowed)+'}(\.[0])?'
    else:
        pat_string=r'[-+]?[0-9]+(\.[0])?'
    
    return {'tips':required_digits,'patternIs':pat_string}