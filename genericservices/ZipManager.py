from zipfile import ZipFile
import zipfile

class CreateZip(object):
    def __init__(self,zipnamesaveas,filepath=None,file_stream=None):
        self.zipname=str(zipnamesaveas).replace('.','_')+".zip"
        self.filepath=filepath
        self.file_stream=file_stream

    def create(self):
        if  self.filepath is None and self.file_stream is None:
            raise Exception("Missing the File objects for zip creation")
        elif self.filepath is None:
            return self.__createFromStream()
        elif self.file_stream is None:
            return self.__createFromPath()
        else:
            raise Exception("Bad Initialization done for the zip creation")

    def __createFromPath(self):
        zipObj = ZipFile(self.zipname,'w',compression=zipfile.ZIP_DEFLATED,compresslevel=6)
        zipObj.write(filename=self.filepath)
        zipObj.close()
        return zipObj
    def __createFromStream(self):
        zipObj = ZipFile(self.zipname,'w',compression=zipfile.ZIP_DEFLATED,compresslevel=6)
        zipObj.writestr(self.zipname,data=self.file_stream)
        zipObj.close()
        return zipObj



