from os import path
import json
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor,ProcessPoolExecutor

class ConcurrentModuleProcessorRegistration(object):

    def __init__(self,module_block,module_params,execution_log_dumpAt=None,dumping_filename=None):
        self.module_block=module_block
        self.module_params=module_params
        self.execution_log_dumpAt=execution_log_dumpAt
        self.dumping_filename=str(dumping_filename).replace('.','_')+".json" if dumping_filename is not None else None
        if self.execution_log_dumpAt is not None:
            try:
                if not path.isdir(self.execution_log_dumpAt):
                    raise FileNotFoundError("The dumping directory is not defined in the server.")
                if self.dumping_filename is None:
                    self.dumping_filename='log_dump_'+str(round(datetime.now().timestamp()))+'.json'
            except Exception:
                raise Exception("The dump location is incorrectly defined. please verify.")
    
    def initiateProcesses(self):
        results=[]
        print('Total sub-processing '+str(len(self.module_params)))
        if self.execution_log_dumpAt is None:
            with ProcessPoolExecutor() as executor:
                results=executor.map(self.parameterServer,self.module_params)      

            return results
        else:
            with ProcessPoolExecutor() as executor:
                for  qc_batch in self.module_params:
                    executor.submit(self.parameterServer,qc_batch).add_done_callback(self.dumpExecutorResult)

            return self.execution_log_dumpAt+"/"+self.dumping_filename
    
    def initiateThreads(self):
        results=[]
        print('Total sub-processing '+str(len(self.module_params)))
        if self.execution_log_dumpAt is None:
            with ThreadPoolExecutor() as executor:
                results=executor.map(self.parameterServer,self.module_params)
            return results
        else:
            with ThreadPoolExecutor() as executor:
                for  qc_batch in self.module_params:
                    executor.submit(self.parameterServer,qc_batch).add_done_callback(self.dumpExecutorResult)

            return self.execution_log_dumpAt+"/"+self.dumping_filename

    
    def dumpExecutorResult(self,future):    
        try:
            with ThreadPoolExecutor() as tpoolexecutor:
                if tpoolexecutor.submit(self.dumptoFile,[future.result()]).done():
                    del future
        except Exception as ex:
            raise Exception("Exception during dumping result"+str(ex))

    def dumptoFile(self,log):
        with open(self.execution_log_dumpAt+'/'+self.dumping_filename,'a') as f:
            f.write(json.dumps(log))
            f.write('\n')
        
        del log
        gc.collect()

    def parameterServer(self,params):
        return self.module_block(*params)

    

