from cloudstorage.drivers.microsoft import AzureStorageDriver
from cloudstorage.exceptions import NotFoundError
from config import prepare_zip_to_upload,app
from werkzeug.datastructures import FileStorage
from io import BytesIO
from os import path
import json,requests
azure_cred=json.load(open(app.config['CLOUD_CONFIG_JSON_AZURE']))
storage = AzureStorageDriver(account_name=azure_cred['storage_name'],key=azure_cred['storage_key'])

#if the required structure doesnot exist then create it
try:
    datasetImportLogContainer=storage.get_container("dataset-import-log")
except NotFoundError as dilnfe:
    datasetImportLogContainer = storage.create_container("dataset-import-log")

try:
    datasetContainer=storage.get_container("datasets")
except NotFoundError as dsnfe:
    datasetContainer = storage.create_container("datasets")

try:
    layoutsContainer=storage.get_container("layouts")
except NotFoundError as nfe:
    layoutsContainer = storage.create_container("layouts")

try:
    tempFileContainer=storage.get_container("tempfile")
except NotFoundError as nfe:
    tempFileContainer = storage.create_container("tempfile")

try:
    frontLayoutUploadContainer=storage.get_container("front-upload-layouts")
except NotFoundError as nfe:
    frontLayoutUploadContainer = storage.create_container("front-upload-layouts")

try:
    frontDatasetUploadContainer=storage.get_container("front-upload-dataset")
except NotFoundError as nfe:
    frontDatasetUploadContainer = storage.create_container("front-upload-dataset")


#defining the functions for the upload and download to several storage container
def save_dataset_import_log_file_az(file_name):    
    with open(prepare_zip_to_upload(path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+file_name)),'rb') as fname:
        storage.upload_blob(container=datasetImportLogContainer,filename=fname,blob_name=file_name+".zip")
    print('Dataset Import log file successfully uploaded to azure')

def save_dataset_import_file_az(file_name):
    with open(prepare_zip_to_upload(path.abspath(app.config['DATASET_FOLDER']+'\\'+file_name)),'rb') as fname:
        storage.upload_blob(container=datasetContainer,filename=fname,blob_name=file_name+".zip")
    print('Dataset Imported file successfully uploaded  to azure')

def save_metadata_import_file_az(file_name):
    with open(prepare_zip_to_upload(path.abspath(app.config['UPLOAD_FOLDER']+'\\'+file_name)),'rb') as fname:
        storage.upload_blob(container=layoutsContainer,filename=fname,blob_name=file_name+".zip")
    print('Metadata Imported file successfully uploaded  to azure')

def save_temp_file_az(file_name):
    with open(prepare_zip_to_upload((path.abspath('./filestorage/tempfile/'+file_name))),'rb') as fname:
        storage.upload_blob(container=tempFileContainer,filename=fname,blob_name=file_name+".zip")
    print('Temp file successfully uploaded  to azure')

def get_file_from_cloud_container(containerName,filename):
    try:
        filedata=storage.get_blob(container=containerName,blob_name=filename)
        response = requests.get(filedata.generate_download_url())
        return FileStorage(BytesIO(response.content),filename=filename)
    except Exception as exc:
        print(exc)
        return str(exc)

def get_metadata_file_az(file_name):
    return get_file_from_cloud_container(frontLayoutUploadContainer,file_name)

def get_dataset_file_az(file_name):
    return get_file_from_cloud_container(frontDatasetUploadContainer,file_name)
