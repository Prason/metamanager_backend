from config import socket
from flask_socketio import SocketIO,send,emit,join_room,leave_room
from flask import session

#datastructure to maintain the user and session
room_data={}

@socket.on('message',namespace='/')
def handle_message(message):
    print('message received on server '+str(message))
    socket.send('BASE::'+message,namespace='/')
    

@socket.on('message',namespace='/dataset')
def handle_dataset_message(message):
    if session:
        handle_log_room_entry(session.get('_user_id'),message)
        print('Message sent:: '+str(message))
    else:
        print('No user session  detected yet')
        socket.send('-----------------\nNew QC logs \n-----------------',namespace='/dataset')

@socket.on('join',namespace='/dataset')
def handle_log_room_entry(user_id,message):
    try:
        session_id = session.get('_id')
        room_data.update({str(session_id):user_id})
        join_room(str(user_id),sid=session_id,namespace='/dataset')
        print(str(user_id)+' has entered the system')
        socket.send(message,namespace='/dataset',room=str(user_id))
    except:
        pass
   
@socket.on('leave',namespace='/dataset')
def handle_log_room_exit(userdata):
    print(str(session.get('_user_id'))+' has exited the system')
    socket.send(userdata+"has left to server",namespace='/dataset')

