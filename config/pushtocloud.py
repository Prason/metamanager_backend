from google.cloud import storage
from config import app,prepare_zip_to_upload
from os import path
from io import BytesIO
from werkzeug.datastructures import FileStorage

client_storage=storage.Client.from_service_account_json(app.config['CLOUD_CONFIG_JSON_GCP'])
BUCKET_NAME='clorox_metadata_manager'
mybucket=client_storage.get_bucket(BUCKET_NAME)

def save_dataset_import_log_file(file_name):
    filenametosave = mybucket.blob('metadata_files'+app.config['DATASET_IMPORT_LOG_FOLDER'][1:]+'/'+file_name+".zip")
    with open(prepare_zip_to_upload(path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+file_name)),'rb') as fname:
        filenametosave.upload_from_file(fname)
    print('Dataset Import log file successfully uploaded')

def save_dataset_import_file(file_name):
    filenametosave = mybucket.blob('metadata_files'+app.config['DATASET_FOLDER'][1:]+'/'+file_name+".zip")
    with open(prepare_zip_to_upload(path.abspath(app.config['DATASET_FOLDER']+'\\'+file_name)),'rb') as fname:
        filenametosave.upload_from_file(fname)
    print('Dataset Imported file successfully uploaded')

def save_metadata_import_file(file_name):
    filenametosave = mybucket.blob('metadata_files'+app.config['UPLOAD_FOLDER'][1:]+'/'+file_name+".zip")
    with open(prepare_zip_to_upload(path.abspath(app.config['UPLOAD_FOLDER']+'\\'+file_name)),'rb') as fname:
        filenametosave.upload_from_file(fname)
    print('Metadata Imported file successfully uploaded')

def save_temp_file(file_name):
    filenametosave = mybucket.blob('metadata_files/filestorage/tempfile/'+file_name+".zip")
    with open(prepare_zip_to_upload(path.abspath('./filestorage/tempfile/'+file_name)),'rb') as fname:
        filenametosave.upload_from_file(fname)
    print('Temp file successfully uploaded')

def get_file_from_cloud(file_uri):
    try:
        filename_to_fetch = mybucket.blob(file_uri)
        file_Data=filename_to_fetch.download_as_string(timeout=360)
        return FileStorage(BytesIO(file_Data),filename=file_uri.rsplit('/',1)[1])
    except Exception as exc:
        print(exc)
        return str(exc)

def get_metadata_file(file_name):
    return get_file_from_cloud('metadata_files/Frontend_FileUpload/metadata_upload/'+file_name)

def get_dataset_file(file_name):    
    return get_file_from_cloud('metadata_files/Frontend_FileUpload/dataset_upload/'+file_name)

