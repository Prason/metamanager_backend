from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_json import FlaskJSON
from flask_bcrypt import Bcrypt
from sqlalchemy import create_engine,MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker
from flask_restplus import Api,fields
import configparser,os
from datetime import timedelta
from genericservices.ZipManager import CreateZip
#flask_login for user session management
from flask_login import LoginManager,current_user

#flask_socketio for the real time logging output and communication to the client.
from flask_socketio import SocketIO,send,emit
from flask_cors import CORS

#preparing the configuration information for initializing th system
config_data = configparser.RawConfigParser()
print(os.path.join(os.path.dirname(__file__), 'application.properties'))

try:
    if os.getenv('MM_ENV').lower()=='prod':
        config_categrory='PRODDATABASECRED'
    elif os.getenv('MM_ENV').lower()=='uat':
        config_categrory='UATDATABASECRED'
    else:
        config_categrory='DEVDATABASECRED'
except:
    config_categrory='DEVDATABASECRED'
    pass

config_data.readfp(open(os.path.abspath('./application.properties')))
dailect_name=config_data.get(config_categrory,'dailect')
username=config_data.get(config_categrory,'username')
password=config_data.get(config_categrory,'password')
db_ip = config_data.get(config_categrory,'database.ip')
db_port = config_data.get(config_categrory,'database.port')
dbname=config_data.get(config_categrory,'database.name')
schema_name=config_data.get(config_categrory,'database.schema')
secretkey = config_data.get('APPCONFIGURATIONS','secretKey')
sessionTimeout=config_data.getint('APPCONFIGURATIONS','session_timeout_minutes')
cloud_storageisAt=config_data.get('APPCONFIGURATIONS','cloud_storage')
if not cloud_storageisAt:
    cloud_storageisAt='GOOGLE'
else:
    cloud_storageisAt=cloud_storageisAt.upper()
    
#initializing the flask application
app = Flask(__name__)

app.config['SECRET_KEY']=secretkey
app.config['UPLOAD_FOLDER']='./filestorage/layouts'
app.config['DATASET_FOLDER']='./filestorage/datasets'
app.config['DATASET_IMPORT_LOG_FOLDER']='./filestorage/dataset_import_log'
app.config['CLOUD_CONFIG_JSON_GCP']='./config/metadata_cloud_backup.json'
app.config['CLOUD_CONFIG_JSON_AZURE']='./config/metadata_cloud_backup_azure.json'
app.config['RESTPLUS_VALIDATE']=True
ALLOWED_EXTENSION={'txt','csv','xlsx','xlsb'}
ALLOWED_DATATYPES={'NUMERIC','FLOAT','TEXT','DATETIME','DATE','ENUM','ARRAY','BOOLEAN','IPADDRESS','EMAIL','URL'}
inbuilt_predefined_basic_types = ['int64','float64','bool','datetime64']
engine = create_engine(dailect_name+'://'+username+':'+password+'@'+db_ip+':'+db_port+'/'+dbname,echo=True)
Base = declarative_base()
Base.metadata= MetaData(schema=schema_name)
bcrypt = Bcrypt(app)
fjson = FlaskJSON(app)
Session = sessionmaker()
Session.configure(bind=engine)
#setting up the restplus config for the swagger implemetation
api = Api(app)


login_manager = LoginManager(app=app)
app.config['REMEMBER_COOKIE_DURATION']=timedelta(minutes=sessionTimeout)

# websocket integrtion to the flask application
socket = SocketIO(app,cors_allowed_origins='*',cookie=None)
# CORS(app)

@login_manager.unauthorized_handler
def unauthorized():
    return { 'status':403, 'message':'Please Login first to user metadata manager services.', 'loginat':'/user/login'}

def checkUser(right):
    if not current_user.is_anonymous:
        return current_user._userRight==right
    else:
        return False

def hasExtraPayload(request_payload,*api_model):
    for keyval in request_payload.keys():
        found=False
        for model in api_model:
            if keyval in model.keys():
                found=True
                break
        if not found:
            return keyval
    return False

def prepare_zip_to_upload(filename):
    try:
        createzip = CreateZip(zipnamesaveas=filename,filepath=filename)
        createzip.create()
        fileselected_to_upload=createzip.zipname
    except Exception as ex:
        print(ex)
        print('Error in Zip creation for the file ')
        fileselected_to_upload=filename
        pass  
    return fileselected_to_upload

from routes import userRoutes,metadataRoutes,filehandlerRoutes,datasetRoutes,datasourceRoutes