import random

def get_Text():
    return "your text input goes here"

def get_Boolean(ranlistInput=None):
    ranlist = ['Yes','No','True','False'] if not ranlistInput else ranlistInput
    return random.choice(ranlist)

def get_numeric(min_len=None,max_len=None):
    if not (min_len and max_len):
        return "555"
    elif min_len<=max_len:
        start='1'*min_len
        end='9'*max_len
        return str(random.randint(int(start),int(end)))
    else:
        return "0123"
    

def get_enum(preferredLOV=None):
    if not preferredLOV:
        preferredLOV=["value1", "value2", "value3","value4"]

    return random.choice(preferredLOV)

def get_float(min_len=None,max_len=None):
    if not (min_len and max_len):
        return "0."+str(random.randint(0,100))
    elif min_len<=max_len:
        start='1'*min_len
        end='9'*max_len
        return str(random.randint(int(start),int(end)))+'.0'
    else:
        return "55.53"

def get_date():
    return "2020/04/12"

def get_datetime():
    return "2020/04/12 16:20:34"

def get_array():
    return "one,two,three"

def get_ipaddress():
    return "128.128.34.5"

def get_url():
    return "https://www.helloworld.com"

def get_email():
    return "abc2Xyz@demo.com"