#creating the base image in the linux environment
FROM ubuntu
#setting up the argument for the package installation
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y python3.8 && apt-get install -y python3-pip && apt-get install -y python3-dev libpq-dev python3-venv && apt-get install -y postgresql-12 postgresql-contrib

#setting up the environment for the application setup inside the container
ENV db_name=metamanager
ENV db_schema=meta_config
ENV db_ipaddress=localhost
ENV db_password=prason
ENV db_dailect=postgres
ENV db_port=5432


#making up the project folder for deployment
RUN mkdir /usr/prason

#initiating the migration work
ADD . /usr/prason/

#defining the PWD to run starter script and application
WORKDIR /usr/prason/

#for project backup and multiple container support
VOLUME ["/usr/prason/"]

#exposing the volumes for the database and config save point 
VOLUME ["/etc/postgresql/","/var/lib/postgresql/","/var/log/postgresql/"]

USER root

#Install all the project dependencies inside
RUN pip3 install -r requirements.txt 

#suppliment package for converting windows file to the unix format
#used for converting starter script before execution
RUN apt install -y dos2unix

RUN dos2unix /usr/prason/Executor.sh

EXPOSE 5000 5432

WORKDIR /usr/prason/ 

#This script needs to be override during docker run command if "Executor.sh" is developed in window machine
CMD [". /usr/prason/Executor.sh"]
