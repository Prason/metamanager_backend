from config.pushtocloud import *
import time,os
from google.cloud.storage import Client
from smart_open import open
start_time = time.perf_counter()
import pandas as pd
import io
from dbmodals.mymodals import session,MetaLayoutDetail
from concurrent.futures import ProcessPoolExecutor,as_completed
from metaengine.datavalidator import initiateDataLevelValidation
# Myfile=get_metadata_file('2020-9-1-19-49-14metadata_files_Frontend_FileUpload_metadata_upload_OD_2017.csv')
# for data in Myfile:
#     print(data.decode('utf-8'))
#     break
# chorolox_grad_1595001928.xlsx
def submitotmodule(params):
    return initiateDataLevelValidation(*params)
    
def dojob():
    client = Client.from_service_account_json('./config/metadata_cloud_backup.json')
    row=0
    batch=[]
    logs=[]
    layout_details = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==7).order_by(MetaLayoutDetail.fieldOrder).all()
    with ProcessPoolExecutor() as executor:
        for data in open('gs://clorox_metadata_manager/OD_2017.csv','rb',encoding='utf-8',transport_params=dict(client=client)):
            # df = pd.read_excel(data.decode('windows-1252','replace'))
            batch.append(data)
            row +=1        
            if row % 20000==0:
                print('submitted'+str(row))
                reports = executor.submit(submitotmodule,[batch,layout_details,[],row])
                # logs.append(reports)
                batch=[]

    end_time = time.perf_counter()
    print(f'total time {end_time-start_time}')

if __name__ == '__main__':
    dojob()

