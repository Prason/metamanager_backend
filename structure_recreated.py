from config import Base
from config import engine,Session,bcrypt,ALLOWED_DATATYPES
from dbmodals.mymodals import MetaUser,MetaDataMasterEntry,MetaLayoutDataSource

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

session=Session()

if not session.query(MetaUser).first():
    user = MetaUser(userName='Admin01',password=bcrypt.generate_password_hash('Bi2i@1234').decode('utf-8'),userEmail='admin@admin.com',_userStatus='ACTIVE', schemaAccess=['ALL'],_userRight='ADMIN')
    session.add(user)
    session.commit()

if not session.query(MetaLayoutDataSource).first():
    datasource_entry=MetaLayoutDataSource(datasourceName='ALL',datasourceDescription='Inclue all the future entries for every datasource',createdBy=1)
    datasource_entry1=MetaLayoutDataSource(datasourceName='Criteo',datasourceDescription='Related to Cretio source',createdBy=1)
    datasource_entry2=MetaLayoutDataSource(datasourceName='iPerception',datasourceDescription='Related to Iperception source',createdBy=1)
    datasource_entry3=MetaLayoutDataSource(datasourceName='BazaarVoice',datasourceDescription='Related to BazaarVoice source',createdBy=1)
    datasource_entry4=MetaLayoutDataSource(datasourceName='Facebook',datasourceDescription='Related to Facebook source',createdBy=1)
    datasource_entry5=MetaLayoutDataSource(datasourceName='Pinterest',datasourceDescription='Related to Pintrest source',createdBy=1)
    datasources=[datasource_entry,datasource_entry1,datasource_entry2,datasource_entry3,datasource_entry4,datasource_entry5]
    session.add_all(datasources)
    session.commit()

if not session.query(MetaDataMasterEntry).first():
    user_type='USER_ROLE'
    user_status='USER_STATUS'
    user_dtype='USER_DTYPE'
    admin_entry= MetaDataMasterEntry(lookupKey=user_type,lookupValue='ADMIN')
    author_entry= MetaDataMasterEntry(lookupKey=user_type,lookupValue='AUTHOR')
    publisher_entry= MetaDataMasterEntry(lookupKey=user_type,lookupValue='PUBLISHER')

    active_entry=MetaDataMasterEntry(lookupKey=user_status,lookupValue='ACTIVE')
    inactive_entry=MetaDataMasterEntry(lookupKey=user_status,lookupValue='INACTIVE')
    suspend_entry=MetaDataMasterEntry(lookupKey=user_status,lookupValue='SUSPEND')

    
    dtype1_entry=MetaDataMasterEntry(lookupKey=user_dtype,lookupValue=','.join(ALLOWED_DATATYPES))

    masterEntries= [dtype1_entry,admin_entry,author_entry,publisher_entry,active_entry,inactive_entry,suspend_entry]

    session.add_all(masterEntries)
    session.commit()
