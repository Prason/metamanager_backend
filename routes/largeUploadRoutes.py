from config.pushtocloud import *
from config import api
import time,os
from google.cloud.storage import Client
from smart_open import open
import pandas as pd
import io
from flask import jsonify
from dbmodals.mymodals import session,MetaLayoutDetail
from concurrent.futures import ProcessPoolExecutor,as_completed,ThreadPoolExecutor
from metaengine.datavalidator import initiateDataLevelValidation
from flask_restplus import Resource
#defining the namespace for the dataset part
name_space = api.namespace('large','test upload')

# Myfile=get_metadata_file('2020-9-1-19-49-14metadata_files_Frontend_FileUpload_metadata_upload_OD_2017.csv')
# for data in Myfile:
#     print(data.decode('utf-8'))
#     break
# chorolox_grad_1595001928.xlsx
import json,gc
def dumptoFile(log):
    with open('./filestorage/mylog.json','a') as f:
        f.write(json.dumps(log))
        f.write('\n')
    
    del log
    gc.collect()

def newPool(future):
    with ThreadPoolExecutor() as tpoolexecutor:
        if tpoolexecutor.submit(dumptoFile,[str(future.result())]).done():
            del future

def submitotmodule(params):
    return initiateDataLevelValidation(*params)
    # with ThreadPoolExecutor() as texecutor:
    #     texecutor.submit(dumptoFile,log)
    
    
@name_space.route('/streamqctest/<filename>/<int:layoutid>')
class StreamTest(Resource):
    def get(self,filename,layoutid):      
        start_time = time.perf_counter()
        client = Client.from_service_account_json('./config/metadata_cloud_backup.json')
        row=0
        batch=[]
        logs=[]
        layout_details = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==layoutid).order_by(MetaLayoutDetail.fieldOrder).all()
        with ProcessPoolExecutor() as executor:
            for data in open('gs://clorox_metadata_manager/'+str(filename),'rb',encoding='utf-8',transport_params=dict(client=client)):
                # df = pd.read_excel(data.decode('windows-1252','replace'))
                data=data.rstrip('\n')
                if(len(data)<5):
                    continue
                batch.append(data)
                row +=1
                # if row==30001:
                #     break        
                if row % 10000==0:
                    print('submitted'+str(row))
                    executor.submit(submitotmodule,[batch,layout_details,[],row]).add_done_callback(newPool)
                    # logs.append(reports)
                    # del reports
                    # print('report deleted')
                    batch=[]

        end_time = time.perf_counter()
        print(f'total time {end_time-start_time}')
