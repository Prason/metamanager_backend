from flask_json import request, json_response,JsonError,jsonify
from flask import abort,json
from config import app, fjson, bcrypt,Session,api,login_manager,checkUser,hasExtraPayload
from dbmodals.mymodals  import MetaUser,MetaDataMasterEntry,MetaLayoutSchemaMapping,MetaLayout,MetaLayoutDetail,MetaFileUploadLogs,MetaLayoutDataSource
from sqlalchemy import exc,any_,or_
from sqlalchemy.exc import DataError
from flask_restplus import Resource,fields,marshal
from dbmodals.swaggermodels import master_value,user_model,user_model_get_params,user_model_get,user_model_post,user_model_put,user_password_put
import logging
from routes import *
from flask_login import current_user,login_user,logout_user,login_required,fresh_login_required
session = Session()
session.rollback()
name_space = api.namespace('user','Handling all user related data communication')

@name_space.route('/thistest',doc=False)
class MMainClass(Resource):

    
    def get(self):
        print(type(current_user))
        if current_user.is_active:
            data='Logged in as : '+current_user.userName
        else: 
            data= 'not logged in'
        return {
        "status": "Got new data",
        "data": data
        }
    
    @fresh_login_required
    def post(self):
        if current_user.is_active:
            data='Logged in as : '+current_user.userName
        else: 
            data= 'not logged in'

        return {
        "status": "Posted new data",
        "data": data
        }
'''
This is the api designed to get all the lookup value stored as master entry with userprefix

'''
@name_space.route('/lookup/<string:key>')
class UserMasterEntry(Resource):

    @name_space.marshal_with(master_value, as_list=True)
    def get(self, key=None):
        if key:
            lookup_key = 'USER_'+str(key).upper()
            try:
                masterEntries = session.query(MetaDataMasterEntry).filter(MetaDataMasterEntry.lookupKey==lookup_key).all()
            except:
                name_space.abort(404,message="No entries found")
                
            if masterEntries: 
                return [result.serialized for result in masterEntries]
            else:
                name_space.abort(404,message="No entries found")
        else:
            name_space.abort(400,message="Key parameter is missing, please verify")


'''

@name_space.route('/<int:user_id>',methods=['GET'])
@name_space.route('/<int:last_record_offset>/<int:total_rows_next>',methods=['GET'])
@name_space.route('/',defaults={'user_id':None},methods=['GET'])

    This is the api for getting the user information
    based on the id supplied or the list of entire users for the front-end Interface.

    Note: /<int:last_record_offset>/<int:total_rows_next> , this path is for lazy loading

    Response for Get Request :
        { user object with its entire property}
    Payload for Get Request:(OPTIONAL)
            {
                //no payload in GET
            }    
'''

'''
@name_space.route('/change/<int:user_id>',methods=['PUT'])
@name_space.route('/remove/<int:user_id>',methods=['DELETE'])

    This is the API that deals with the update and delete record of the users one at a time
     based on the id provided in the following URL
    =============================
    FOR THE UPDATE/[PUT]  REQUEST 
    =============================
    request payload will be similar to:
    {
        "newData": {
            'userName':'value',
            'password':'value',
            'userEmail':'value',
            'userStatus':'value',
            'userRight':'value',
            'schemaAccess':'value'
            }
    }

    response payload:    
        {
            "message": 'Update Success'
            "data" : {<USER_DATA>}
        }

    =============================
    FOR THE DELETE  REQUEST 
    =============================

    Request payload ;
    {}

    Response payload:
    {
        "message":"Delete Success"
         "userId": "idVal"
    }
'''

@name_space.route('/<int:user_id>',methods=['GET'])
@name_space.route('/<int:last_record_offset>/<int:total_rows_next>',methods=['GET'])
@name_space.route('/',defaults={'user_id':None},methods=['GET'])
@name_space.route('/change/<int:user_id>',methods=['PUT'])
@name_space.route('/remove/<int:user_id>',methods=['DELETE'])
class UserClass(Resource):

    @login_required
    @name_space.marshal_with(user_model_get,as_list=True)
    def get(self,user_id=None,last_record_offset=None,total_rows_next=None):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        if (last_record_offset is not None) or (total_rows_next is not None):
            if total_rows_next>100 or total_rows_next<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')
        
        try:
            if user_id:
                userrsList = session.query(MetaUser).filter(MetaUser.userId==user_id).all()
            else:
                try:
                    userrsList=session.query(MetaUser).filter(MetaUser.userId>last_record_offset).limit(total_rows_next).all()
                except Exception as kr:
                    session.rollback()
                    userrsList = session.query(MetaUser).all()
            if userrsList:
                return userrsList
            else:
                name_space.abort(400,message='No User Data found for the given ID input.')

        except Exception as err:
            session.rollback()
            name_space.abort(400,message="No such User Information Available,  Please verify.")

    @name_space.expect(user_model_put,validate=True)
    @login_required
    @name_space.marshal_with(user_model)
    def put(self,user_id):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data=request.get_json(force=True)
        extra_payload =hasExtraPayload(request_data['newData'],user_model)
        if extra_payload:
            return name_space.abort(400,message='Unexpected key at payload :> '''+extra_payload)
        
        username_input_received=False
        email_input_received=False
        try:
            username_input=request_data['newData']['userName']
            username_input_received=True
            email_input=request_data['newData']['userEmail']
            email_input_received=True
        except KeyError as krr:
            pass
        
        if username_input_received and not validUserName(username_input):
            return name_space.abort(400,message="Username must only have letters or numbers without spaces")
            
        if email_input_received and not isEmail(email_input):
            return name_space.abort(400,message="Not a Valid email format received.")

        try:
            userData=session.query(MetaUser).filter(MetaUser.userId==user_id)
            if userData.first():
                userData.update(request_data['newData'],synchronize_session=False)
                session.commit()
                return userData.first()
            else:
                name_space.abort(400,message='There is no such user data, Please verify.')

        except exc.IntegrityError as err:
            session.rollback()
            name_space.abort(403,message="This Username already exist, please try another username.")

        except DataError as ex:
            msg=str(ex.__dict__['orig'])
            if msg.find('URight')>0:
                message=data['user_right']+ ' is unacceptable. Value must be either ["ADMIN","AUTHOR","PUBLISHER"]'
            elif msg.find('UStatus')>0:
                message=data['status']+' is unacceptable. Value must be either ["ACTIVE","INACTIVE","SUSPEND"] '
            else:
                message=ex.__dict__['orig']
            session.rollback()
            name_space.abort(400,message=message)

        except Exception as err:
            session.rollback()
            name_space.abort(400,message='There is no such user data, please check '+err.__str__())
    
    @login_required
    def delete(self,user_id):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        try:
            if (user_id==1):
                # return {'status':403,'message':'Change request received against the super User'}
                name_space.abort(403,message="Change request received against the super User",defect="Please Contact your system administrator.")
            elif (current_user.userId==user_id):
                name_space.abort(403,message="Please contact your system administrator to remove your information.")
            else:
                user_to_remove = session.query(MetaUser).filter(MetaUser.userId==user_id)
                if user_to_remove.first():
                    userMapResult = mapToAdminUser(user_id)
                else:
                    return {'message':"Request aborted , No such user Exist.", 'userId':user_id}

            if not userMapResult:
                user_to_remove.delete(synchronize_session=False)
                session.commit()
                return {'message':"Delete Success", 'userId':user_id}
            else:
                name_space.abort(400,message="Unable To perform Delete request", defect=userMapResult)
        except Exception as err:
            session.rollback()
            name_space.abort(400,message=err.__str__)


def mapToAdminUser(user_id):
    admin_creator={'createdBy':1}
    admin_updator={'updatedBy':1}
    try:
        session.query(MetaLayout).filter(MetaLayout.createdBy==user_id).update(admin_creator,synchronize_session=False)
        session.query(MetaLayout).filter(MetaLayout.updatedBy==user_id).update(admin_updator,synchronize_session=False)
        session.query(MetaLayoutDetail).filter(MetaLayoutDetail.createdBy==user_id).update(admin_creator,synchronize_session=False)
        session.query(MetaLayoutDetail).filter(MetaLayoutDetail.updatedBy==user_id).update(admin_updator,synchronize_session=False)
        session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.createdBy==user_id).update(admin_creator,synchronize_session=False)
        session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.updatedBy==user_id).update(admin_updator,synchronize_session=False)
        session.query(MetaFileUploadLogs).filter(MetaFileUploadLogs.uploadedBy==user_id).update({'uploadedBy':1},synchronize_session=False)
        session.commit()
        return False
    except Exception as exce:
        session.rollback()
        return exce.__str__

'''
@name_space.route('/save',methods=['POST'])

    This is the api for the user registration purpose from the front-end Interface.

    Response for Post Request:
        {
         "saved_user": "prason",
         "status": 200
        }
    Payload for POST Request :
        { "username": "prason",
            "password": "prason",
            "email": "chriso@gmail.com",
            "status" : "ACTIVE",
            "schema_access_ist": ["Schema 1","Schema 2"]
            "user_right" : 'PUBLISHER'
            }
        
Note: status and user_rights are optional and will be populated as shown in above payload

'''
'''

    @name_space.route('/changepass/<int:user_id>',methods=['PUT'])

    This is the api route defined for changing the paassword for the user
    /<int:user_id> : is the information of user id whose password will be changed
'''
def isStrongPass(val):
    if not val:
        return False

    if len(val)<=7:
        return False
    else:
        if not hasUppercase(val):
           return False
        if not hasSpecialChar(val):
            return False
        if not hasLowercase(val):
            return False
        if not hasNumber(val):
            return False
            
    return True
            
def validUserName(val):
    if not val:
        return False

    if len(val)<=3:
        return False
    else:
        if not hasDotUnderscoreOnly(val):
            return False
        if re.search(' ',val):
            return False

    return True
        

@name_space.route('/save',methods=['POST'])
@name_space.route('/changepass/<int:user_id>',methods=['PUT'])
@name_space.route('/list',methods=['GET'])
class UserClass(Resource):

    def get(self):
        try:
            userrsList = session.query(MetaUser).all()
            if userrsList:
                return [{'id':user.userId, 'username':user.userName } for user in userrsList]
            else:
                name_space.abort(400,message='No User Data found for the request')

        except Exception as err:
            session.rollback()
            name_space.abort(400,message="No User Information Available,  Please verify.")

    @name_space.expect(user_model_post,validate=True)
    @login_required
    @name_space.marshal_with(user_model_get,code=200)
    def post(self):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        data=request.get_json()
        try:
            password_input = data['password']
            username_input = data['username']
            email_input=data['email']

            if not isStrongPass(password_input):
                return name_space.abort(400,message="Not a Strong password, Include combination of uppercase numbers and specail characters as well(at least 6 characters).")

            if not validUserName(username_input):
                return name_space.abort(400,message="Username must only have letters or numbers without spaces")
            
            if not isEmail(email_input):
                return name_space.abort(400,message="Not a Valid email format received.")

            passwork = bcrypt.generate_password_hash(password_input).decode('utf-8')
            #CHECK if datasource name is valid
            datasources_namelist = [data.datasourceName for data in session.query(MetaLayoutDataSource).all()]
            for datasources in list(data['schema_access_ist']):
                if not str(datasources):
                    name_space.abort(400,message="defining empty datasource is not allowed")
                if not datasources in datasources_namelist:
                    name_space.abort(400,message='There is no datasource defined as :'+str(datasources))
                
            user = MetaUser(userName=username_input,password=passwork,userEmail=email_input,_userStatus=data['status'] if 'status' in data.keys() else 'ACTIVE', schemaAccess=data['schema_access_ist'],_userRight=data['user_right'] if 'user_right' in data.keys() else 'PUBLISHER')
            # user = MetaUser(userName=username,password=passwork,userEmail=email,userstatus, schemaAccess=schema_access_ist,userRight=user_right)
            session.add(user)
            session.commit()
            return user
        except exc.IntegrityError as err:
            session.rollback()
            name_space.abort(403,message="This User already exist, please try logging in")
        except DataError as ex:
            msg=str(ex.__dict__['orig'])
            if msg.find('URight')>0:
                message=data['user_right']+ ' is unacceptable. Value must be either ["ADMIN","AUTHOR","PUBLISHER"]'
            elif msg.find('UStatus')>0:
                message=data['status']+' is unacceptable. Value must be either ["ACTIVE","INACTIVE","SUSPEND"] '
            else:
                message=ex.__dict__['orig']
            session.rollback()
            name_space.abort(400,message=message)
    
    @name_space.expect(user_password_put,validate=True)
    @login_required
    def put(self,user_id):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data=request.get_json(force=True)
        try:
            password_input = request_data['new_password']['password']
            if not isStrongPass(password_input):
                return name_space.abort(400,message="Not a Strong password, Include combination of uppercase numbers and specail characters as well(at least 6 characters).")

            passwrok = bcrypt.generate_password_hash(password_input).decode('utf-8')
            user_to_update=session.query(MetaUser).filter(MetaUser.userId==user_id)
            if user_to_update.first():
                user_to_update.update({'password':passwrok},synchronize_session=False)
            else:
                return {'message':'Invalid User data detected','status':200}

            session.commit()
            return {'message':'Password Successfully changed','status':200}
        except Exception as err:
            session.rollback()
            name_space.abort(400,message='There is no such user data, please check '+err.__str__())


'''
    This API is to validate the login credential of the user based on username and password supplied

    Response Body:
        {
            "message" : "Success"
            "username" : "prason"
            "status" : 'ACTIVE'
            "role" : 'ADMIN'
        }

    Request Payload:
    {
       "username":"prason"
        "password" "prason"
    }

'''

#model for the login params 
login_cred = api.model('loginModel',{'username':fields.String(required=True,description='username of the user'),'password':fields.String(required=True,description="password for the user"),'remember_me':fields.Boolean(required=True,description='Whether or not to remember the user if the browser is closed')})
@name_space.route('/login')
class LoginManager(Resource):
    
    @name_space.expect(login_cred,validate=True)
    def post(self):
        request_data = request.get_json(force=True)
        uname = request_data['username']
        upass = request_data['password']
        if len(uname)<1 or len(upass)<1:
            name_space.abort(400,message="Username and/or password shouldnot be empty.")

        rememberMe=request_data['remember_me']
        try:
            userdata= session.query(MetaUser).filter(MetaUser.userName==uname).first()
        except:
            session.rollback()
            name_space.abort(403,message="Unauthorized to login")
        if userdata:
            if userdata._userStatus in ['INACTIVE']:
                name_space.abort(400,message="This user is Inactive, Please Contact your admin.")
            elif userdata._userStatus in ['SUSPEND']:
                name_space.abort(400,message="This user is suspended, Please Contact your admin.")
        
        try:
            if userdata and bcrypt.check_password_hash(userdata.password,upass):
                login_user(userdata,remember=rememberMe)
                return {
                    'message':"Success","user_id":userdata.userId,"username":uname,"status":userdata._userStatus,"role":userdata._userRight
                }
            else:
                return name_space.abort(400,message="Login Failed !!! Enter Valid Username and Password")
        except ValueError as verr:
            session.rollback()
            return name_space.abort(403,message="Unauthorized to login")

user_filter_get = name_space.model('user_filter_all',{'userName':fields.String(required=True,description='register username'), 'userEmail':fields.String(description='abc@example.com'),'_userStatus':fields.List(fields.String(enum=['ACTIVE','INACTIVE','SUSPEND'])),'_userRight':fields.List(fields.String(enum=['AUTHOR','ADMIN','PUBLISHER'])),"schemaAccess": fields.List(fields.String(description='List of all accessible schemas'))})

'''
This is the api route for filterring the user data based on the different user related parameters
simply,
 This api is for the searching purpose of the users

    Request payload:
    {
        'userName':'value',
        'password':'value',
        'userEmail':'value',
        'userStatus':'value',
        'userRight':'value',
        'schemaAccess':'value'
    }

    server response:
    {
        List of all the user data matching the query
    }

    Note: the keys of the request payload must be exact same as define, or additional field as is displaayed in swagger UI

'''

@name_space.route('/filter')
class UserFilter(Resource):
    
    @login_required
    @name_space.expect(user_filter_get,validate=True)
    @name_space.marshal_with(user_model_get,envelope='data',as_list=True)
    def post(self):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data = request.get_json(force=True)
        extra_payload=hasExtraPayload(request_data,user_filter_get)
        if extra_payload: 
            name_space.abort(403, message="Invalid key received for filtering :>"+extra_payload)

        userData=session.query(MetaUser)
        for key,value in request_data.items():
            try:
                this_attr=getattr(MetaUser,key)
                if  (this_attr is MetaUser._userRight) or  (this_attr is MetaUser._userStatus):
                    userData=userData.filter(this_attr.in_(value)) 
                elif (this_attr is MetaUser.schemaAccess):
                    userData=userData.filter(or_(*[this_attr.any(v) for v in value]))
                    # userData=userData.filter(getattr(MetaUser,key).or_(like(f'%{v}%' for v in value)))
                else:   
                    userData=userData.filter(this_attr.like(f'%{value}%'))

            except Exception as excep:
                session.rollback()
                name_space.abort(400,message=excep.__str__)
        
        if userData.first():
            return userData.all()
        else:
            name_space.abort(400,message="No data found for the given filter key(s).")
       
'''
    This is the api block for user to be able to logout of the system and clear all of his session history

'''
@name_space.route('/logout')
class UserLogout(Resource):

    @login_required
    def post(self):
        logout_user()
        return {'status':200, 'message':'Logged out Successfully'}

'''
    This is the api designed to know about the user himself

'''
@name_space.route('/whoMI')
class KnowUser(Resource):

    @login_required
    @name_space.marshal_with(user_model)
    def get(self):
        if current_user.is_active:
            if current_user.is_authenticated:
                return current_user