from flask import request,redirect,abort
from flask_restplus import Resource,fields
from flask_json import JsonError, json_response, jsonify
from sqlalchemy.exc import IntegrityError,SQLAlchemyError,DataError
from datetime import datetime
from config import Session, app,api,checkUser,hasExtraPayload,ALLOWED_DATATYPES
from dbmodals.mymodals import MetaLayout, MetaLayoutDetail, MetaUser,MetaLayoutSchemaMapping,MetaFileUploadLogs
from dbmodals.swaggermodels import metainfo_get,metainfo_post,metainfo_put,metainfo_detail_get,metainfo_detail_post,metainfo_detail_put,metainfo_model,metainfo_detail,mapping_data,mapping_data_get,mapping_data_postinfo,mapping_data_put,mapping_data_put_content,mapping_data_remove,metainfo_detail_bulk,metainfo_detail_bulkget,metaimport_upload_post
from os.path import os

from flask_login import login_required,current_user

name_space = api.namespace('metainfo','Handles all the MetaData and its attribute infromation')

@name_space.route('/test',doc=False)
class MainClass(Resource):
    def get(self):
        name_space.abort(400,message='this is rest response ')
    # return {
    # "status": "Got new data"
    # }
    def post(self):
        return {
            "status": "Posted new data"
        }



#db session for the metainfo management
session=Session()
session.rollback()
@name_space.route('/upload/')
class MetadataDirectUpload(Resource):

    @name_space.expect(metaimport_upload_post)
    def post(self):
        request_data= request.get_json()
        try:
            #handling the upload of the netadata
            metainfo = MetaInfoClass()
            layoutMsg=metainfo.post(internalPayload=request_data['layout_info'])
            #handling the upload of the mapping data
            mapper = SchemaLayoutMapper()
            mappings=request_data['mapping_info']['mappings']
            for data in mappings:
                data['layout_id']=layoutMsg['layout_id']
            mapping_result=mapper.post(internalPayload=request_data['mapping_info'])
            print(mapping_result)
            if not 'mapper_id' in mapping_result[0].keys():
                session.rollback()
                if layoutMsg:
                    clearMetaDetails(layoutMsg['layout_id'])
                name_space.abort(400,message="Error in Creating the mapping info")

            #handling the upload of the details attribute data
            metadetails=DetailMetaInfoClass()
            detailsdata=request_data['detail_info']['Detail_data']
            request_data['detail_info']['override_layout']=layoutMsg['layout_id']
            for data in detailsdata:
                data['attribute_layout']=layoutMsg['layout_id']
            detail_upload_result=metadetails.post(internalPayload=request_data['detail_info'])
            print(detail_upload_result)
            if detail_upload_result['message'] !='SUCCESS':
                if layoutMsg:
                    clearMetaDetails(layoutMsg['layout_id'])
                name_space.abort(400,message=detail_upload_result['message'],remarks=detail_upload_result['remarks'],defect=detail_upload_result['defect'],causedby=detail_upload_result['causedby'],attribute_details=detail_upload_result['attribute_details'])
            else:
                remark=detail_upload_result['remarks'] if 'remarks' in detail_upload_result.keys() else ''
                return {'message':'Metadata Uploaded successfully.','remarks':remark,'attribute_details':detail_upload_result['attribute_details']}
                
        except SQLAlchemyError as ex:
            if layoutMsg:
                clearMetaDetails(layoutMsg['layout_id'])
            session.rollback()
            name_space.abort(400,message=str(ex))
        
    

'''

@name_space.route('/',methods=['GET','POST'])
@name_space.route('/<int:layout_id>',methods=['GET'])
@name_space.route('/<int:lastrecord_offset>/<int:total_new_rows>',methods=['GET'])


    This is the api for the Metadata layout registration purpose and the getting of the layout information
    based on the id supplied or the list of entire layout for the front-end Interface.

    /<int:lastrecord_offset>/<int:total_new_rows> : this is path for lazy loading purpose

    Response for Get Request :
        { Meta Layout object with its entire property}
    Payload for Get Request:(OPTIONAL)
            {
               //there is no payload for GET request
            } 

    Response for Post Request:
        {
         "message": "Record saved for new layout ABC LAYOUT",
         "status": 200
        }
    Payload for POST Request :
            {
            'layoutHead':"ABC LAYOUT",
            'layoutDescription':"description of layout",
            'userId':"userIdValue"
            }
        
'''

@name_space.route('/',methods=['GET','POST'])
@name_space.route('/<int:layout_id>',methods=['GET'])
@name_space.route('/<int:lastrecord_offset>/<int:total_new_rows>',methods=['GET'])

class MetaInfoClass(Resource):
    
    @login_required
    @name_space.marshal_with(metainfo_get,as_list=True)
    def get(self,layout_id=None,lastrecord_offset=None,total_new_rows=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        if (lastrecord_offset is not None) or (total_new_rows is not None):
            if total_new_rows>100 or total_new_rows<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')

        if layout_id is not None:
            metalayouts= session.query(MetaLayout).filter(MetaLayout.id==layout_id).all()
        else:
            try:
                if lastrecord_offset is not None and total_new_rows is not none:
                    metalayouts=session.query(MetaLayout).filter(MetaLayout.id>lastrecord_offset).limit(total_new_rows).all()
                else:
                    metalayouts = session.query(MetaLayout).all()
            except Exception as kr:
                session.rollback()
                metalayouts = session.query(MetaLayout).all()
        if metalayouts:
            return [data.serialized for data in metalayouts]
        else:
            name_space.abort(400,message='No Data found to load')
    
    @name_space.expect(metainfo_post,validate=True)
    @login_required
    def post(self,internalPayload=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data=request.get_json(force=True) if not internalPayload else internalPayload
        try:
            header_row=request_data['headerOnRow'] if 'headerOnRow' in request_data.keys() else 1
            footer_count=request_data['footerRows'] if 'footerRows' in request_data.keys() else 0
            new_metaLayout = MetaLayout(layoutHead=request_data['layout_head'],layoutDescription=request_data['layout_description'] if 'layout_description' in request_data.keys() else '',createdBy=current_user.userId,headerAtRow=header_row,footerRowCount=footer_count)
            metalayout_head = session.query(MetaLayout).filter(MetaLayout.layoutHead==new_metaLayout.layoutHead)
            if metalayout_head.first():
                #check if override flag is on
                if request_data['override_Flag']:
                    clearMetaDetails(metalayout_head.first().id)
                    return {'status':200, 'message':'Record saved for new layout '+new_metaLayout.layoutHead,'layout_id':metalayout_head.first().id}
            if len(str(new_metaLayout.layoutHead).strip())<1:
                name_space.abort(400,message="Metadataname cannot be empty")

            session.add(new_metaLayout)
            if not internalPayload:
                session.commit()
            saved_metainfo=session.query(MetaLayout).filter(MetaLayout.layoutHead==new_metaLayout.layoutHead).first()
            return {'status':200, 'message':'Record saved for new layout '+new_metaLayout.layoutHead,'layout_id':saved_metainfo.id}

        except KeyError as kr:
            session.rollback()
            name_space.abort(400,message=kr.__str__())
        except SQLAlchemyError as err:
            session.rollback()
            errorstring = str(err.__str__)

            if (errorstring.find('fk_create_layout_user')>0) or (errorstring.find('fk_update_layout_user')>0):
                name_space.abort(400,message="User information doesnot exist for  "+str(request_data['user_id'])+", Please Verify.")
            elif errorstring.find('meta_layouts_layoutHead_key')>0:
                name_space.abort(403,message="Layout Heading as "+request_data['layout_head']+" already exists.")
            elif errorstring.find('chk_negative_headerval')>0:
                name_space.abort(403,message="Header value starts from 1")
            elif errorstring.find('chk_negative_footerVal')>0:
                name_space.abort(403,message="Footer row count is either 0 or positive number")
            else:
                name_space.abort(400,message=errorstring)
            #need to add implementation for the override flag removed from the file import section

'''
@name_space.route('/change/<int:layout_id>',methods=['PUT'])
@name_space.route('/remove/<int:layout_id>',methods=['DELETE'])


    This is the API that deals with the update and delete record of the metalayout one at a time
     based on the id provided in the following URL
    =============================
    FOR THE UPDATE  REQUEST 
    =============================
    request payload:
    {
        "newLayoutData": { 
            'layoutHead':"new Heading",
            'layoutDescription':"new Description",
            'updatedBy':"userId of updator",
            }
    }

    response payload:    
        {
            "message": 'Update Success'
            "data" : {<META_LAYOUT_DATA>}
        }

    =============================
    FOR THE DELETE  REQUEST 
    =============================

    Request payload ;
    {}

    Response payload:
    {
        "message":"Delete Success"
    }
'''

@name_space.route('/change/<int:layout_id>',methods=['PUT'])
@name_space.route('/remove/<int:layout_id>',methods=['DELETE'])
class MetaInfoClassDML(Resource):

    @name_space.expect(metainfo_put,validate=True)
    @login_required
    @name_space.marshal_with(metainfo_get,code=200)
    def put(self,layout_id,internalPayload=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")
        
        request_data=request.get_json() if internalPayload is None else internalPayload
        extra_payload = hasExtraPayload(request_data['newLayoutData'],metainfo_model)
        if extra_payload:
            name_space.abort(400,message='Unexpected key at payload :> '''+extra_payload)

        metalayout = session.query(MetaLayout).filter(MetaLayout.id==layout_id)
        if not metalayout.first():
            name_space.abort(400,message="No Data available for processing your change request")
        else:
            try:
                request_data['newLayoutData']['updatedBy']=current_user.userId
                if request_data['newLayoutData']['updatedBy']:
                    request_data['newLayoutData']['updatedOn']=datetime.now()
                    if 'layoutHead' in request_data['newLayoutData'].keys():
                        if len(str(request_data['newLayoutData']['layoutHead']))<1:
                            name_space.abort(400,message="Metadataname cannot be updated with empty value")
                         
                    metalayoutdata = metalayout.update(request_data['newLayoutData'],synchronize_session=False)
                    layoutData = metalayout.first()
                    session.commit()
                    
                    if not layoutData:
                        name_space.abort(400,message="No data found:"+str(layout_id))
                    else:
                        return layoutData.serialized
                else:
                    name_space.abort(400,message='Cannot process the data,please verify the user Information')
            except KeyError as ke:
                session.rollback()
                name_space.abort(400,message='Missing updator Infromation')
            except DataError as derr:
                session.rollback()
                name_space.abort(400,message="Invalid layout Heading provide, maximum length is 50 character")
            except SQLAlchemyError as err:
                session.rollback()
                errorstring = str(err.__str__)

                if (errorstring.find('fk_create_layout_user')>0) or (errorstring.find('fk_update_layout_user')>0):
                    name_space.abort(400,message="Provided User information doesnot exist for  , Please Verify.")
                elif errorstring.find('meta_layouts_layoutHead_key')>0:
                    name_space.abort(403,message="Layout Heading already exists.")
                elif errorstring.find('chk_negative_headerval')>0:
                    name_space.abort(403,message="Header value starts from 1")
                elif errorstring.find('chk_negative_footerVal')>0:
                    name_space.abort(403,message="Footer row count is either 0 or positive number")
                else:
                    name_space.abort(400,message=errorstring)
            # except Exception as err:
            #     session.rollback()
            #     name_space.abort(400,message=err.__str__())
            
    @login_required           
    def delete(self,layout_id):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        try:
            metalayout = session.query(MetaLayout).filter(MetaLayout.id==layout_id)
            if not metalayout.first():
                return json_response(400,message="No Data available for processing your change request")
            else:
                clearingResult=clearMetaDetails(layout_id)

            if not clearingResult:
                metalayout.delete(synchronize_session=False)
                session.commit()
                return {'message':'Delete Successful', 'status':200}
            else:
                name_space.abort(400,message="Unable To perform MetaData Delete request", defect=clearingResult)
      
        except Exception as err:
            session.rollback()
            name_space.abort(400, message="Invalid Reqest made, please verify"+err.__str__())

def clearMetaDetails(layoutId):
    try:
        session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==layoutId).delete(synchronize_session=False)
        session.query(MetaFileUploadLogs).filter(MetaFileUploadLogs.belongsToLayout==layoutId).update({'belongsToLayout':None},synchronize_session=False)
        session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.layoutHead==layoutId).delete(synchronize_session=False)
        session.query(MetaLayout).fillter(MetaLayout.id==layoutId).delete(synchronize_session=False)
        session.commit()
        return False
    except Exception as execp:
        session.rollback()
        return execp.__str__
'''
@name_space.route('/details/',methods=['GET','POST'])
@name_space.route('/details/<int:detail_id>',methods=['GET'])
@name_space.route('/details/<int:lastdetail_offset>/<int:total_rows>',methods=['GET'])


    This is the api for the Metadata layout registration purpose and the getting of the layout detail information
    based on the id supplied or the list of entire layout for the front-end Interface.

        /details/<int:lastdetail_offset>/<int:total_rows>: this is path for lazy loading purpose


    Response for Get Request :
        {   details columns list of Meta Layout object with its entire property for all layouts }
    Payload for Get Request:(OPTIONAL)
        {
            //no payload for the GET Request 
        } 

    Response for Post Request:
        {
         "message": "Record saved for new layout ABC LAYOUT",
         "status": 200
        }
    Payload for POST Request :
            {
            'attrIdentifier':'identifier name',
            'attrDescription':"attribute description",
            'attrName':"attribute name ",
            'attrType':" data type for the attribute ",
            'attrMinLength':'minimum length for attribute value ',
            'attrMaxLength':'minimum length for attribute value ',
            'attrLOV':['v1','v2','v3'],
            'attrNull':true/false,
            'attrRegex':"/regexpattern/",
            'layoutId':"id of layout to link ",
            'createdBy':"user id sending the request",
          }
        
Note: attrIdentifier,attrType,layoutId and createdBy are mandatory fields and be populated as shown in above payload
other wise it may give attribute errror
'''
@name_space.route('/details/all',methods=['GET'],doc=False)
@name_space.route('/details/',methods=['POST'])
@name_space.route('/details/<int:detail_id>',methods=['GET'])
@name_space.route('/details/<int:lastdetail_offset>/<int:total_rows>',methods=['GET'])
class DetailMetaInfoClass(Resource):
    
    @login_required
    @name_space.marshal_with(metainfo_detail_get,as_list=True,code=200)
    def get(self,detail_id=None,lastdetail_offset=None,total_rows=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")
        
        if (lastdetail_offset is not None) or (total_rows is not None):
            if total_rows>100 or total_rows<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')

        if detail_id:
            detailsInfo=session.query(MetaLayoutDetail).filter(MetaLayoutDetail.id==detail_id).all()
        elif detail_id==0:
            name_space.abort(400,message='Invalid parameter supplied.')
        else:
            try:
                detailsInfo=session.query(MetaLayoutDetail).filter(MetaLayoutDetail.id>lastdetail_offset).limit(total_rows).all()
            except Exception as kr:
                session.rollback()
                detailsInfo = session.query(MetaLayoutDetail).all()   
        if detailsInfo:
            return [result.serialized for result in detailsInfo]
        else:
            name_space.abort(400,message='No Attribute Details data found')

    
    @name_space.expect(metainfo_detail_bulk,validate=True)
    @login_required
    @name_space.marshal_with(metainfo_detail_bulkget)
    def post(self,internalPayload=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_payload = request.get_json() if not internalPayload else internalPayload
        if not internalPayload:
            try:
                if request_payload['override_flag']:
                    layout_id_to_remove = request_payload['override_layout'] if 'override_layout' in request_payload.keys() else request_payload['Detail_data'][0]['attribute_layout']
                    clearMetaDetails(layout_id_to_remove)
            except Exception as exc:
                session.rollback()
                name_space.abort(400,message=exc.__str__)

        saved_attributes=[]
        for request_data in request_payload['Detail_data']:
            try:
                detailinfo = MetaLayoutDetail(
                attrName=request_data['attribute_name'] if 'attribute_name' in request_data.keys() else '',\
                attrRegex=request_data['attribute_regex'] if 'attribute_regex' in request_data.keys() else '', \
                attrDescription=request_data['attribute_desc'] if 'attribute_desc' in request_data.keys() else '',\
                attrIdentifier=request_data['attribute_identifier'],\
                attrMinLength=request_data['attribute_minlen']if 'attribute_minlen' in request_data.keys() else None,\
                attrMaxLength=request_data['attribute_maxlen'] if 'attribute_maxlen' in request_data.keys() else None,\
                attrLOV=request_data['attribute_lov'] if 'attribute_lov' in request_data.keys() else [],\
                attrNull=request_data['attribute_null'] if 'attribute_null' in request_data.keys() else False,\
                attrType=request_data['attribute_type'] ,\
                createdBy=current_user.userId,\
                layoutHead=request_data['attribute_layout'],\
                fieldOrder=request_data['field_order'] )
                
                if len(str(detailinfo.attrIdentifier).strip())<1:
                    name_space.abort(400,message='Attribute Identifier cannot be empty at field order'+str(detailinfo.fieldOrder))


                session.add(detailinfo)
                session.commit()
                data = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.attrIdentifier==request_data['attribute_identifier']).first()
                saved_attributes.append(detailinfo.serialized)
            except KeyError as kr:
                session.rollback()
                if saved_attributes:
                    return {'message':kr.__str__, 'causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']),'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                name_space.abort(400,message=kr.__str__)
            except SQLAlchemyError as err:
                session.rollback()
                try:
                    errorstring = str(err.__dict__['orig'])
                except:
                    errorstring=err.__str__()

                if errorstring.find('fk_create_layoutdtl_user')>0 or errorstring.find('fk_update_layoutdtl_user')>0:
                    if saved_attributes:
                        return {'message':'Provided User doesn\'t exist. please verify details','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']), 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message="Provided User doesn't exist. please verify details")
                elif errorstring.find('fk_layoutid_layoutdtl_layout')>0:
                    if saved_attributes:
                        return {'message':'Provided Layout doesn\'t exist. please verify details','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']), 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message="Provided Layout doesn't exist. please verify details")
                elif errorstring.find('chk_min_max')>0:
                    if saved_attributes:
                        return {'message':'Min value must be less than Max Value. please verify details', 'causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']),'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message="Min value must be less than Max Value. please verify details")
                elif errorstring.find('uq_same_field_in_layout')>0:
                    if saved_attributes:
                        return {'message':'This attribute for current layout already exist, Each column must have unique name. please verify','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']), 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message='This attribute for current layout already exist, Each column must have unique name. please verify')                
                elif errorstring.find('uq_same_col_order_in_layout')>0:
                    if saved_attributes:
                        return {'message':'Each column must have unique field orders, Overlapping not allowed. please verify','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']), 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message='Each column must have unique field orders, Overlapping not allowed. please verify')                
                elif errorstring.find('chk_negative_ordering')>0:
                    if saved_attributes:
                        return {'message':'Field ordering for current layout cannot be zero or negative value. please verify','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']), 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message='Field ordering for current layout cannot be zero or negative value. please verify')                
                elif errorstring.find('chk_type_attribute')>0:
                    if saved_attributes:
                        return {'message':'Provided datatype is not supported by the application.','causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']),'defects':'supported attribute types are '+str(ALLOWED_DATATYPES),'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message='Provided datatype is not supported by the application.',defects='supported attribute types are '+str(ALLOWED_DATATYPES))
                else: 
                    if saved_attributes:
                        return {'message':errorstring, 'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                    name_space.abort(400,message=errorstring)

            except Exception as err:
                session.rollback()
                if saved_attributes:
                        return {'message':'Invalid Request made, please verify'+err.__str__(), 'causedby':'Please check detail of attribute :'+str(request_data['attribute_identifier']),'remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}
                name_space.abort(400, message="Invalid Request made, please verify"+err.__str__())

        return {'message':'SUCCESS','remarks':str(len(saved_attributes))+' record(s) are saved','attribute_details':saved_attributes}



'''
@name_space.route('/details/change/<int:detail_id>',methods=['PUT'])
@name_space.route('/details/remove/<int:detail_id>',methods=['DELETE'])

    This is the API that deals with the update and delete record of the detail of the metalayout one at a time
    based on the id provided in the following URL
    =============================
    FOR THE UPDATE  REQUEST 
    =============================
    request payload:
    {
        "newDetailInfo": {
            'attrIdentifier':'identifier name',
            'attrDescription':"attribute description",
            'attrName':"attribute name ",
            'attrType':" data type for the attribute ",
            'attrMinLength':'minimum length for attribute value ',
            'attrMaxLength':'minimum length for attribute value ',
            'attrLOV':['v1','v2','v3'],
            'attrNull':true/false,
            'attrRegex':"/regexpattern/",
            'layoutId':"id of layout to link ",
            'updatedBy':"user id sending the request"
            }
    }

    response payload:    
        {
            "message": 'Update Success'
            }

    =============================
    FOR THE DELETE  REQUEST 
    =============================

    Request payload ;
    {}

    Response payload:
    {
        "message":"Delete Success"
    }

    Note: for the upadate/ PUT request the attribut name must match with the modals identifier compulsorily
'''
@name_space.route('/details/change/',methods=['PUT'])
@name_space.route('/details/remove/<int:detail_id>',methods=['DELETE'])
class DetailMetaInfoDMLClass(Resource):

    @name_space.expect(metainfo_detail_put,validate=True)
    @login_required
    @name_space.marshal_with(metainfo_detail_bulkget)
    def put(self):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")
        updated_metadetails=[]
        request_data = request.get_json()
        #update the layoutinformation if any
        try:
            metainfo_data = request_data['newLayoutData']
            metainfo_update = MetaInfoClassDML()
            layoutis=metainfo_data.pop('layoutHead')
            metainfo_update.put(layout_id=layoutis,internalPayload={'newLayoutData':metainfo_data})
        except KeyError as kerror:
            pass
        for detail_data in request_data['newDetailInfo']:
            try:      
                extra_payload = hasExtraPayload(detail_data,metainfo_detail,metainfo_model)
                if extra_payload:
                        if updated_metadetails:
                            return {'message':'Unexpected key at payload :> '''+extra_payload,'causedby':'Please check detail of attributeid :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
                        else:
                            name_space.abort(400,message='Unexpected key at payload :> '''+extra_payload)

                detail_data['updatedBy']=current_user.userId
                if detail_data['updatedBy']:                   
                    detail_data['updatedOn']=datetime.now()
                    attribute_id=detail_data.pop('layoutDtlId')
                    detailData=session.query(MetaLayoutDetail).filter(MetaLayoutDetail.id==attribute_id)
                    if not detailData.first():
                        if updated_metadetails:
                            return {'message':'No Data available for processing your change request on attribute ID: '+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
                        else:
                            name_space.abort(400,message="No Data available for processing your change request on attribute ID: "+str(attribute_id))
                    
                    detailData.update(detail_data,synchronize_session=False)
                    session.commit()
                    metadata_impacted=detailData.first().layoutHead
                    session.query(MetaLayout).filter(MetaLayout.id==metadata_impacted).update({'updatedBy':current_user.userId,'updatedOn':datetime.now()})
                    session.commit()
                    updated_metadetails.append(detailData.first().serialized)
                else:
                    session.rollback()
                    if updated_metadetails:
                        return {'message':'Invalid updator Infromation supplied','causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
                    else:
                        name_space.abort(400,message='Invalid updator Infromation supplied')
                     
            except KeyError:
                session.rollback()
                if updated_metadetails:
                        return {'message':'Missing updator or detail identifier Infromation','causedby':'Please check detail of attributeid :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}

                name_space.abort(400,message='Missing updator or detail identifier Infromation')


            except SQLAlchemyError as err:
                session.rollback()
                try:
                    errorstring = str(err.__dict__['orig'])
                except:
                    errorstring = err.__str__()

                if errorstring.find('fk_create_layoutdtl_user')>0 or errorstring.find('fk_update_layoutdtl_user')>0:
                    if updated_metadetails:
                        return {'message':'Provided User doesn\'t exist. please verify details','causedby':'Please check detail of attributeid :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
                    name_space.abort(400,message="Provided User doesn't exist. please verify details")
                elif errorstring.find('fk_layoutid_layoutdtl_layout')>0:
                    if updated_metadetails:
                        return {'message':'Provided Layout doesn\'t exist. please verify details','causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
                    name_space.abort(400,message="Provided Layout doesn't exist. please verify details")
                elif errorstring.find('chk_min_max')>0:
                    if updated_metadetails:
                        return {'message':'Min value must be less than Max Value. please verify details','causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}

                    name_space.abort(400,message="Min value must be less than Max Value. please verify details")
                elif errorstring.find('uq_same_field_in_layout')>0:
                    if updated_metadetails:
                        return {'message':'This attribute for current layout already exist, Each column must have unique name. please verify','causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}

                    name_space.abort(400,message='This attribute for current layout already exist, Each column must have unique name. please verify')                
                elif errorstring.find('chk_type_attribute')>0:
                    if updated_metadetails:
                        return {'message':'Provided datatype is not supported by the application.','causedby':'Please check detail of attribute :'+str(attribute_id),'defects':'supported attribute types are '+str(ALLOWED_DATATYPES),'remarks':str(len(updated_metadetails))+' record(s) are saved','attribute_details':updated_metadetails}
                    name_space.abort(400,message='Provided datatype is not supported by the application.',defects='supported attribute types are '+str(ALLOWED_DATATYPES))
                elif errorstring.find('chk_negative_ordering')>0:
                    if updated_metadetails:
                        return {'message':'Field ordering for current layout cannot be zero or negative value. please verify','causedby':'Please check detail of attribute :'+str(attribute_id), 'remarks':str(len(updated_metadetails))+' record(s) are saved','attribute_details':updated_metadetails}
                    name_space.abort(400,message='Field ordering for current layout cannot be zero or negative value. please verify')
                else:
                    if updated_metadetails:
                        return {'message':errorstring,'causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}

                    name_space.abort(400,message=errorstring)

            # except Exception as exc:
            #     session.rollback()
            #     if updated_metadetails:
            #             return {'message':exc.__str__(),'causedby':'Please check detail of attribute :'+str(attribute_id),'remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}
            #     name_space.abort(400,message=exc.__str__())
        
        return {'message':'SUCCESS','remarks':str(len(updated_metadetails))+' rows are updated','attribute_details':updated_metadetails}


    @login_required
    def delete(self,detail_id):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data = request.get_json()
        detailData=session.query(MetaLayoutDetail).filter(MetaLayoutDetail.id==detail_id)
        if not detailData.first():
            return json_response(400,message="No Data available for processing your change request")
        else:
            try:            
                detailData.delete(synchronize_session=False)
                session.commit()
                return {'status':200, 'message':'Delete SuccessFul'}
            except SQLAlchemyError as err:
                session.rollback()
                name_space.abort(400,message=err._sql_message('utf-8'))
            except Exception as exc:
                session.rollback()
                name_space.abort(400,message=exc.__str__())

'''
    This is the api for getting all the details of the particular layout id supplied from the front-end Interface.

    Response for Get Request :
        { 
            Meta Layout details object with its entire property belonging to the particular layout 
            }
        Payload for Get Request:
            {
                //no payload required for the GET request
            }

'''

@name_space.route('/allattr/meta/<int:layout_id>')
class FetchLayoutAttributes(Resource):

    @login_required
    @name_space.marshal_with(metainfo_detail_get,as_list=True)
    def get(self,layout_id):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        metalayoutdetails = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==layout_id).all()
        if metalayoutdetails:
            return [result.serialized for result in metalayoutdetails]
        else:
            name_space.abort(403,message="No Attribute has been created yet for the Metadata :"+str(layout_id))


@name_space.route('/map/<int:schema_id>',methods=['GET'])
@name_space.route('/map/<int:mapid_offset>/<int:totalrows_next>',methods=['GET'])
@name_space.route('/map',methods=['GET','POST','PUT','DELETE'])
class SchemaLayoutMapper(Resource):

    @login_required
    @name_space.marshal_with(mapping_data_get)
    def get(self,schema_id=None,mapid_offset=None,totalrows_next=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        if (mapid_offset is not None) or (totalrows_next is not None):
            if totalrows_next>100 or totalrows_next<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')

        if schema_id:
            maps = session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.schemaName==schema_id).all()
        else:
            try:
                if mapid_offset is not None and totalrows_next is not None:
                    maps=session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.mapId>mapid_offset).limit(totalrows_next).all()
                else:
                    maps = session.query(MetaLayoutSchemaMapping).all()
            except:
                session.rollback()
                maps = session.query(MetaLayoutSchemaMapping).all()
        if maps:
            return [result.serialized for result in maps]
        else:
            name_space.abort(400,message='No Mapping data found')
        
    @login_required
    @name_space.marshal_with(mapping_data_get, as_list=True)    
    @name_space.expect(mapping_data_postinfo,validate=True)
    def post(self,internalPayload=None):
        try:
            if checkUser('PUBLISHER'):
                name_space.abort(403,message="Please contact your sytem adminstrator for this request")

            payload=request.get_json() if not internalPayload else internalPayload
            mapLists=[]
            for request_data in payload['mappings']:
                mapData = MetaLayoutSchemaMapping(schemaName=request_data['schema_name'], \
                layoutHead=request_data['layout_id'], \
                createdBy=current_user.userId)
                mapLists.append(mapData)
            
            processingRecord=None
            for mapentry in mapLists:
                processingRecord=mapentry
                session.add(mapentry)

            session.commit()
            return [result.serialized for result in mapLists]
        except KeyError as krr:
            session.rollback()
            name_space.abort(400,message='Invalid or unexpected key detected.Please verify the payload')
            
        except SQLAlchemyError as err:
            session.rollback()
            print(err.__str__)
            try:
                errorstring = str(err.__dict__['orig'])
            except:
                errorstring=err.__str__()

            if errorstring.find('unq_layoutschema_map')>0:
                name_space.abort(403,message='Attempt of creating duplicate mapping, please verify',defect='('+str(processingRecord.layoutHead)+', '+str(processingRecord.schemaName)+')')

            elif errorstring.find('fk_mapper_schema')>0:
                name_space.abort(400,message='Supplied Datasource isnot defined in the system.',defect='('+str(processingRecord.layoutHead)+', '+str(processingRecord.schemaName)+')')

            elif errorstring.find('fk_mapper_layoutid')>0:
                name_space.abort(400,message='Supplied layout doesnot exists., please verify',defect='('+str(processingRecord.layoutHead)+', '+str(processingRecord.schemaName)+')')

            elif ((errorstring.find('fk_mapper_creator')>0) or (errorstring.find('fk_mapper_updator')>0) ) :
                name_space.abort(400,message='No such User Exist, please verify',defect='('+str(processingRecord.layoutHead)+', '+str(processingRecord.schemaName)+')')
       
            else:
                name_space.abort(400,message=errorstring)
        except Exception as exce:
                session.rollback()
                name_space.abort(400,message=exce.__str__())

    @login_required
    @name_space.marshal_with(mapping_data_get)
    @name_space.expect(mapping_data_put,validate=True)
    def put(self):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        processingRecord=None
        mapLists=[]
        try:
            payload=request.get_json()
            
            for request_data in payload['newmappings']:
                if hasExtraPayload(request_data['mapdata'],mapping_data_put_content):
                    name_space.abort(403,message ='Cannot process the additional values provided, please check the payload format',defects=str(request_data['mapdata'].keys()))
                
                request_data['mapdata']['updatedBy']=current_user.userId
                request_data['mapdata']['updatedOn']=datetime.now()
                processingRecord=session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.mapId==request_data['map_id'])
                processingRecord.update(request_data['mapdata'],synchronize_session=False)
                mapLists.append(processingRecord.first())
            
            session.commit()
            return [result.serialized for result in mapLists]
    
        except SQLAlchemyError as err:
            session.rollback()
            try:
                errorstring = str(err.__dict__['orig'])
            except:
                errorstring=err.__str__()

            if errorstring.find('unq_layoutschema_map')>0:
                name_space.abort(400,message='Attempt of creating duplicate mapping, please verify',defect='('+str(processingRecord.first().layoutHead)+', '+processingRecord.first().schemaName+')')

            elif errorstring.find('fk_mapper_schema')>0:
                name_space.abort(400,message='Supplied Datasource isnot defined in the system.',defect='('+str(processingRecord.first().layoutHead)+', '+processingRecord.first().schemaName+')')

            elif errorstring.find('fk_mapper_layoutid')>0:
                name_space.abort(400,message='Supplied layout doesnot exists., please verify',defect='('+str(processingRecord.first().layoutHead)+', '+processingRecord.first().schemaName+')')

            elif ((errorstring.find('fk_mapper_creator')>0) or (errorstring.find('fk_mapper_updator')>0) ) :
                name_space.abort(400,message='No such User Exist, please verify',defect='('+str(processingRecord.first().layoutHead)+', '+processingRecord.first().schemaName+')')
       
            else:
                name_space.abort(400,message=errorstring)
        except Exception as exce:
                session.rollback()
                name_space.abort(400,message=exce.__str__())

    @login_required
    @name_space.expect(mapping_data_remove)
    def delete(self):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        try:
            payload=request.get_json()
            removedEntry=0
            for request_data in payload['removemappings']:
                mapping = session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.mapId==request_data['map_id'])
                if mapping:
                    mapping.delete(synchronize_session=False)
                    removedEntry+=1                
            
            if removedEntry:
                session.commit()
                return {'message':str(removedEntry)+' entries are deleted successfully', 'status':200}
            else:
                return {'message':'No Such Mapping Found to Delete', 'status':200}                

        except Exception as err:
            session.rollback()
            name_space.abort(400, message="Invalid Reqest made, please verify"+err.__str__())    
    