from config import Session,api,checkUser,hasExtraPayload
from flask_restplus import marshal_with,Resource
from flask_login import login_required, current_user
from dbmodals.mymodals import MetaLayoutSchemaMapping,MetaFileUploadLogs,MetaClientDatasets,MetaUser,MetaLayout,MetaLayoutSchemaMapping,MetaLayoutDataSource
from dbmodals.swaggermodels import dataset_model,metainfo_model_list,dataset_filter_get
from sqlalchemy import distinct
from flask import request

#defining the namespace for the dataset part
name_space = api.namespace('datasetinfo','Handles all the data communication related to dataset file from publisher')
#creating new session handler for the dataset routes
session = Session()
session.rollback()
@name_space.route('/',methods=['GET'])
@name_space.route('/<int:datasetId>',methods=['GET'])
@name_space.route('/<int:last_record_offset>/<int:total_records>',methods=['GET'])
@name_space.route('/remove/<int:dataset_id>',methods=['DELETE'])
class DatasetHandler(Resource):

    @login_required
    @name_space.marshal_with(dataset_model,as_list=True)
    def get(self,datasetId=None,last_record_offset=None,total_records=None):
        if (last_record_offset is not None) or (total_records is not None):
            if total_records>100 or total_records<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')
        
        schema_list = current_user.schemaAccess
        accessible_metadata_id=[]
        if 'ALL' not in schema_list:
            schema_id_needed = [ data.id for data in session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.datasourceName.in_(schema_list)).all()]
            result_mapping = session.query(distinct(MetaLayoutSchemaMapping.layoutHead)).filter(MetaLayoutSchemaMapping.schemaName.in_(schema_id_needed)).all()
            result_metadata=session.query(MetaLayout).filter(MetaLayout.id.in_(result_mapping)).all()
            if result_metadata:
                accessible_metadata_id = [data.id for data in result_metadata]
            else:
                name_space.abort(400,message='You haven\'t uploaded any dataset yet')

        if datasetId:
            if accessible_metadata_id:
                datasets= session.query(MetaClientDatasets).filter(MetaClientDatasets.id==datasetId).from_self().join(MetaFileUploadLogs).filter(MetaFileUploadLogs.belongsToLayout.in_(accessible_metadata_id)).all()
            else:
                datasets= session.query(MetaClientDatasets).filter(MetaClientDatasets.id==datasetId).all()

        else:
            if accessible_metadata_id:
                try:
                    if last_record_offset and total_records:
                        datasets=session.query(MetaClientDatasets).filter(MetaClientDatasets.id>last_record_offset).from_self().join(MetaFileUploadLogs).filter(MetaFileUploadLogs.belongsToLayout.in_(accessible_metadata_id)).limit(total_records).all()
                    else:
                        datasets = session.query(MetaClientDatasets).from_self().join(MetaFileUploadLogs).filter(MetaFileUploadLogs.belongsToLayout.in_(accessible_metadata_id)).all()
                except Exception as kr:
                    session.rollback()
                    datasets = session.query(MetaClientDatasets).from_self().join(MetaFileUploadLogs).filter(MetaFileUploadLogs.belongsToLayout.in_(accessible_metadata_id)).all()
            else:
                try:
                    if last_record_offset and total_records:
                        datasets=session.query(MetaClientDatasets).filter(MetaClientDatasets.id>last_record_offset).limit(total_records).all()
                    else:
                        datasets = session.query(MetaClientDatasets).all()
                except Exception as kr:
                    session.rollback()
                    datasets = session.query(MetaClientDatasets).all()

        if datasets:              
            return [data.serialized for data in datasets]
        else:
            name_space.abort(400,message='No Data found for the Dataset upload.')

    def delete(self,dataset_id=None):
        if dataset_id:
            dataset= session.query(MetaClientDatasets).filter(MetaClientDatasets.id==dataset_id)
            if(dataset.first()):
                dataset.delete(synchronize_session=False)
                session.commit()
                return {'message':'Delete successful', 'datasetId':dataset_id}
            else:
                return {'message': 'Aborting request. No Data found for the given input.'}


        else:
            name_space.abort(400,message='Aborting request, for invalid input.')

@name_space.route('/getMetaList/<int:user_id>')
class MetaDataAvailable(Resource):
    
    @login_required
    @name_space.marshal_with(metainfo_model_list,as_list=True)
    def get(self,user_id=None):
        if user_id:
            try:
                user_info = session.query(MetaUser).filter(MetaUser.userId==user_id).first()
            except:
                session.rollback()
                user_info = session.query(MetaUser).filter(MetaUser.userId==user_id).first()

            if not user_info:
                name_space.abort(400,message="No Such user Exists, Please Verify !!!")
            
            if (current_user.userId!=user_id):
                name_space.abort(403,message='Accessing other user information is prohibited')

            schema_allowed_touse = user_info.schemaAccess
            
            if not schema_allowed_touse:
                name_space.abort(400,message="No Schema is tagged to you. please contact your system admin to Tag on Schema for dataset uploading. ")
            
            elif 'ALL' in schema_allowed_touse:
                metadata_list = session.query(MetaLayout).all()
                return [ data.serialized for data in metadata_list]
            else:
                schema_id_needed = [ data.id for data in session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.datasourceName.in_(schema_allowed_touse)).all()]
                result_mapping = session.query(distinct(MetaLayoutSchemaMapping.layoutHead)).filter(MetaLayoutSchemaMapping.schemaName.in_(schema_id_needed)).all()
                result_metadata=session.query(MetaLayout).filter(MetaLayout.id.in_(result_mapping)).all()
                if result_metadata:
                    return [data.serialized for data in result_metadata]
                else:
                    name_space.abort(400,'No Metadata has been tagged to the schema that you are allowed to access. please contact your system Adminstrator')

        else:
            name_space.abort(400,message="User identity is expect for processing the request.")


@name_space.route('/filter')
class DatasetFilter(Resource):
    
    @login_required
    @name_space.expect(dataset_filter_get,validate=True)
    @name_space.marshal_with(dataset_model,envelope='data',as_list=True)
    def post(self):
        
        request_data = request.get_json(force=True)
        extra_payload=hasExtraPayload(request_data,dataset_filter_get)
        if extra_payload: 
            name_space.abort(403, message="Invalid key received for filtering :>"+extra_payload)

        userData=session.query(MetaClientDatasets)
        for key,value in request_data.items():
            if not value:
                    name_space.abort(400,message='Empty value for the filter is not expected at :'+str(key))
            try:
                this_attr=getattr(MetaClientDatasets,key)
                userData=userData.filter(this_attr.like(f'%{value}%'))
            except Exception as excep:
                session.rollback()
                name_space.abort(400,message=excep.__str__)

        if userData.first():
            return [data.serialized for data in userData.all()]
        else:
            name_space.abort(400,message="No data found for the provider filter keys.")