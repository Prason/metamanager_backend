from flask import request
from config import Session,checkUser,api,app,hasExtraPayload
from flask_restplus import Resource
from sqlalchemy.exc import SQLAlchemyError,DataError
from dbmodals.mymodals import MetaLayoutDataSource,MetaLayout,MetaLayoutSchemaMapping,MetaUser
from dbmodals.swaggermodels import datasource_get,datasource_model,datasource_post,datasource_put,datasource_model_list,datasource_user_map
from flask_login import login_required,current_user
import datetime

name_space = api.namespace('datasource','Handling the communication realated to the datasources')
session = Session()
session.rollback()
@name_space.route('/',methods=['GET','POST'])
@name_space.route('/<int:datasource_id>',methods=['GET','PUT','DELETE'])
@name_space.route('/<int:lastrecord_offset>/<int:total_new_rows>',methods=['GET'])
class DatasourceHandler(Resource):

    @login_required
    @name_space.marshal_with(datasource_get)
    def get(self,datasource_id=None,lastrecord_offset=None,total_new_rows=None):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        if (lastrecord_offset is not None) or (total_new_rows is not None):
            if total_new_rows>100 or total_new_rows<=0:
                name_space.abort(400, message='Row limit must be in range of 1 to 100')

        if datasource_id:
            datasource_data= session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.id==datasource_id).all()
        else:
            try:
                if lastrecord_offset and total_new_rows:
                    datasource_data=session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.id>lastrecord_offset).limit(total_new_rows).all()
                else:
                    datasource_data = session.query(MetaLayoutDataSource).all()
                    
            except Exception as kr:
                session.rollback()
                datasource_data = session.query(MetaLayoutDataSource).all()
        if datasource_data:
            return [data.serialized for data in datasource_data]
        else:
            name_space.abort(400,message='No datasource found matching for your request')
    
    @login_required
    @name_space.expect(datasource_post)
    def post(self):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data=request.get_json(force=True)
        try:
            new_datasource = MetaLayoutDataSource(
                datasourceName=request_data['datasource_name'],
                datasourceDescription=request_data['datasource_description'] if 'datasource_description' in request_data.keys() else '',
                createdBy=current_user.userId)

            if new_datasource.datasourceName:      
                session.add(new_datasource)
                session.commit()
                saved_datasource=session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.datasourceName==new_datasource.datasourceName).first()
                return {'status':200, 'message':'Record saved for new Datasource '+new_datasource.datasourceName,'datasource_id':saved_datasource.id}
            else:
                name_space.abort(400,message="Empty value for the datasource name is not allowed")

        except KeyError as kr:
            session.rollback()
            name_space.abort(400,message=kr.__str__())
        except SQLAlchemyError as err:
            session.rollback()
            errorstring = str(err.__str__)

            if (errorstring.find('fk_create_datasource_user')>0) or (errorstring.find('fk_update_datasource_user')>0):
                name_space.abort(400,message="User information doesnot exist for  "+str(request_data['user_id'])+", Please Verify.")
            elif errorstring.find('metalayout_datasources_datasourceName_key')>0:
                name_space.abort(403,message="Datasource Name as "+request_data['datasource_name']+" already exists.")
            else:
                name_space.abort(400,message=errorstring)
    
    @login_required
    def delete(self,datasource_id):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        try:
            datasource_data = session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.id==datasource_id)
            if not datasource_data.first():
                name_space.abort(400,message="No Data available for processing your change request")
            else:
                clearingResult=clearDatasourceMapping(datasource_id,datasource_data.first().datasourceName)

            if not clearingResult:
                datasource_data.delete(synchronize_session=False)
                session.commit()
                return {'message':'Delete Successful', 'status':200}
            else:
                name_space.abort(400,message="Unable To perform Datasource Delete request", defect=clearingResult)
      
        except Exception as err:
            session.rollback()
            name_space.abort(400, message="Invalid Reqest made, please verify"+err.__str__())

    @login_required
    @name_space.expect(datasource_put)
    @name_space.marshal_with(datasource_get)
    def put(self,datasource_id):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data=request.get_json()
        extra_payload = hasExtraPayload(request_data['newDatasourceData'],datasource_model)
        if extra_payload:
            name_space.abort(400,message='Unexpected key at payload :> '''+extra_payload)

        datasource = session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.id==datasource_id)
        if not datasource.first():
            name_space.abort(400,message="No Data available for processing your change request")
        else:
            try:
                request_data['newDatasourceData']['updatedBy']=current_user.userId
                if request_data['newDatasourceData']['updatedBy']:
                    request_data['newDatasourceData']['updatedOn']=datetime.datetime.now()
                    old_name=datasource.first().datasourceName
                    datasourceData = datasource.update(request_data['newDatasourceData'],synchronize_session=False)
                    datasource_newdata = datasource.first()
                    session.commit()
                    if not datasource_newdata:
                        name_space.abort(400,message="No data found:"+str(layout_id))
                    else:
                        try:
                            usersData=session.query(MetaUser).all()                
                            for user in usersData:
                                if old_name in user.schemaAccess:
                                    user.schemaAccess.remove(old_name)
                                    user.schemaAccess.append(datasource_newdata.datasourceName)
                                    session.query(MetaUser).filter(MetaUser.userId==user.userId).update({'schemaAccess':user.schemaAccess})
                        except:
                            session.rollback()
                            name_space.abort(400,message="Datasource name updated but couldn't process user assignment to new datasource name")
                        
                        session.commit()
                        return datasource_newdata.serialized
                else:
                    name_space.abort(400,message='Cannot process the data,please verify the user Information')
            except KeyError as ke:
                session.rollback()
                name_space.abort(400,message='Missing updator Infromation')
            except DataError as derr:
                session.rollback()
                name_space.abort(400,message="Invalid Datasource Name provide, maximum length is 60 character")
            except Exception as err:
                session.rollback()
                name_space.abort(400,message=err.__str__())

def clearDatasourceMapping(datasource_id,datasourceName):
    try:
        session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.schemaName==datasource_id).delete(synchronize_session=False)
        usersData=session.query(MetaUser).all()
                
        for user in usersData:
            if datasourceName in user.schemaAccess:
                user.schemaAccess.remove(datasourceName)
                session.query(MetaUser).filter(MetaUser.userId==user.userId).update({'schemaAccess':user.schemaAccess})
        
        session.commit()
        return None #none means no error here
    except SQLAlchemyError as exc:
        session.rollback()
        return exc.__str__
    except Exception as exc:
        session.rollback()
        return exc.__str__

@name_space.route('/user/save')
class DatasourceToUserMap(Resource):

    @login_required
    @name_space.expect(datasource_user_map)
    def post(self):
        if not checkUser('ADMIN'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        usercounter=0
        request_data = request.get_json()
        # try:
        for mapping_Detail in request_data['MappingDetail']:
            datasourceData = session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.id==mapping_Detail['datasource_id']).first()
            if datasourceData:  
                for userid in mapping_Detail['userids']:
                    userdata = session.query(MetaUser).filter(MetaUser.userId==int(userid))
                    if not userdata.first():
                        if usercounter:
                            name_space.abort(400,message=str(usercounter)+' user(s) mapped with datasources, but Not all users in the request are Valid')

                        name_space.abort(400,message='Not all users in the request are Valid')
                    datasources = set(userdata.first().schemaAccess)
                    datasources.add(datasourceData.datasourceName)
                    userdata.update({'schemaAccess':list(datasources)})
                    usercounter+=1
            else:
                if usercounter:
                    name_space.abort(400,message=str(usercounter)+' user(s) mapped with datasources,Not all datasources in the request are Valid')

                name_space.abort(400,message='Invalid datasource provided')
        
        session.commit()
        return {'message': str(usercounter)+' user(s) mapped with datasources'}
        # except Exception as exce:
        #     session.rollback()
        #     name_space.abort(400,message=str(exce))





@name_space.route('/access/<int:user_id>',defaults=None)
class DatasourceRestriction(Resource):

    @login_required
    @name_space.marshal_with(datasource_model_list)
    def get(self,user_id=None):
        if user_id or user_id<0:
            user_info=session.query(MetaUser).filter(MetaUser.userId==user_id).first()
            if not user_info:
                name_space.abort(400,message="No Such User exists. Please Verify !!!")

            schema_allowed_touse = user_info.schemaAccess

            if not schema_allowed_touse:
                name_space.abort(400,message="No Schema is tagged to you. please contact your system admin to Tag on Schema for dataset uploading. ")
            
            elif 'ALL' in schema_allowed_touse:
                datasource_list = session.query(MetaLayoutDataSource).all()
                
            else:
                datasource_list = session.query(MetaLayoutDataSource).filter(MetaLayoutDataSource.datasourceName.in_(user_info.schemaAccess)).all()

            if datasource_list:
                return [ data.serialized for data in datasource_list]
            else:
                name_space.abort(400,message="No Datasource has been defined yet. Please contact your admin to define them first.")
        else:
            name_space.abort(400,message="No Such User exists. Please Verify !!!")
        