from config import app,Session,ALLOWED_DATATYPES, ALLOWED_EXTENSION,api,checkUser,cloud_storageisAt
from config.dummyGenerator import *
from dbmodals.mymodals import MetaLayout,MetaLayoutDetail,MetaFileUploadLogs, MetaClientDatasets,MetaLayoutSchemaMapping
from dbmodals.swaggermodels import meta_data_upload_stream,meta_data_upload_bucket,dataset_upload_stream,dataset_upload_bucket,metadata_preverification_model
from flask_json import json_response, jsonify, JsonError
from flask_restplus import Resource
from flask import request,send_file
from sqlalchemy.exc import IntegrityError,DataError,SQLAlchemyError
from os.path import os
from datetime import datetime
import re,random,chardet
import pandas as pd
from werkzeug.datastructures import FileStorage
from flask_login import login_required,current_user
from metaengine import process_csv_for_metadata,process_excel_for_metadata,perform_dataqc_for_excel,perform_dataqc_for_csv
from metaengine.datavalidator import initiateDataLevelValidation
from config.socketDispatcher import handle_dataset_message
try:
    if cloud_storageisAt=='GOOGLE':
        from config.pushtocloud import *
    elif cloud_storageisAt=='MICROSOFT':
        from config.pushtocloudAzure import *
    else:
        raise Exception("Application cannnot start without the cloud Configurations. please verify !!!")
except:
    print("Couldnot startup the application due to cloud confiuguration error in the server please verify !!!")
    exit(0)

name_space = api.namespace('file','Handling all Files related data communication')
session= Session()
session.rollback()
@name_space.route('/test',doc=False)
class DMainClass(Resource):
    def get(self):
        return {
        "status": "Got new data"
        }
    def post(self):
        return {
        "status": "Posted new data"
        }

@name_space.route('/metadata/large')
class MetadataImportFromCloud(Resource):

    @login_required
    @name_space.expect(meta_data_upload_bucket)    
    @name_space.marshal_with(metadata_preverification_model,skip_none=True)
    def post(self):
        request_data = meta_data_upload_bucket.parse_args()
        meta_import = MetadataImport()
        try:
            import_result = meta_import.post(internalPayload=request_data)
            if 'metadata_name' in import_result.keys():
                return import_result
            else:
                name_space.abort(400,message=str(import_result))
        except KeyError as kr:
                name_space.abort(400,message=str(kr))
        except UnicodeDecodeError as ex:
                name_space.abort(400,message=str(ex))
        # except Exception as verr:
        #         name_space.abort(400,message=str(verr))
        
'''

@name_space.route('/metadata/')

    API for the layout and detail importing feature from the files
    
    Response for Post Request:
        {
            message: "File Uploaded Successfully"
        }
    Payload for the POST request:
        
        form.file = file uploaded with enctype=multipart/form-data
        form.uploaded_by = form field
        form.name=metadataname 
        form.description =metadatadescription 


'''

@name_space.route('/metadata/')
class MetadataImport(Resource):

    def saveMetadataImport(self,filename):
        if cloud_storageisAt=='MICROSOFT':
            save_metadata_import_file_az(filename)
        elif cloud_storageisAt=='GOOGLE':
            save_metadata_import_file(filename)
        else:
            name_space.abort(400,message='Cloud Storage either not defined or is Incorrect in the server. please Verify !!!')


    @login_required
    @name_space.expect(meta_data_upload_stream,validate=True)
    @name_space.marshal_with(metadata_preverification_model,skip_none=True)
    def post(self,internalPayload=None,LimitedFileSize=True):
        #LimitedFileSize=False
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        request_data = meta_data_upload_stream.parse_args() if not internalPayload else internalPayload
        print(request_data)
        if 'metadata_file' in request_data.keys():
            myfile=request_data['metadata_file']
        elif 'metadata_filename' in request_data.keys(): 
            try:
                filename=request_data['metadata_filename']
                try:
                    filetype=(filename.rsplit('.',1)[1]).lower()
                except:
                    name_space.abort(400,message="File doesn't have extenstion")

            except KeyError as kr:
                name_space.abort(400,message="File name is not received for processing.")

            if valid_fileExtension(filetype):   
                if cloud_storageisAt=='MICROSOFT':
                    file_obj=get_metadata_file_az(filename)
                elif cloud_storageisAt=='GOOGLE':
                    file_obj=get_metadata_file(filename)
                else:
                    name_space.abort(400,message="Cloud Storage either not defined or is Incorrect in the server. please Verify !!!")

                if isinstance(file_obj,FileStorage):
                    print('file Received from the cloud storage is '+file_obj.filename)
                    myfile=file_obj
                elif isinstance(file_obj,str):
                    name_space.abort(400,message=file_obj)
            else:
                msg='Requested file doesnot have valid names and/or extension('+str(ALLOWED_EXTENSION)+')'
                name_space.abort(400,message=msg)

        if myfile:
            try:
                filetype=(myfile.filename.rsplit('.',1)[1]).lower()
            except:
                name_space.abort(400,message="File doesn't have extenstion")

            size_bound_in_kb=25500                 
            try:
                uploader = current_user.userId
                name=request_data['name']
                desc=request_data['description'] if 'description' in request_data.keys() else ''
                hasHeaderonTop=request_data['header_ontop'] if 'header_ontop' in request_data.keys() else False
                hasHeaderonRow=request_data['header_onrow'] if 'header_onrow' in request_data.keys() else 0
                csvSeperator=request_data['csv_seperator'] if 'csv_seperator' in request_data.keys() else ','
                footerRows=request_data['footer_rows'] if 'footer_rows' in request_data.keys() else 0
                data_encoding=request_data['data_encoding'] if 'data_encoding' in request_data.keys() else 0
                data_in_file=int(request_data['file_size']) if 'file_size' in request_data.keys() else 0

                if csvSeperator not in [',','|','\\t','~']:
                    name_space.abort(400,message="Invalid seperator received for the file. please use ',','|','\\t','~' only. ")

                if hasHeaderonTop and hasHeaderonRow:
                    name_space.abort(400,message='Ambiguious input received, Header is at the top or on row '+str(hasHeaderonRow)+'??')
                
                meta_data_pre_verification_list={'metadata_name':name,'metadata_description':desc,'headerAtRow':hasHeaderonRow,'footerRowCount':footerRows}

            except KeyError as krr:
                msg='Insufficient information in the payload'
                name_space.abort(400, message=msg)

            if valid_fileExtension(filetype):  
                if not data_encoding:
                    enc=chardet.detect(myfile.read(int(size_bound_in_kb)*1024))
                    print(str(enc))
                    data_encoding=enc['encoding']   

                if not data_encoding:
                    data_encoding='utf-8'     

                print( 'encoding is '+str(data_encoding))
                myfile.seek(0,0)            
                try:
                    if LimitedFileSize:
                        if not data_in_file:
                            if filetype in ['xlsx','xlsb']:
                                data_in_file=len(myfile.read((int(size_bound_in_kb)*1024)+10))
                            else:
                                data_in_file=len(myfile.read((int(size_bound_in_kb)*1024)+10).decode(data_encoding))

                except UnicodeDecodeError as ex:
                    print(ex.__str__())
                    encodingUsed,defectText,defectPosition,nextPosition,errorReport=ex.args
                    msg='Unrecognizable character found in the file.Data in the file is not as per the ('+encodingUsed+') standard.'
                    defect = defectText[max(defectPosition-20,0):nextPosition+20].decode(data_encoding,'replace')
                    cause=defect[20:21]
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(0)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=msg+' : '+str(defect))
                    name_space.abort(400,message=msg,defect=defect,causedby=cause)
                
                except Exception as verr:
                    name_space.abort(400,message=verr.__str__())

                file_size  = round(data_in_file/1024,2)
                if LimitedFileSize and  (file_size>size_bound_in_kb):
                    msg='File size limit exceeded, Only upto 25MB size allowed here.'
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                    name_space.abort(400,message=msg)      
                elif (data_in_file<10):
                    msg='Please dont submit the empty file that has no any valid data to process'
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                    name_space.abort(400,message=msg) 

                myfile.seek(0,0)
                target_file_name=str(myfile.filename).rsplit('.',1)[0]+'_'+str(round(datetime.now().timestamp()))+'.'+filetype
                print('target file name is '+str(target_file_name))

                try:
                    if filetype in ['csv','txt']:
                        #saving the file first because its stream is closed for the csv and txt format by panda import
                        myfile.save(os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+target_file_name))
                        myfile.seek(0,0)
                        meta_data_pre_verification_list['metadata_details']=process_csv_for_metadata(myfile,filetype,seperator=csvSeperator,headeronTop=hasHeaderonTop,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding)
                    elif filetype=='xlsx':
                        meta_data_pre_verification_list['metadata_details']=process_excel_for_metadata(myfile,filetype,headeronTop=hasHeaderonTop,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding,engineparser='xlrd')
                    elif filetype=='xlsb':
                        meta_data_pre_verification_list['metadata_details']=process_excel_for_metadata(myfile,filetype,headeronTop=hasHeaderonTop,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding,engineparser='pyxlsb')
                    
                except Exception as ex:
                    #save the file if something went wrong during fiile processing due to data
                    try:
                        if filetype in ['csv','txt']:
                            os.rename(os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+target_file_name),os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+'ERR_'+target_file_name))
                        elif filetype in ['xlsx','xlsb']:
                            myfile.seek(0,0)
                            myfile.save(os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+'ERR_'+target_file_name))

                        self.saveMetadataImport('ERR_'+target_file_name)
                    except:                        
                        pass

                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=str(ex))
                    name_space.abort(400,tips='please provide the header row information if file contains other information also',message='Invalid file Uploaded, Column mismatched in the uploaded file:'+str(myfile.filename),
                    defect=str(ex))

                #if everything works fine then we save the file as backup for future reference.
                try:
                    if filetype in ['xlsx','xlsb']:
                        myfile.seek(0,0)
                        myfile.save(os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+target_file_name))
                    
                    self.saveMetadataImport(target_file_name)
                except:
                    pass
                # file_obj_backup.close()
                print('file is saved')
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks='COMPLETED')
                # print(meta_data_pre_verification_list)
                return meta_data_pre_verification_list      

            else:
                msg='Uploaded file doesnot have valid names and/or extension('+str(ALLOWED_EXTENSION)+')'
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(0)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                name_space.abort(400,message=msg)

            return{ 'status':'SUCCESS','message':'File is uploaded successfully main block.'}
        else:
            name_space.abort(400,message='No file is uploaded, Missing file data ')


'''
    This is the api designed for  generating the sample csv file that should be uploaded,
    with the data inside it representing how the metadata information and attributes are to be arranged

'''

@name_space.route('/metadata/sample/<filetype>',doc=False)
@name_space.route('/metadata/sample',doc=False)
class MetadataDownloader(Resource):

    @login_required
    def get(self,filetype=None):
        if checkUser('PUBLISHER'):
            name_space.abort(403,message="Please contact your sytem adminstrator for this request")

        try:
            if not filetype:
                return send_file('../file_structure_format.csv',as_attachment=True)
            else:
                return send_file('../file_structure_formatexcel.xlsx',as_attachment=True)
        except:
            return {'status':200, 'message': 'Sample File is not available for download'}


@name_space.route('/dataqc/log/<log_for>')
class DataQCLogDownloader(Resource):

    @login_required
    def get(self,log_for=None):
        try:
            if isinstance(log_for,int):
                #Trying to fetch the  log for previous execution
                dataset=session.query(MetaClientDatasets).filter(MetaClientDatasets.id==log_for).first()
                if dataset.uploadDetail:
                    logDetail=session.query(MetaFileUploadLogs).filter(MetaFileUploadLogs.id==dataset.uploadDetail).first()
                    filename=str(logDetail.savedFileName).rsplit('.',1)[0]
                    print('file name to fetch is '+str(filename))
                    #file_tosend=prepare_zip_to_upload(os.path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+filename+'.log'))
                    file_tosend=os.path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+filename+'.log')
                    return send_file(file_tosend,as_attachment=True)
                else:
                    name_space.abort(400,message="No log Available for your Request")
            else:
                filename=str(log_for).rsplit('.',1)[0]
                file_tosend=prepare_zip_to_upload(os.path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+filename+'.log'))
                #file_tosend=os.path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+filename+'.log')
                return send_file(file_tosend,as_attachment=True)

        except Exception as exc:
            print(exc)
            session.rollback()
            name_space.abort(400, message='No log Available for your Request')

''' 

    This is the api designed to provide the sample data that the user can use as the reference 
    in order to uploade the dataset  related to the particular metadata

'''

@name_space.route('/dataset/<int:metadata_id>/sample')
@name_space.route('/dataset/<int:metadata_id>/sample/<anystring>')
class DatasetFormatDownloader(Resource):
    def saveTempFile(self,filename):
        if cloud_storageisAt=='MICROSOFT':
            save_temp_file_az(filename)
        elif cloud_storageisAt=='GOOGLE':
            save_temp_file(filename)
        else:
            name_space.abort(400,message='Cloud Storage either not defined or is Incorrect in the server. please Verify !!!')


    @login_required
    def get(self,metadata_id,anystring=None):
        meta_header = session.query(MetaLayout).filter(MetaLayout.id==metadata_id).first()
        if meta_header:
            metalayoutdetails = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==metadata_id).order_by(MetaLayoutDetail.fieldOrder).all()
            if metalayoutdetails:
                metadetails_header=['Attribute_identifier','Attribute_name','Attribute_description','Attribute_data_type','Attribute_min_length','Attribute_max_length','Attribute_ListOFValues','Attribute_null_support','Attribute_regex_check_string']
                decorate_bar="="*150
                column_info=''
                column_dummy=''
                all_column_lists=[]
                # 'NUMERIC','FLOAT','TEXT','DATETIME','DATE','ENUM','ARRAY','BOOLEAN','IPADDRESS','EMAIL','URL'
                dumping_metadata_detail=[]
                for i in range(10):
                    column_dummy=''
                    for detail in metalayoutdetails:
                        if i==1:
                            dumping_metadata_detail.append(detail.detailList)
                        if i==0:
                            # if (detail._attrType=='ENUM'):
                                # enumList = ','.join(detail.attrLOV)
                            #     column_info=column_info+detail.attrIdentifier+"( "+enumList+" )|"
                            # else:
                                column_info=column_info+detail.attrIdentifier+" |"
                        
                        if(detail._attrType=='FLOAT'):
                            column_dummy=column_dummy+get_float(detail.attrMinLength,detail.attrMaxLength)+"|"
                        elif(detail._attrType=='NUMERIC'):
                            column_dummy=column_dummy+get_numeric(detail.attrMinLength,detail.attrMaxLength)+"|"
                        elif(detail._attrType=='TEXT'):
                            column_dummy=column_dummy+get_Text()+"|"
                        elif(detail._attrType=='DATETIME'):
                            column_dummy=column_dummy+get_datetime()+"|"
                        elif(detail._attrType=='DATE'):
                            column_dummy=column_dummy+get_date()+"|"
                        elif(detail._attrType=='ENUM'):
                            column_dummy=column_dummy+get_enum(detail.attrLOV)+"|"
                        elif(detail._attrType=='ARRAY'):
                            column_dummy=column_dummy+get_array()+"|"
                        elif(detail._attrType=='BOOLEAN'):
                            if detail.atrrRegex:
                                try:
                                    possible_val = str(detail.atrrRegex)[1:-1].split('|')
                                except:
                                    possible_val=None
                            else:
                                possible_val=None
                            column_dummy=column_dummy+get_Boolean(possible_val)+"|"
                        elif(detail._attrType=='IPADDRESS'):
                            column_dummy=column_dummy+get_ipaddress()+"|"
                        elif(detail._attrType=='EMAIL'):
                            column_dummy=column_dummy+get_email()+"|"
                        elif(detail._attrType=='URL'):
                            column_dummy=column_dummy+get_url()+"|"
                        else:
                            column_dummy=column_dummy+" "+"|"
                        
                    all_column_lists.append(column_dummy[0:-1]+'\n')
                try:
                    #now we generate the file to be downloaded 
                    file_name=str(meta_header.layoutHead).replace(" ","_")+'_sample_'+str(round(datetime.now().timestamp()))
                    with open('./filestorage/tempfile/'+file_name+".csv",'w',encoding='utf-8') as sample_file:
                        '' if anystring else sample_file.writelines("FORMAT\n")
                        '' if anystring else sample_file.writelines(decorate_bar+'\n') 
                        sample_file.writelines(column_info[0:-1]+'\n')
                        '' if anystring else sample_file.writelines(decorate_bar+'\n\n')
                        '' if anystring else sample_file.writelines("Note: Please upload only '|' delimited value as shown below(5 Data Rows) without this header.\n")
                        
                        for data in all_column_lists:
                            sample_file.writelines(data)
                    if anystring:
                        fileisAt=os.path.abspath('./filestorage/tempfile/'+file_name)
                        datasetData = pd.read_csv(fileisAt+'.csv',sep='|')
                        #Also add the new sheet for complete snap of the attribute of metadata
                        attribute_info_data=pd.DataFrame(dumping_metadata_detail)
                        with pd.ExcelWriter(fileisAt+'.xlsx',encoding='utf-8') as writer:
                            datasetData.to_excel(writer,sheet_name='samplesheet',index=False)
                            attribute_info_data.to_excel(writer,sheet_name='Columns Details',index=False,header=metadetails_header)
                        self.saveTempFile(file_name+'.xlsx')
                        return send_file(fileisAt+'.xlsx',as_attachment=True)
                    self.saveTempFile(file_name+'.csv') 
                    return send_file(os.path.abspath('./filestorage/tempfile/'+file_name)+'.csv',as_attachment=True)
                    
                except Exception as err:
                    name_space.abort(400, message=err.__str__)
            else:
                return { 'status': 200, 'message': 'Details for the layout '+meta_header.layoutHead+' has not been uploaded to Metadata manager Yet. Please upload details first.'}
        
        else:
            return { 'status': 400, 'message': 'Please supply valid MetadataInformation to get the associated sample data'}


        return {'status':200}

@name_space.route('/dataset/large')
class DatasetImportFromCloud(Resource):

    @login_required
    @name_space.expect(dataset_upload_bucket)
    def post(self):
        request_data = dataset_upload_bucket.parse_args()
        dataset_import = DataSetImportManager()
        try:
            import_result = dataset_import.post(internalPayload=request_data)
            if 'type' in import_result.keys():
                return import_result
            else:
                name_space.abort(400,message=str(import_result))
        except KeyError as kr:
                name_space.abort(400,message=str(kr))
        except UnicodeDecodeError as ex:
                name_space.abort(400,message=str(ex))
        # except Exception as verr:
        #         name_space.abort(400,message=str(verr))

'''
    API for the dataset importing feature from the client file as is uploaded.
    This will act like the FTP client service
    
    Response for Post Request:
        {
            message: "File Uploaded Successfully"
        }
    Payload for the POST request:
        
        form.file = file uploaded with enctype=multipart/form-data
        form.uploaded_by = form field
        form.name=metadataname or datasetname
        form.description =metadatadescription or associateddatasetname


'''

@name_space.route('/dataset/')
class DataSetImportManager(Resource):

    def datasetImportSave(self,filename):
        if cloud_storageisAt=='MICROSOFT':
            save_dataset_import_file_az(filename)
        elif cloud_storageisAt=='GOOGLE':
            save_dataset_import_file(filename)
        else:
            name_space.abort(400,message='Cloud Storage either not defined or is Incorrect in the server. please Verify !!!')

    @login_required
    @name_space.expect(dataset_upload_stream,validate=True)
    def post(self,internalPayload=None,LimitedFileSize=True):
        LimitedFileSize=False
        request_data = dataset_upload_stream.parse_args() if not internalPayload else internalPayload
        print(request_data)
        if 'dataset_file' in request_data.keys():
            myfile=request_data['dataset_file']
        elif 'dataset_filename' in request_data.keys(): 
            try:
                filename=request_data['dataset_filename']
                try:
                    filetype=(filename.rsplit('.',1)[1]).lower()
                except:
                    name_space.abort(400,message="File doesn't have extenstion")

            except KeyError as kr:
                name_space.abort(400,message="File name is not received for processing.")

            if valid_fileExtension(filetype):  
                if cloud_storageisAt=='MICROSOFT':
                    file_obj=get_dataset_file_az(filename)
                elif cloud_storageisAt=='GOOGLE':
                    file_obj=get_dataset_file(filename)
                else:
                    name_space.abort(400,message='Cloud Storage either not defined or is Incorrect in the server. please Verify !!!')

                if isinstance(file_obj,FileStorage):
                    print('datasetfile Received from the cloud storage is '+file_obj.filename)
                    myfile=file_obj
                elif isinstance(file_obj,str):
                    name_space.abort(400,message=file_obj)
            else:
                msg='Requested file doesnot have valid names and/or extension('+str(ALLOWED_EXTENSION)+')'
                name_space.abort(400,message=msg)

        if myfile:
            try:
                filetype=(myfile.filename.rsplit('.',1)[1]).lower()
            except:
                name_space.abort(400,message="File doesn't have extenstion")

            dataset_import_logs=[]
            size_bound_in_kb=25600
            final_data_list=[]
            try:
                uploader = current_user.userId
                name=request_data['name'] #here will be the dataset name
                desc=request_data['description'] if 'description' in request_data.keys() else 0 #here will be the metadata name
                hasHeaderonTop=request_data['header_ontop'] if 'header_ontop' in request_data.keys() else False 
                hasHeaderonRow=request_data['header_onrow'] if 'header_onrow' in request_data.keys() else 0
                csvSeperator=request_data['csv_seperator'] if 'csv_seperator' in request_data.keys() else ','
                footerRows=request_data['footer_rows'] if 'footer_rows' in request_data.keys() else 0
                data_encoding=request_data['data_encoding'] if 'data_encoding' in request_data.keys() else 0
                data_in_file=request_data['file_size'] if 'file_size' in request_data.keys() else 0
                override_headerNfooter=request_data['override_headfoot'] if 'override_headfoot' in request_data.keys() else False

                if csvSeperator not in [',','|','\\t','~']:
                    name_space.abort(400,message="Invalid seperator received for the file. please use ',','|','\\t','~' only. ")

                if hasHeaderonTop and hasHeaderonRow:
                    name_space.abort(400,message='Ambiguious input received, Header is at the top or on row '+str(hasHeaderonRow)+'??')
                
            except KeyError as krr:
                msg='Insufficient information in the payload'
                name_space.abort(400, message=msg)
            if valid_fileExtension(filetype):
                if not data_encoding:
                    enc=chardet.detect(myfile.read(size_bound_in_kb*1024))                    
                    print(str(enc))
                    data_encoding=enc['encoding']   
                if not data_encoding:
                    data_encoding='utf-8'  

                dataset_import_logs.append({'type':'SUCCESS','message':'Received File Encoding of '+data_encoding,'logtime':str(datetime.now())})
                handle_dataset_message({'type':'SUCCESS','message':'Received File Encoding of '+data_encoding})
                print( 'encoding is '+str(data_encoding))
                myfile.seek(0,0)            
                try:
                    if LimitedFileSize:
                        if not data_in_file:
                            if filetype in ['xlsx','xlsb']:
                                data_in_file=len(myfile.read((size_bound_in_kb*1024)+10))
                            else:
                                data_in_file=len(myfile.read((size_bound_in_kb*1024)+10).decode(data_encoding))

                except UnicodeDecodeError as ex:
                    print(ex.__str__())
                    encodingUsed,defectText,defectPosition,nextPosition,errorReport=ex.args
                    dataset_import_logs.append({'type':'FAIL','message':'Unparsable data found as per the encoding ('+encodingUsed+') standard.'})
                    handle_dataset_message({'type':'FAIL','message':'Unparsable data found as per the encoding ('+encodingUsed+') standard.','logtime':str(datetime.now())})
                    msg='Unrecognizable character found in the file.Data in the file is not as per the ('+encodingUsed+') standard.'
                    defect = defectText[max(defectPosition-20,0):nextPosition+20].decode(data_encoding,'replace')
                    cause=defect[20:21]
                    save_upload_log(name=name,description='-',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(0)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks=msg+' : '+str(defect))
                    name_space.abort(400,message=msg,defect=defect,causedby=cause)
                
                except Exception as verr:
                    name_space.abort(400,message=verr.__str__())

                #checking if the user has already uploaded the file with the given name
                dataset_record = session.query(MetaClientDatasets).filter(MetaClientDatasets.datasetName==name).first()
                if dataset_record:
                    dataset_import_logs.append({'type':'FAIL','message':'Redundant Dataset Name as :'+str(name),'logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'Redundant Dataset Name as :'+str(name)})
                    name_space.abort(400, message="Already uploaded the File with the given dataset name.Please verify or change the name for reuploading.")

                file_size  = round(data_in_file/1024,2)
                if LimitedFileSize and (file_size>size_bound_in_kb):
                    dataset_import_logs.append({'type':'FAIL','message':'File Size limit Exceeded '+str(file_size)+'KB','logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'File Size limit Exceeded '+str(file_size)+'KB'})
                    msg='File size limit exceeded, Only upto '+str(size_bound_in_kb/1024)+' MB size allowed'
                    save_upload_log(name=name,description='-',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks=msg)
                    name_space.abort(400,message=msg)
                elif (data_in_file<10):
                    dataset_import_logs.append({'type':'FAIL','message':'Empty File For processing '+str(file_size)+'KB','logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'Empty File For processing '+str(file_size)+'KB'})
                    msg='Please dont submit the empty file without any valid data to process'
                    save_upload_log(name=name,description='-',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks=msg)
                    name_space.abort(400,message=msg)            
                
                dataset_import_logs.append({'type':'SUCCESS','message':'File Size Is under Accectable limit of '+str(size_bound_in_kb/1024)+' MB:: '+str(file_size)+'KB','logtime':str(datetime.now())})
                handle_dataset_message({'type':'SUCCESS','message':'File Size Is under Accectable limit.'+str(size_bound_in_kb/1024)+' MB:: '+str(file_size)+'KB'})
                myfile.seek(0,0)
                save_upload_log(name=name,description='-',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks='File size is under the valid size limit with acceptable extenstion')
                layout_head = session.query(MetaLayout).filter(MetaLayout.id==desc).first()
                if not layout_head:
                    dataset_import_logs.append({'type':'FAIL','message':'Invalid Metadata information supplied','logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'Invalid Metadata information supplied'})
                    msg='No such MetaData information exists, please verify'
                    save_upload_log(name=name,description='Invalid Metadata',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks=msg)
                    name_space.abort(400,message=msg)
                else:
                    dataset_import_logs.append({'type':'SUCCESS','message':'QC begin against metadata '+layout_head.layoutHead,'logtime':str(datetime.now())})
                    handle_dataset_message({'type':'SUCCESS','message':'QC begin against metadata '+layout_head.layoutHead})
                    save_upload_log(name=name,description=layout_head.layoutHead,uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(file_size)+'KB',belongsToLayout=None,uploadedBy=uploader,isDataFile=True,remarks='Dataset QC Started')
                    #Update the header on row and footer on row based on the information in the database.
                    if not override_headerNfooter:
                        hasHeaderonRow=layout_head.headerAtRow
                        footerRows=layout_head.footerRowCount
                        
                    layout_details = session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==layout_head.id).order_by(MetaLayoutDetail.fieldOrder).all()
                    total_fields=len(layout_details)   

                target_file_name=layout_head.layoutHead+"_"+str(round(datetime.now().timestamp()))+'.'+filetype
                #saving the log for begininning of the metainformation import process
                if total_fields:
                    save_upload_log(name=name,description=layout_head.layoutHead,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isDataFile=True,remarks='DatasetValidation import started')
                else:
                    dataset_import_logs.append({'type':'FAIL','message':'Attempt of doing QC against the empty Metadata. '+layout_head.layoutHead,'logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'Attempt of doing QC against the empty Metadata. '+layout_head.layoutHead})
                    msg='Attempt of doing QC against the empty Metadata.'+layout_head.layoutHead
                    save_upload_log(name=name,description=layout_head.layoutHead,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isDataFile=True,remarks=msg)
                    save_log_file_fs(target_file_name,dataset_import_logs)
                    name_space.abort(400,message=msg,log_file=target_file_name)
                    
                print('target file name is '+str(target_file_name))           
                print("path is "+os.path.abspath(app.config['DATASET_FOLDER']+'\\'+target_file_name))
                dataset_import_logs.append({'type':'SUCCESS','message':'File Extenstion accepted as '+filetype,'logtime':str(datetime.now())})
                handle_dataset_message({'type':'SUCCESS','message':'File Extenstion accepted as '+filetype})
                try:
                    if filetype in ['xlsx']:
                        quality_check_error = perform_dataqc_for_excel(myfile,filetype,hasHeaderonTop,layout_details,qc_logs=dataset_import_logs,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding,engineparser='xlrd')
                    elif filetype=='xlsb':
                        quality_check_error = perform_dataqc_for_excel(myfile,filetype,hasHeaderonTop,layout_details,qc_logs=dataset_import_logs,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding,engineparser='pyxlsb')
                    elif filetype in ['csv','txt']:
                        myfile.save(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+target_file_name))
                        myfile.seek(0,0)
                        quality_check_error=perform_dataqc_for_csv(myfile,filetype,hasHeaderonTop,layout_details,qc_logs=dataset_import_logs,seperator=csvSeperator,headeronRow=hasHeaderonRow,footerRows=footerRows,encodingUsed=data_encoding)
                except Exception as ex:
                    #save the file if something went wrong during fiile processing due to data or file
                    try:
                        if filetype in ['csv','txt']:
                            os.rename(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+target_file_name),os.path.abspath(app.config['DATASET_FOLDER']+'\\'+'DERR_'+target_file_name))
                        elif filetype in ['xlsx','xlsb']:
                            myfile.seek(0,0)
                            myfile.save(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+'DERR_'+target_file_name))
                        
                        # save_dataset_import_file('DERR_'+target_file_name)
                        self.datasetImportSave('DERR_'+target_file_name)
                    except:                        
                        pass

                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isDataFile=True,remarks=str(ex))
                    name_space.abort(400,tips='please provide the header row information if file contains other information also',message='Invalid file Uploaded, Column mismatched in the uploaded file:'+str(myfile.filename),
                    defect=str(ex))

                if quality_check_error['message'] is None:
                    dataset_import_logs=quality_check_error['Log_list']
                    #also save the log int the database about the validation completion
                    dataset_import_logs.append({'type':'SUCCESS','message':'All QC Passed','logtime':str(datetime.now())})
                    handle_dataset_message({'type':'SUCCESS','message':'All QC Passed'})
                    logging_id=save_upload_log(name=name,description=layout_head.layoutHead,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isDataFile=True,remarks='COMPLETED')
                    dataset_upload_record=MetaClientDatasets(datasetName=name,uploadDetail=logging_id)
                    session.add(dataset_upload_record)
                    session.commit()

                    #if everything works fine then we save the file as backup for future reference.
                    try:
                        if filetype in ['xlsx','xlsb']:
                            myfile.seek(0,0)
                            myfile.save(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+target_file_name))
                        
                        # save_dataset_import_file(target_file_name)
                        self.datasetImportSave(target_file_name)
                    except:
                        pass
                    #also save the log generated for the files
                    save_log_file_fs(target_file_name,dataset_import_logs)
                    return {'type':'SUCCESS','message':'File QC Validation successful','log_file':target_file_name} 
                else:
                    dataset_import_logs=quality_check_error['Log_list']
                    #if something wrong happens at the qc level then we store the file as backup for future reference.
                    try:
                        if filetype in ['csv','txt']:
                            os.rename(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+target_file_name),os.path.abspath(app.config['DATASET_FOLDER']+'\\'+'DERR_'+target_file_name))
                        elif filetype in ['xlsx','xlsb']:
                            myfile.seek(0,0)
                            myfile.save(os.path.abspath(app.config['DATASET_FOLDER']+'\\'+'DERR_'+target_file_name))

                        # save_dataset_import_file('DERR_'+target_file_name)
                        self.datasetImportSave('DERR_'+target_file_name)
                    except:                        
                        pass
                    
                    save_upload_log(name=name,description=layout_head.layoutHead,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isDataFile=True,remarks=str(quality_check_error['message']))
                    dataset_import_logs.append({'type':'FAIL','message':'QC Failed due to '+str(quality_check_error['message']),'logtime':str(datetime.now())})
                    handle_dataset_message({'type':'FAIL','message':'QC Failed due to '+str(quality_check_error['message'])})
                    try:
                        handle_dataset_message({'type':'FAIL','message':str(quality_check_error['defect'])})
                        dataset_import_logs.append({'type':'FAIL','message':str(quality_check_error['defect']),'logtime':str(datetime.now())})
                        handle_dataset_message({'type':'FAIL','message':str(quality_check_error['tips'])})
                        dataset_import_logs.append({'type':'FAIL','message':str(quality_check_error['tips']),'logtime':str(datetime.now())})
                    except:
                        pass

                    save_log_file_fs(target_file_name,dataset_import_logs)
                    name_space.abort(400,message=quality_check_error['message'],defect=quality_check_error['defect'] if 'defect' in quality_check_error.keys() else '',tips=quality_check_error['tips'],log_file=target_file_name)

            else:
                handle_dataset_message({'type':'FAIL','message':'QC Failed due to invalid file extenstion '+str(filetype)})
                msg='Uploaded file doesnot have valid names and/or extension('+str(ALLOWED_EXTENSION)+')'
                save_upload_log(name=name,description='-',uploadedFileName=myfile.filename,savedFileName='-',fileSize=str(0)+'KB',belongsToLayout=None,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                name_space.abort(400,message=msg)

            return { 'status':200,'message':'File Validation successful'}  
        else:
            name_space.abort(400,message='No file is uploaded, Missing file data ')


def valid_fileExtension(filetype):
    return filetype in ALLOWED_EXTENSION

def save_upload_log(name,description,uploadedFileName,savedFileName,fileSize,belongsToLayout,uploadedBy,isLayoutFile=False,isDataFile=False,remarks='-'):
    try:
        file_load_info = MetaFileUploadLogs(name=name,description=description,uploadedFileName=uploadedFileName,savedFileName=savedFileName,fileSize=fileSize,belongsToLayout=belongsToLayout,uploadedBy=uploadedBy,isLayoutFile=isLayoutFile,isDatasetFile=isDataFile,remarks=remarks)
        session.add(file_load_info)
        session.commit()
        if isDataFile and remarks=='COMPLETED':
            saved_loginfo = session.query(MetaFileUploadLogs).filter(MetaFileUploadLogs.isDatasetFile==True).filter(MetaFileUploadLogs.savedFileName==savedFileName).filter(MetaFileUploadLogs.remarks=='COMPLETED').first()
            return saved_loginfo.id

        return True
    except Exception as err:
        session.rollback()
        print(err.__str__)
        return False

def save_log_file_fs(file_name,loglist):
    target_file_name=file_name.rsplit('.',1)[0]
    with open(os.path.abspath(app.config['DATASET_IMPORT_LOG_FOLDER']+'\\'+target_file_name+'.log'),'w',encoding='utf-8') as logfile:
        faillogFound=False
        for item in loglist:
            logType=item['type']
            if not faillogFound:
                faillogFound=True if logType=='FAIL' else False
            if (not faillogFound) ^ (logType=='SUCCESS'):
                pass
            else:
                remarks= item['tips'] if 'tips' in item.keys() else None
                defectAt= item['defect'] if 'defect' in item.keys() else ''
                logfile.write(str(item['logtime'])+':>|'+str(item['type'])+':|:'+str(item['message'])+':|:'+defectAt)
                if remarks:
                    logfile.write('\n')
                    logfile.write(remarks)
                logfile.write('\n')
    print('saving the log now')
    if cloud_storageisAt=='GOOGLE':
        save_dataset_import_log_file(target_file_name+'.log')
    elif cloud_storageisAt=='MICROSOFT':
        save_dataset_import_log_file_az(target_file_name+'.log')

        
def processMyCsvFile(name,desc,myfile,uploader,file_size,filetype,overrideLayout):
    final_record_list=[]  
    attributeList=MetaLayoutDetail.get_mandateField()  
    firstRow = myfile.stream.readline()
    receivingBytes=False
    if type(firstRow) is not str:
        receivingBytes=True
        firstRow=firstRow.decode('utf-8')
    if(firstRow.count('|')!=2):
        return {'status':400,'message':'Only Layout header Information should be provided at the first row'}
    elif(len(firstRow.split('|')[0])>50 or len(firstRow.split('|')[0])<2):
        return {'status':400,'message':'Invalid layout Heading provide,Header title must be between 2 to 50 characters only'}
    else:
        target_file_name = firstRow.split("|")[0]+'.'+myfile.filename.rsplit('.',1)[1]
        layout_head_data=firstRow.split('|')
        SCHEMA_TO_MAP=layout_head_data[2].strip('\n').strip('\r').split(',')
        try:
            session.add(MetaLayout(layoutHead=layout_head_data[0],layoutDescription=layout_head_data[1],createdBy=uploader))
            session.commit() #save item to databse
        except IntegrityError as err:            
            session.rollback()
            if overrideLayout:
                pass
            else:
                return {'status':400,'message':'This Metadata Name already Exist. Are you willing to overwrite its details ?'}

        except KeyError as krr:
            return {'status':400,'message':'User information missing in the payload, cant process header info'}
        
        layout_head = session.query(MetaLayout).filter(MetaLayout.layoutHead==layout_head_data[0]).first()
       
        #if the overrwrite flag is on the we remove all the existing mapping
        if overrideLayout:
            session.query(MetaLayoutSchemaMapping).filter(MetaLayoutSchemaMapping.layoutHead==layout_head.id).delete(synchronize_session=False)
            session.commit
            
        #Now we also save the Mapping information
        processingRecord=None
        for schema_Name in SCHEMA_TO_MAP:
            try:
                processingRecord=MetaLayoutSchemaMapping(schemaName=schema_Name.upper(),layoutHead=layout_head.id,createdBy=uploader)
                session.add(processingRecord)
                session.commit()

            except SQLAlchemyError as err:
                session.rollback()
                try:
                    errorstring = str(err.__dict__['orig'])
                except:
                    errorstring=err.__str__()

                if errorstring.find('unq_layoutschema_map')>0:
                    if overrideLayout:
                        pass
                    else:
                        return {'status':400,'message':'Attempt of creating duplicate mapping, please verify','defect':'('+str(processingRecord.layoutHead)+', '+processingRecord.schemaName+')'}

                elif errorstring.find('fk_mapper_schema')>0:
                    return {'status':400,'message':'Supplied Schema isnot defined in the system.','defect':'('+str(processingRecord.layoutHead)+', '+processingRecord.schemaName+')'}

                elif errorstring.find('fk_mapper_layoutid')>0:
                    return {'status':400,'message':'Supplied layout doesnot exists., please verify','defect':'('+str(processingRecord.layoutHead)+', '+processingRecord.schemaName+')'}

                elif ((errorstring.find('fk_mapper_creator')>0) or (errorstring.find('fk_mapper_updator')>0)):
                    return {'status':400,'message':'No such User Exist, please verify','defect':'('+str(processingRecord.layoutHead)+', '+processingRecord.schemaName+')'}
        
                else:
                    return {'status':400,'message':errorstring}

            except Exception as exce:
                session.rollback()
                return {'status':400,'message':str(exce)}


    #saving the log for begininning of the metainformation import process
    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks='MetaInformation import started')
    print('target file name is '+str(target_file_name))           
    print("path is "+os.path.abspath(app.config['UPLOAD_FOLDER']))
    with open(os.path.abspath(app.config['UPLOAD_FOLDER']+'\\'+target_file_name),'w') as f:
        lines=[firstRow]
        for filedata in myfile.stream.readlines():
            if receivingBytes:
                filedata=filedata.decode('utf-8')
            filedata=filedata.strip('\n').strip('\r')
            if re.findall('\\\\[a-zA-Z]| \||\| \|?',filedata):
                msg='File data contains unnecessary Characters and/spaces after delimeter, remove all spaces and special characters'
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg+' :: '+filedata)
                return {'status':400,'message':msg,'defect':filedata}
            
            if(filedata.count('|')==0 and len(filedata)==0):
                pass
                #do nothing
            elif '|' not in filedata:
                msg="File Not uploaded, Corrupted File recieved for processing" if filetype in ['xlsx'] else " File Not uploaded, Undefined row Structure found. Only '|' delemiter is supported " 
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg+' :: '+filedata)
                return {'status':400,'message':msg,'defect':filedata}
            elif filedata.count('|')==1:
                if not layout_head:
                    msg="Invalid layout Heading provided, couldnot fetch new Layout head"
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                    return {'status':400,'message':msg}
                else:
                    msg="Multiple Layout header found in the file , Layout header info should be only at the top of the file."
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                    return {'status':422,'message':msg}
            elif(filedata.count('|') !=9):
                msg="Unexpected details received, please remove the additional data column from the excel file." if  filetype in ['xlsx'] else "Only 10 '|' delemited values are allowed, Correct an re-upload the file"
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                return {'status':400,'message':msg,'defect':filedata}
            else:
                try:
                    if not layout_head:
                        msg="Invalid layout Heading provided, couldnot fetch new Layout head"
                        save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                        return {'status':400,'message':msg}
                    else:
                        rowdata=filedata.split('|') 
                        rowdata.append(layout_head.id)
                        rowdata.append(uploader)     
                        temp = MetaLayoutDetail('','','','',None,1) ##initializing the empty object first for later update                       
                        final_record_list.append(temp.build(dict(zip(attributeList,rowdata))))
                        lines.append(filedata)
                except KeyError as krr:
                    msg='User information missing in the payload'
                    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg)
                    return {'status':400,'message':msg}
        f.writelines(lines)

    #registering all the detail informations
    if overrideLayout:
        existing_record=session.query(MetaLayoutDetail).filter(MetaLayoutDetail.layoutHead==layout_head.id)
        existing_record.delete(synchronize_session=False)
        session.commit() # remove all details of curren layout head if exists

    for layoutDetailInfo in final_record_list:
        layoutDetailInfo.attrLOV=str(layoutDetailInfo.attrLOV).split(',')
        #converting str type to list type for saving in the database.                
        try:     
            session.add(layoutDetailInfo)
            session.commit()
                   
        except SQLAlchemyError as serr:
            session.rollback()
            if(type(serr) is DataError):
                msg="Loading Aborted due to invalid data for the Layout "+str(layout_head.layoutHead)
                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=msg+' :: '"Check for Data format in  row starting with: "+serr.__dict__['params']['attrIdentifier'])
                return {'status':400,'message':msg,'defect':"Check for Data format in  row starting with: "+serr.__dict__['params']['attrIdentifier']}
                
            else:
                try:
                    errorstring = str(serr.__dict__['orig'])
                except:
                    errorstring=serr.__str__()

                if errorstring.find('fk_create_layoutdtl_user')>0 or errorstring.find('fk_update_layoutdtl_user')>0:
                    message="Provided User doesn't exist. please verify details"

                elif errorstring.find('fk_layoutid_layoutdtl_layout')>0:
                    message="Provided Layout doesn't exist. please verify details"

                elif errorstring.find('chk_min_max')>0:
                    message="Min value must be less than Max Value. please verify details"
            
                elif errorstring.find('chk_negative_ordering')>0:
                    message='Field ordering for current layout cannot be negative value. please verify'

                elif errorstring.find('uq_same_field_in_layout')>0:
                    message="Attempt of Importing duplicate column for the Layout "+str(layout_head.layoutHead)

                elif errorstring.find('uq_same_col_order_in_layout')>0:
                    message="Each column must have unique field orders, Overlapping not allowed. please verify: "+str(layout_head.layoutHead)+" layout data "  

                elif errorstring.find('chk_type_attribute')>0:
                    message='Provided datatype is not supported by the application. Supported attribute types are '+str(ALLOWED_DATATYPES)
                else: 
                    message=errorstring

                save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks=message+' :: '+serr.__dict__['params']['attrIdentifier']) 

                return {'status':400,'message':message,'defect':serr.__dict__['params']['attrIdentifier']}

        except Exception as exc:
            session.rollback()
            return {'status':400,'message':exc.__str__()}
        

    # session.commit() #commiting the detail information when everything is done

    save_upload_log(name=name,description=desc,uploadedFileName=myfile.filename,savedFileName=target_file_name,fileSize=str(file_size)+'KB',belongsToLayout=layout_head.id,uploadedBy=uploader,isLayoutFile=True,remarks='COMPLETED')
    return None #None represent that no any error was encountered during processing of this module


def initiateCSVTransformation(fileStreamObject,isMetaDataFile=False,isDatasetFile=False,hasHeader=None):
    try:
        filename=fileStreamObject.filename.rsplit('.',1)[0]+'_'+str(round(datetime.now().timestamp()))
        targetStorage=os.path.abspath('./filestorage/tempfile/'+filename+'.csv')
        rawInputDataframe= pd.read_excel(fileStreamObject,header=None)
        nullRefined_df=rawInputDataframe.dropna(how='all').reset_index()
        if isMetaDataFile:
            #preparing the conversion for the metada information
            try:
                metalayout=(nullRefined_df.iloc[0:2])
                metalayout.columns=metalayout.loc[0]
                metalayout.loc[:,'Datasource Name':'Brand'].drop(index=0).to_csv(targetStorage,header=False,index=False,sep="|")
            except KeyError as krr:
                return {'message':'Datasource information should be written first. Please donot alter the Excel file header row and its formatting. Check on the Sample File for reference.',
                    'defect':'missing Header :> '+str(krr)
                    }
            except Exception as exc:
                return {
                    'message':exc.__str__
                }
                

            #preparing the conversion for the metadata attributes information
            try:
                metalayoutdetail=nullRefined_df.iloc[2:].reset_index()
                metalayoutdetail.columns=metalayoutdetail.loc[0]
                # metalayoutdetail['Attribute_ListOFValues']=metalayoutdetail['Attribute_ListOFValues'].apply(lambda x: '[\''+str(x).strip().replace(',','\',\'')+'\']')
                (metalayoutdetail.drop(index=0)[[
                    'Attribute_identifier',
                    'Attribute_name',
                    'Attribute_description',
                    'Attribute_data_type',
                    'Attribute_min_length',
                    'Attribute_max_length',
                    'Attribute_ListOFValues',
                    'Attribute_null_support',
                    'Attribute_regex_check_string',
                    'Field_order_of_current_attribute']]
                ).to_csv(targetStorage,sep="|",header=False,index=False,mode="a")
            except KeyError as krr:
                return {'message':'Attribute information should be written after the Datasource information.Multiple Datasource information is not allowed in same file. Please donot alter the Attribute(s) heading row and its spelling/formatting. Check on the Sample file for Reference.',
                    'defect':'missing Header :> '+str(krr)+' in its place.'
                    }
            except Exception as exc:
                return {
                    'message':exc.__str__,
                    'defect':''
                }
                
        elif isDatasetFile:
            if hasHeader:
                nullRefined_df.columns=nullRefined_df.loc[0]
                nullRefined_df.drop(index=0).drop(labels=0, axis=1).to_csv(targetStorage,header=False,index=False,sep="|")
            else:
                nullRefined_df.to_csv(targetStorage,header=False,index=False,sep="|")

        return str(targetStorage) #when everything is fine we return the location of the temp file resulted after the transformation process
    except Exception as ex:
        session.rollback()
        return ex.__str__