service postgresql start

su postgres <<EOSU

psql --command "CREATE DATABASE $db_name;"

psql -U postgres -d $db_name -c "CREATE SCHEMA  $db_schema;"

psql --command "ALTER USER postgres WITH PASSWORD '$db_password'; "

EOSU

sed -i "s/name=metamanager/name=$db_name/1" application.properties
sed -i "s/schema=meta_config/schema=$db_schema/1" application.properties
sed -i "s/password=root/password=$db_password/1" application.properties
sed -i "s/ip=localhost/ip=$db_ipaddress/1" application.properties
sed -i "s/port=5432/port=$db_port/1" application.properties
sed -i "s/dailect=postgres/dailect=$db_dailect/1" application.properties


python3.8 structure_recreated.py

python3.8 main.py